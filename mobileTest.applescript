set currentDate to do shell script "date +'%m-%d-%Y-%H%M.%S'"
tell application "iTerm2"
    create window with default profile
    tell current session of current window
        -- Run the Lighthouse test command
        write text "lighthouse https://localhost:9000/ --view --output-path=./reports/" & currentDate & "-lighthouse-report.html --quiet --form-factor=mobile --chrome-path=\"/Applications/Google Chrome Canary.app/Contents/MacOS/Google Chrome Canary.app\" --chrome-flags=\"--incognito --headless\""
    end tell
end tell

-- Delay for a few seconds to allow the Lighthouse test to complete
delay 33

tell application "iTerm2"
    close the front window
end tell