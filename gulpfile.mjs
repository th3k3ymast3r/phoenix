"use strict";

// Load GULP plugins
import { default as gulp, series, watch, src, dest, task, lastRun } from "gulp";
import { default as autoprefixer } from "gulp-autoprefixer";
import { default as cache } from "gulp-cache";
import { default as cleanCSS } from "gulp-clean-css";
import { default as concat } from "gulp-concat";
import { default as ejs } from "gulp-ejs";
import { default as gulpif } from "gulp-if";
import { default as ignore } from "gulp-ignore";
import { default as gzip } from "gulp-gzip";
import { default as include } from "gulp-include";
import { default as plumber } from "gulp-plumber";
import { default as rename } from "gulp-rename";
import { default as gulpSass } from "gulp-sass";
import * as sass from "sass";
const sassCompiler = gulpSass(sass);
import { default as sassGlob } from "gulp-sass-glob";
import { default as sourcemaps } from "gulp-sourcemaps";
import { default as uglify } from "gulp-uglify";

// Load third-party plugins
import { default as browsersync } from "browser-sync";
import { default as compression } from "compression";
import { deleteSync as deleteSync } from "del";
import { default as log } from "fancy-log";
import { default as mime } from "mime-types";
import * as path from 'path';
import sharp from "sharp";
import through2 from 'through2';

// Output to terminal
import { exec } from "child_process";

// Define paths
const paths = {

	fonts : {
		src : "src/fonts/*.{eot,svg,ttf,woff,woff2}",
		dest : "dist/assets/fonts"
	},
	images : {
		src : "src/images/**/*.{gif,jpg,png,svg}",
		dest : "dist/assets/images"
	},
	root : {
		src : "src/templates/*.{ico,js,json,png,txt,webp,appcache,htaccess}",
		dest : "dist"
	},
	sass : {
		src : "src/css/**/*.{scss,sass,css,map}",
		dest : "dist/assets/css",
		aux : "src/css/maps"
	},
	sensorjs : {
		src : "src/vendor/sensor/js/**/*.js",
		dest : "dist/assets/js/sensor"
	},
	scripts : {
		src : "src/js/**/*.js",
		dest : "dist/assets/js"
	},
	atf : {
		src : [
			"src/css/atf.sass",
			"src/css/atf/*.scss"
			],
		dest : "dist/assets/css"
	},
	styles : {
		src : [
			"src/css/*.sass",
			"src/css/partials/*.scss"
		],
		aux : "src/css/maps",
		dest : "dist/assets/css"
	},
	templates : {
		src : "src/templates/**/*.ejs",
		dest : "dist"
	},
	video : {
		src : "src/video/*.*",
		dest : "dist/assets/video"
	},
	audio : {
		src : "src/audio/*.*",
		dest : "dist/assets/audio"
	},
	errorpagessass : {
		src : [
			"src/vendor/error-pages/sass/*.sass",
			"src/vendor/error-pages/sass/partials/*.scss"
		],
		dest : "dist/assets/css/error-pages"
	},
	errorpagesjs : {
		src : "src/vendor/error-pages/js/**/*.js",
		dest : "dist/assets/js/error-pages"
	},
	errorpageshtml : {
		src : "src/vendor/error-pages/**/*.ejs",
		dest : "dist"
	},
    errorpagesimg : {
		src : "src/vendor/error-pages/images/**/*.jpg",
		dest : "dist/assets/images/error-pages"
	},
	backgroundvdosass : {
		src : [
			"src/vendor/background-vdo/sass/*.sass",
			"src/vendor/background-vdo/sass/partials/*.scss"
		],
		dest : "dist/assets/css/background-vdo"
	},
	backgroundvdojs : {
		src : "src/vendor/background-vdo/js/**/*.js",
		dest : "dist/assets/js/background-vdo"
	},
	backgroundvdohtml : {
		src : "src/vendor/background-vdo/*.ejs",
		dest : "dist"
	},
    backgroundvdoimg : {
		src : "src/vendor/background-vdo/images/**/*.jpg",
		dest : "dist/assets/images/background-vdo"
	},
    backgroundvdo : {
		src : "src/vendor/background-vdo/video/*.{mp4,webm,vtt}",
		dest : "dist/assets/video/background-vdo"
	},
	gtranslatecss : {
		src : "src/vendor/gtranslate/css/*.css",
		dest : "dist/assets/css/gtranslate"
	},
	gtranslatejs : {
		src : [
			"src/vendor/gtranslate/js/*.js",
			"src/vendor/gtranslate/translate_a/*.js"
		],
		dest : "dist/assets/js/gtranslate"
	},	
	gtranslatejsmin : {
		src : "src/vendor/gtranslate/js/*.js",
		dest : "dist/assets/js/gtranslate/js"
	},
	elementjsmin : {
		src : "src/vendor/gtranslate/translate_a/*.js",
		dest : "dist/assets/js/gtranslate/translate_a"
	},
	gtranslateimg : {
		src : "src/vendor/gtranslate/flags/*.png",
		dest : "dist/assets/images/gtranslate"
	},
	globalvars : {
		src : "src/vendor/global-vars/*.css",
		dest : "dist/assets/css/global-vars"
	},
	settingssass : {
		src : [
			"src/vendor/settings-menu/sass/*.sass",
			"src/vendor/settings-menu/sass/partials/*.scss"
		],
		dest : "dist/assets/css/settings-menu"
	},
	settingsjs : {
		src : [
			"src/vendor/settings-menu/js/*.js",
			"src/vendor/settings-menu/js/partials/*.js"
		],
		dest : "dist/assets/js/settings-menu"
	},
	settingshtml : {
		src : "src/vendor/settings-menu/*.ejs",
		dest : "dist"
	},
	sunrisesunset : {
		src : "src/vendor/sunrise-sunset/sunrise-sunset.js",
		dest : "dist/assets/js/sunrise-sunset"
	},
	sunrisesunsetdarkmode : {
		src : "src/vendor/sunrise-sunset/sunrise-sunset-darkmode.js",
		dest : "dist/assets/js/sunrise-sunset"
	},
	testimonialssass : {
		src : [
			"src/vendor/testimonials/sass/*.sass",
			"src/vendor/testimonials/sass/partials/*.scss"
		],
		dest : "dist/assets/css/testimonials"
	},
	testimonialshtml : {
		src : "src/vendor/testimonials/*.html",
		dest : "dist"
	},
	testimonialsimg : {
		src : "src/vendor/testimonials/images/*.jpg",
		dest : "dist/assets/images/testimonials"
	},
	twitcker : {
		src : "src/vendor/twitcker/css/*.css",
		dest : "dist/assets/css/twitcker"
	},
	threedmenusass : {
		src : [
			"src/vendor/three-d-menu/sass/*.sass",
			"src/vendor/three-d-menu/sass/partials/*.scss"
		],
		dest : "dist/assets/css/three-d-menu"
	},
	threedmenuhtml : {
		src : "src/vendor/three-d-menu/*.html",
		dest : "dist"
	},
	departuresboard : {
		src : "src/vendor/departures-board/js/*.js",
		dest : "dist/assets/js/departures-board"
	},
	departuresboardsass : {
		src : [
			"src/vendor/departures-board/sass/*.sass",
			"src/vendor/departures-board/sass/partials/*.scss"
		],
		aux : "src/vendor/departures-board/sass/maps",
		dest : "dist/assets/css/departures-board"
	},
	departuresboard3p : {
		src : [
			"src/vendor/departures-board/3p/*.js"
		],
		dest : "dist/assets/js/departures-board"
	},
	progressmarkercss : {
		src : "src/vendor/progress-marker/css/*.css",
		dest : "dist/assets/css/progress-marker"
	},
	progressmarkerjs : {
		src : "src/vendor/progress-marker/js/*.js",
		dest : "dist/assets/js/progress-marker"
	},
	embedmediumcss : {
		src : "src/vendor/embed-medium/css/*.css",
		dest : "dist/assets/css/embed-medium"
	},
	embedmediumjs : {
		src : "src/vendor/embed-medium/js/*.js",
		dest : "dist/assets/js/embed-medium"
	},
	visdatafonts : {
		src : "src/vendor/vis-data/fonts/*.{eot,svg,ttf,woff,woff2}",
		dest : "dist/assets/fonts"
	},
	visdatacss : {
		src : "src/vendor/vis-data/css/*.css",
		dest : "dist/assets/css/vis-data"
	},
	visdatasass : {
		src : [
			"src/vendor/vis-data/sass/*.sass",
			"src/vendor/vis-data/sass/partials/*.scss"
		],
		aux : "src/vendor/vis-data/sass/maps",
		dest : "dist/assets/css/vis-data"
	},
	visdatajs : {
		src : [
			"src/vendor/vis-data/js/*.js",
			"src/vendor/vis-data/js/partials/*.js"
		],
		dest : "dist/assets/js/vis-data"
	},
	visdata3p : {
		src : [
			"src/vendor/vis-data/3p/*.js"
		],
		dest : "dist/assets/js/vis-data"
	},
	visdatahtml : {
		src : "src/vendor/vis-data/*.html",
		dest : "dist"
	},
	visdataimg : {
		src : "src/vendor/vis-data/images/*.{jpg,svg}",
		dest : "dist/assets/images/vis-data"
	},
	customboostrapcss : {
		src : [
			"src/vendor/bootstrap/bootstrap.sass",
			"src/vendor/bootstrap/scss/*.scss"
		],
		dest : "src/vendor/bootstrap/css"
	},
	bootstrapcss : {
		src : "src/vendor/bootstrap/css/*.{css,map}",
		dest : "dist/assets/css/bootstrap"
	},
	bootstrapjs : {
		src : "src/vendor/bootstrap/js/*.{js,map}",
		dest : "dist/assets/js/bootstrap"
	},
	vendorjs : {
		src : [
			"src/vendor/es6-promise/*.js",
			"src/vendor/jquery/*.js",
			"src/vendor/jsonld/*.js",
			"src/vendor/modernizr/*.js",
			"src/vendor/pwacompat/*.js",
			"src/vendor/voice-recognition/*.min.js"
		],
		dest : "dist/assets/js"
	},
	downloads : {
		src : "src/downloads/**/*.*",
		dest : "dist/assets/downloads"
	}
};

//Compression gzip enabled for browsersync
const middlewares = [compression(), function(req, res, next) { res.setHeader('Access-Control-Allow-Origin', '*');  next(); }];

// Clear cache
export const clear = () => cache.clearAll();

export const clean = async () => {
	clear();
	await deleteSync(['dist']);
}

// Handle error logging
function handleerror(err) {
    log.error('Error:', err.toString());
    this.emit('end');
}

// Generate version fragment
export function generateVersionTimestamp() {
    const now = new Date();
    const year = now.getFullYear();
    const month = String(now.getMonth() + 1).padStart(2, '0');
    const day = String(now.getDate()).padStart(2, '0');
    const hours = String(now.getHours()).padStart(2, '0');
    const minutes = String(now.getMinutes()).padStart(2, '0');
    return `?v=${year}-${month}-${day}-${hours}${minutes}`;
}

// For SASS/SCSS + GZIP files and minifying
export function cssminifyandgzip({ source, destination, paths, aux, task }) {
	return src(source, { since : lastRun(task), encoding : false })
		.pipe(plumber({ errorHandler: handleerror }))
		.pipe(sourcemaps.init())
		.pipe(sassGlob())
		.pipe(sassCompiler({
			outputStyle: 'compressed',
			includePaths: paths,
			errLogToConsole: true
		}).on('error', sassCompiler.logError))
		.pipe(autoprefixer("last 2 version", "safari 5", "ie 8", "ie 9", "opera 12.1", "ios 6", "android 4"))
		.pipe(cleanCSS({debug: true}, (details) => {
			// Extract the relative file name without extension
			const basename = path.basename(details.name, path.extname(details.name));
			const originalsizekb = details.stats.originalSize / 1024;
			const minifiedsizekb = details.stats.minifiedSize / 1024;
			log(`${basename}.css` + `: ${originalsizekb.toFixed(3)} kb`);
			log(`${basename}.min.css` + `: \x1b[38;2;249;42;114m${minifiedsizekb.toFixed(3)} kb\x1b[0m`);
		}))
		.pipe(sourcemaps.write({
			includeContent : false,
			sourceRoot : file => `file:///Users/th3k3ymast3r/dev/phoenix/${aux}`
		}))
		.pipe(rename({
			suffix: ".min"
		}))
		.pipe(dest(destination))
		.pipe(gzip())
		.pipe(dest(destination))
}

//For EJS -> HTML + GZIP
export function htmlandgzip({ source, destination, ext, task }) {
	return src(source, { since : lastRun(task), encoding : false })
		.pipe(plumber({ errorHandler: handleerror }))
		.pipe(ejs({ versionTimestamp: generateVersionTimestamp(), cache: false, async: false }))
		.pipe(rename({ extname: ext }))
		.pipe(ignore.exclude('partials/**'))
		.pipe(dest(destination))
		.pipe(gzip())
		.pipe(dest(destination));
}

//For JS + GZIP
function jsandgzip({ source, destination, basename, ext, task }) {
	return src(source, { since : lastRun(task), encoding : false })
		.pipe(plumber({ errorHandler: handleerror }))
		// Condition = app.js
		.pipe(gulpif(basename+ext==="app.js", concat("app.js")))
		.pipe(gulpif(basename+ext==="app.js", include()))
		// Condition = error-pages.js
		.pipe(gulpif(basename+ext==="error-pages.js", concat("error-pages.js")))
		.pipe(gulpif(basename+ext==="error-pages.js", include()))
		// Condition = gtranslate.js
		.pipe(gulpif(basename+ext==="gtranslate.js", concat("gtranslate.js")))
		.pipe(gulpif(basename+ext==="gtranslate.js", include()))
		// Condition = element.js
		.pipe(gulpif(basename+ext==="element.js", concat("element.js")))
		.pipe(gulpif(basename+ext==="element.js", include()))
		// Condition = sensor.js
		.pipe(gulpif(basename+ext==="sensor.js", concat("sensor.js")))
		.pipe(gulpif(basename+ext==="sensor.js", include()))
		// Condition = settings-menu.js
		.pipe(gulpif(basename+ext==="settings-menu.js", concat("settings-menu.js")))
		.pipe(gulpif(basename+ext==="settings-menu.js", include()))
		// Condition = sunrise-sunset.js
		.pipe(gulpif(basename+ext==="sunrise-sunset.js", concat("sunrise-sunset.js")))
		.pipe(gulpif(basename+ext==="sunrise-sunset.js", include()))
		// Condition = sunrise-sunset-darkmode.js
		.pipe(gulpif(basename+ext==="sunrise-sunset-darkmode.js", concat("sunrise-sunset-darkmode.js")))
		.pipe(gulpif(basename+ext==="sunrise-sunset-darkmode.js", include()))
		// Condition = vis-data.js
		.pipe(gulpif(basename+ext==="vis-data.js", concat("vis-data.js")))
		.pipe(gulpif(basename+ext==="vis-data.js", include()))
		// Condition = departures-board.js
		.pipe(gulpif(basename+ext==="departures-board.js", concat("departures-board.js")))
		.pipe(gulpif(basename+ext==="departures-board.js", include()))
		// Condition = background-vdo.js
		.pipe(gulpif(basename+ext==="background-vdo.js", concat("background-vdo.js")))
		.pipe(gulpif(basename+ext==="background-vdo.js", include()))
		.pipe(uglify({
			mangle: false,  // Disable name mangling
            compress: {
                drop_console: false  // Keep console logs for debugging
            }
		}))
		.pipe(rename({
			suffix: ".min",
			extname: ext
		}))
		.pipe(dest(destination))
		.pipe(gzip())
		.pipe(dest(destination))
}

// For Compressing IMG + GZIP
export function compressimg({ source, destination, task }) {
    let processedFiles = [];

    return src(source, { since: lastRun(task), encoding: false })
        .pipe(plumber({ errorHandler: handleerror }))
		.pipe(gulpif(
            file => ['.gif', '.jpg', '.png'].includes(path.extname(file.relative)),
            through2.obj((file, enc, cb) => {
                const ext = path.extname(file.relative).toLowerCase();

                // Process GIF, JPG, and PNG files with sharp
                if (ext === '.gif') {
                    sharp(file.contents)
                        .gif({ colors: 128 }) // Use 128 colors for GIF
                        .toBuffer()
                        .then(data => {
                            file.contents = data;
                            cb(null, file);
                        })
                        .catch(err => cb(err));
                } else if (ext === '.jpg') {
                    sharp(file.contents)
                        .jpeg({ quality: 75, mozjpeg: true }) // Use 75 quality for JPG
                        .toBuffer()
                        .then(data => {
                            file.contents = data;
                            cb(null, file);
                        })
                        .catch(err => cb(err));
                } else if (ext === '.png') {
                    sharp(file.contents)
                        .png({ quality: 88 }) // Use 88 quality for PNG
                        .toBuffer()
                        .then(data => {
                            file.contents = data;
                            cb(null, file);
                        })
                        .catch(err => cb(err));
                } else {
                    // Pass through if file extension is not GIF, JPG, or PNG
                    cb(null, file);
                }
            })
        ))
        .on('data', (file) => {
            const basename = path.basename(file.relative, path.extname(file.relative));
            if (!processedFiles.includes(basename)) {
                const originalSizeKB = file.contents.length / 1024;
                const compressedSizeKB = file.contents.length / 1024;
                log(`\x1b[38;2;102;217;239m${basename}${file.extname}\x1b[0m` + ` @ ${originalSizeKB.toFixed(3)} kb ⋈` + ` \x1b[38;2;249;42;114m${compressedSizeKB.toFixed(3)} kb\x1b[0m`);
                processedFiles.push(basename);
            }
        })
        .pipe(dest(destination)) // Copy processed files to destination folder
        .pipe(gzip()) // Gzip the processed files
        .pipe(dest(destination)); // Copy gzipped files to destination folder
}

//For Copy + MIME + GZIP
export function copyandmime({ source, destination, task }) {
	let copiedFiles = [];

	return src(source, { since : lastRun(task), encoding : false })
		.on('data', (file) => {
			const basename = path.basename(file.relative);
			const ext = path.extname(basename);
			const mimetype = mime.lookup(ext);

			if (basename && !copiedFiles.includes(basename)) {
				log(`\x1b[1m❏\x1b[0m '\x1b[38;2;102;217;239m${basename}\x1b[0m' with MIME type '\x1b[38;2;249;42;114m${mimetype}\x1b[0m'`);
				}
			if (basename && basename !== '' && basename !== null && basename !== undefined && basename !== '-1') {
				copiedFiles.push(basename);
			}
		})
		.pipe(dest(destination))
		.pipe(gzip())
		.pipe(dest(destination));
}

// For Copy + GZIP only
export function copyandgzip({ source, destination, basement, task }) {
	let copiedFiles = [];

	return src(source, { since : lastRun(task), base : basement, encoding : false })
		.on('data', (file) => {
			const basename = path.basename(file.relative, path.extname(file.relative));

			if (basename && !copiedFiles.includes(basename)) {
				log(`\x1b[1m⇧\x1b[0m '\x1b[38;2;102;217;239m${basename}${file.extname}\x1b[0m'`);
			}
			if (basename && basename !== '' && basename !== null && basename !== undefined && basename !== '-1') {
				copiedFiles.push(basename);
			}
		})
		.pipe(dest(destination))
		.pipe(gzip())
		.pipe(dest(destination));
}

// For Copy only
export function copyonly({ source, destination, task }) {
	let copiedFiles = [];

    return src(source, { since : lastRun(task), encoding : false })
		.on('data', (file) => {
			const basename = path.basename(file.relative, path.extname(file.relative));

			if (basename && !copiedFiles.includes(basename)) {
				log(`\x1b[1m⇧\x1b[0m '\x1b[38;2;102;217;239m${basename}${file.extname}\x1b[0m'`);
			}
			if (basename && basename !== '' && basename !== null && basename !== undefined && basename !== '-1') {
				copiedFiles.push(basename);
			}
		})
        .pipe(dest(destination));
}

export function root() {
	return copyandgzip({
		source : paths.root.src,
		destination : paths.root.dest,
		task : "root"
	});
}

export function rootmin() {
    return compressimg({
        source: paths.root.src,
        destination: paths.root.dest,
		task : "rootmin"
    });
}

export function sensorjs() {
	return jsandgzip({
		source : paths.sensorjs.src,
		destination : paths.sensorjs.dest,
		basename : "sensor",
		ext : ".js",
		task : "sensorjs"
	});
}

export function atf() {
	return cssminifyandgzip({
		source : paths.atf.src,
		destination : paths.atf.dest,
		aux : "src/vendor/atf",
		task : "atf"
	});
}

export function globalvars() {
	return cssminifyandgzip({
		source : paths.globalvars.src,
		destination : paths.globalvars.dest,
		aux : "src/vendor/globalvars",
		task : "globalvars"
	});
}

export function errorpagessass() {
	return cssminifyandgzip({
		source : paths.errorpagessass.src,
		destination : paths.errorpagessass.dest,
		paths : "src/vendor/error-pages/partials",
		task : "errorpagessass"
	});
}

export function errorpagesjs() {
	return jsandgzip({
		source : paths.errorpagesjs.src,
		destination : paths.errorpagesjs.dest,
		basename : "error-pages",
		ext : ".js",
		task : "errorpagesjs"
	});
}

export function errorpageshtml() {
	return htmlandgzip({
		source : paths.errorpageshtml.src,
		destination : paths.errorpageshtml.dest,
        ext : ".html",
		task : "errorpageshtml"
	});
}

export function errorpagesimg() {
	return compressimg({
		source : paths.errorpagesimg.src,
		destination : paths.errorpagesimg.dest,
		task : "errorpagesimg"
	});
}

export function backgroundvdosass() {
	return cssminifyandgzip({
		source : paths.backgroundvdosass.src,
		destination : paths.backgroundvdosass.dest,
		paths : "src/vendor/background-vdo/partials",
		task : "backgroundvdosass"
	});
}

export function backgroundvdojs() {
	return jsandgzip({
		source : paths.backgroundvdojs.src,
		destination : paths.backgroundvdojs.dest,
		basename : "background-vdo",
		ext : ".js",
		task : "backgroundvdojs"
	});
}

export function backgroundvdohtml() {
	return htmlandgzip({
		source : paths.backgroundvdohtml.src,
		destination : paths.backgroundvdohtml.dest,
        ext : ".html",
		task : "backgroundvdohtml"
	});
}

export function backgroundvdoimg() {
	return compressimg({
		source : paths.backgroundvdoimg.src,
		destination : paths.backgroundvdoimg.dest,
		task : "backgroundvdoimg"
	});
}

export function backgroundvdo() {
	return copyandgzip({
		source : paths.backgroundvdo.src,
		destination : paths.backgroundvdo.dest,
		task : "backgroundvdo"
	});
}

export function gtranslatecss() {
	return cssminifyandgzip({
		source : paths.gtranslatecss.src,
		destination : paths.gtranslatecss.dest,
		aux : "src/vendor/gtranslate/css",
		task : "gtranslatecss"
	});
}

export function gtranslatejs() {
	return copyandgzip({
		source : paths.gtranslatejs.src,
		destination : paths.gtranslatejs.dest,
		basement: "src/vendor/gtranslate",
		task : "gtranslatejs"
	});
}

export function gtranslatejsmin() {
	return jsandgzip({
		source : paths.gtranslatejsmin.src,
		destination : paths.gtranslatejsmin.dest,
		basename : "gtranslate",
		ext : ".js",
		task : "gtranslatejsmin"
	});
}

export function elementjsmin() {
	return jsandgzip({
		source : paths.elementjsmin.src,
		destination : paths.elementjsmin.dest,
		basename : "element",
		ext : ".js",
		task : "elementjsmin"
	});
}

export function gtranslateimg() {
	return copyonly({
		source : paths.gtranslateimg.src,
		destination : paths.gtranslateimg.dest,
		task : "gtranslateimg"
	});
}

export function gtranslatemin() {
	return compressimg({
		source : paths.gtranslateimg.src,
		destination : paths.gtranslateimg.dest,
		task : "gtranslatemin"
	});
}

export function settingssass() {
	return cssminifyandgzip({
		source : paths.settingssass.src,
		destination : paths.settingssass.dest,
		paths : "src/vendor/settings-menu/sass/partials",
		aux : "src/vendor/settings-menu/sass",
		task : "settingssass"
	});
}

export function settingsjs() {
	return jsandgzip({
		source : paths.settingsjs.src,
		destination : paths.settingsjs.dest,
		basename : "settings-menu",
		ext : ".js",
		task : "settingsjs"
	});
}

export function settingshtml() {
	return htmlandgzip({
		source : paths.settingshtml.src,
		destination : paths.settingshtml.dest,
        ext : ".html",
		task : "settingshtml"
	});
}

export function sunrisesunset() {
	return jsandgzip({
		source : paths.sunrisesunset.src,
		destination : paths.sunrisesunset.dest,
		basename : "sunrise-sunset",
		ext : ".js",
		task : "sunrisesunset"
	});
}

export function sunrisesunsetdarkmode() {
	return jsandgzip({
		source : paths.sunrisesunsetdarkmode.src,
		destination : paths.sunrisesunsetdarkmode.dest,
		basename : "sunrise-sunset-darkmode",
		ext : ".js",
		task : "sunrisesunsetdarkmode"
	});
}

export function twitcker() {
	return cssminifyandgzip({
		source : paths.twitcker.src,
		destination : paths.twitcker.dest,
		aux : "src/vendor/twitcker/css",
		task : "twitcker"
	});
}

export function embedmediumcss() {
	return cssminifyandgzip({
		source : paths.embedmediumcss.src,
		destination : paths.embedmediumcss.dest,
		aux : "src/vendor/embed-medium/css",
		task : "embedmediumcss"
	});
}

export function embedmediumjs() {
	return jsandgzip({
		source : paths.embedmediumjs.src,
		destination : paths.embedmediumjs.dest,
		basename : "embed-medium",
		ext : ".js",
		task : "embedmediumjs"
	});
}

export function testimonialshtml() {
	return copyandgzip({
		source : paths.testimonialshtml.src,
		destination : paths.testimonialshtml.dest,
		basement: "src/vendor/testimonials",
		task : "testimonialshtml"
	});
}

export function testimonialssass() {
	return cssminifyandgzip({
		source : paths.testimonialssass.src,
		destination : paths.testimonialssass.dest,
		paths : "src/vendor/testimonials/sass/partials",
		aux : "src/vendor/testimonials/sass",
		task : "testimonialssass"
	});
}

export function testimonialsimg() {
	return compressimg({
		source : paths.testimonialsimg.src,
		destination : paths.testimonialsimg.dest,
		task : "testimonialsimg"
	});
}

export function threedmenusass() {
	return cssminifyandgzip({
		source : paths.threedmenusass.src,
		destination : paths.threedmenusass.dest,
		paths : "src/vendor/three-d-menu/sass/partials",
		aux : "src/vendor/three-d-menu/sass",
		task : "threedmenusass"
	});
}

export function threedmenuhtml() {
	return copyandgzip({
		source : paths.threedmenuhtml.src,
		destination : paths.threedmenuhtml.dest,
		basement: "src/vendor/three-d-menu",
		task : "threedmenuhtml"
	});
}

export function visdatafonts() {
	return copyandgzip({
		source : paths.visdatafonts.src,
		destination : paths.visdatafonts.dest,
		basement: "src/vendor/vis-data/fonts",
		task : "visdatafonts"
	});
}

export function visdatasass() {
	return cssminifyandgzip({
		source : paths.visdatasass.src,
		destination : paths.visdatasass.dest,
		paths : "src/vendor/vis-data/sass/partials",
		aux : "src/vendor/vis-data/sass",
		task : "visdatasass"
	});
}

export function visdatacss() {
	return cssminifyandgzip({
		source : paths.visdatacss.src,
		destination : paths.visdatacss.dest,
		aux : "src/vendor/vis-data/css",
		task : "visdatacss"
	});
}

export function visdatajs() {
	return jsandgzip({
		source : paths.visdatajs.src,
		destination : paths.visdatajs.dest,
		basename : "vis-data",
		ext : ".js",
		task : "visdatajs"
	});
}

export function visdata3p() {
	return jsandgzip({
		source : paths.visdata3p.src,
		destination : paths.visdata3p.dest,
		basename : "vis-data-3p",
		ext : ".js",
		task : "visdata3p"
	});
}

export function visdatahtml() {
	return copyandgzip({
		source : paths.visdatahtml.src,
		destination : paths.visdatahtml.dest,
		basement: "src/vendor/vis-data",
		task : "visdatahtml"
	});
}

export function visdataimg() {
	return compressimg({
		source : paths.visdataimg.src,
		destination : paths.visdataimg.dest,
		task : "visdataimg"
	});
}

export function departuresboard() {
	return jsandgzip({
		source : paths.departuresboard.src,
		destination : paths.departuresboard.dest,
		basename : "departures-board",
		ext : ".js",
		task : "departuresboard"
	});
}

export function departuresboardsass() {
	return cssminifyandgzip({
		source : paths.departuresboardsass.src,
		destination : paths.departuresboardsass.dest,
		paths : "src/vendor/departures-board/sass/partials",
		aux : "src/vendor/departures-board/sass",
		task : "departuresboardsass"
	});
}

export function departuresboard3p() {
	return jsandgzip({
		source : paths.departuresboard3p.src,
		destination : paths.departuresboard3p.dest,
		basename : "departures-board-3p",
		ext : ".js",
		task : "departuresboard3p"
	});
}

export function progressmarkercss() {
	return cssminifyandgzip({
		source : paths.progressmarkercss.src,
		destination : paths.progressmarkercss.dest,
		aux : "src/vendor/progress-marker/css",
		task : "progressmarkercss"
	});
}

export function progressmarkerjs() {
	return jsandgzip({
		source : paths.progressmarkerjs.src,
		destination : paths.progressmarkerjs.dest,
		basename: "progress-marker",
		ext : ".js",
		task : "progressmarkerjs"
	});
}

export function styles() {
	return cssminifyandgzip({
		source : paths.styles.src,
		destination : paths.styles.dest,
		paths : "src/css/partials",
		aux : "src/css",
		task : "styles"
	});
}

export function scripts() {
	return jsandgzip({
		source : paths.scripts.src,
		destination : paths.scripts.dest,
		basename : "app",
		ext : ".js",
		task : "scripts"
	});
}

export function downloads() {
	return copyonly({
		source: paths.downloads.src,
		destination: paths.downloads.dest,
		task : "downloads"
	});
}

export function images() {
	return copyandgzip({
		source : paths.images.src,
		destination : paths.images.dest,
		task : "images"
	});
}

export function imagesmin() {
	return compressimg({
		source : paths.images.src,
		destination : paths.images.dest,
		task : "imagesmin"
	});
}

export function video() {
	return copyandgzip({
		source : paths.video.src,
		destination : paths.video.dest,
		task : "video"
	});
}

export function audio() {
	return copyandgzip({ 
		source : paths.audio.src,
		destination : paths.audio.dest,
		task : "audio"
	});
}

export function customboostrapcss() {
	return cssminifyandgzip({
		source : paths.customboostrapcss.src,
		destination : paths.customboostrapcss.dest,
		paths : "src/css/vendor/bootstrap/scss",
		aux : "src/css/vendor/bootstrap/css",
		task : "customboostrapcss"
	});
}

export function bootstrapcss() {
	return copyandgzip({
		source : paths.bootstrapcss.src,
		destination : paths.bootstrapcss.dest,
		task : "bootstrapcss"
	});
}

export function bootstrapjs() {
	return copyandgzip({
		source : paths.bootstrapjs.src,
		destination : paths.bootstrapjs.dest,
		task : "bootstrapjs"
	});
}

export function vendorjs() {
	return copyandgzip({
		source : paths.vendorjs.src,
		destination : paths.vendorjs.dest,
		basement: "src/vendor",
		task : "vendorjs"
	});
}

export function templates() {
	return htmlandgzip({
		source : paths.templates.src,
		destination : paths.templates.dest,
		ext : ".html",
		task : "templates"
	});
}

export function fonts() {
	return copyandgzip({
		source : paths.fonts.src,
		destination : paths.fonts.dest,
		task : "fonts"
	});
}

// Execute the AppleScript to open Google Chrome Canary in incognito mode
function deploy(done) {
	const appleScriptCommand = `osascript -e 'do shell script "open -a \\"Google Chrome Canary.app\\" --new --args -incognito https://localhost:9000"'`;

	exec(appleScriptCommand, function (error, stdout, stderr) {
		if (error) {
			console.error(`Error executing AppleScript: ${stderr}`);
		} else {
			console.log(stdout);
		}
		done();
	});
}
export { deploy };

//Create localhost on port 9000 using https
function serve(done) {
	browsersync.init({
		watch: true,
		server: {
			baseDir: './dist',
			middleware: middlewares
		},
		port: 9000,
		https: {
			key: './localhost.key',
			cert: './localhost.crt'
		},
		notify: false,
		open: false,
	});
	done();
}
export { serve }

const reload = (done) => { browsersync.reload(); done() };
export { reload };

const spin = () => {
	const spincharsanticlockwise = ["-", "\\", "|", "/"];
	const spincharsclockwise = ["-", "/", "|", "\\"];
	let i = 0;

	const spinanimation = () => {
		process.stdout.write(`\r\x1b[38;2;255;171;29m${spincharsanticlockwise[i]} [:: :: :: watching for changes :: :: ::] ${spincharsclockwise[i]}\x1b[0m`);
		i = (i + 1) % spincharsanticlockwise.length;
	};

	// Return a function to start the spinner when needed
	return () => {
		// Start the spinner animation
		const spinner = setInterval(spinanimation, 100);

		// Return a function to stop the spinner when needed
		return () => clearInterval(spinner);
	};
};
export { spin };

// Start the spinner when the watch task is executed
const startspinner = spin();

//Lighthouse test in Desktop mode using AppleScript desktopTest.sh
const desktopTest = (done) => {
	const appleScriptPath = '/Users/th3k3ymast3r/dev/phoenix/desktopTest.applescript'; // Update with the actual path

	// Execute the AppleScript using osascript
	exec(`osascript ${appleScriptPath}`, (error, stdout, stderr) => {
		if (error) {
			console.error(`Error executing AppleScript: ${error}`);
			done(error); // Signal completion with error
		} else {
			console.log(stdout);
			done(); // Signal completion without error
		}
	});
};

export { desktopTest };

//Lighthouse test in Mobile mode using AppleScript mobileTest.sh
const mobileTest = (done) => {
	const appleScriptPath = '/Users/th3k3ymast3r/dev/phoenix/mobileTest.applescript';

	// Execute the AppleScript using osascript
	exec(`osascript ${appleScriptPath}`, (error, stdout, stderr) => {
		if (error) {
			console.error(`Error executing AppleScript: ${error}`);
			done(error); // Signal completion with error
		} else {
			console.log(stdout);
			done(); // Signal completion without error
		}
	});
};

export { mobileTest };

// Watch
export function watchfiles () {

	// Start the spinner animation
	const stopspinner = startspinner();
	return new Promise(resolve => {

		watch(paths.root.src, gulp.series('root', reload)).on('change', path => { 
			log({ title: 'root files update detected', message: `File ${path} was changed` });
		});
		watch(paths.sensorjs.src, gulp.series('sensorjs', reload)).on('change', path => { 
			log({ title: 'sensor js edit detected', message: `File ${path} was changed` });
		});
		watch(paths.atf.src, gulp.series('atf', reload)).on('change', path => { 
			log({ title: 'atf css edit detected', message: `File ${path} was changed` });
		});
		watch(paths.styles.src, gulp.series('styles', reload)).on('change', path => { 
			log({ title: 'app css edit detected', message: `File ${path} was changed` });
		});
		watch(paths.scripts.src, gulp.series('scripts', reload)).on('change', path => { 
			log({ title: 'scripts edit detected', message: `File ${path} was changed` });
		});
		watch(paths.templates.src, gulp.series('templates', reload)).on('change', path => { 
			log({ title: '.ejs edit detected', message: `File ${path} was changed` });
		});
		watch(paths.globalvars.src, gulp.series('globalvars', reload)).on('change', path => { 
			log({ title: 'global-css edit detected', message: `File ${path} was changed` });
		});
		watch(paths.settingshtml.src, gulp.series('settingshtml', reload)).on('change', path => { 
			log({ title: 'settings html update detected', message: `File ${path} was changed` });
		});
		watch(paths.settingssass.src, gulp.series('settingssass', reload)).on('change', path => { 
			log({ title: 'settings css edit detected', message: `File ${path} was changed` });
		});
		watch(paths.settingsjs.src, gulp.series('settingsjs', reload)).on('change', path => { 
			log({ title: 'settings js edit detected', message: `File ${path} was changed` });
		});
		watch(paths.threedmenuhtml.src, gulp.series('threedmenuhtml', reload)).on('change', path => { 
			log({ title: 'threedmenu html edit detected', message: `File ${path} was changed` });
		});
		watch(paths.threedmenusass.src, gulp.series('threedmenusass', reload)).on('change', path => { 
			log({ title: 'threedmenu sass edit detected', message: `File ${path} was changed` });
		});
		watch(paths.errorpageshtml.src, gulp.series('errorpageshtml', reload)).on('change', path => { 
			log({ title: 'errorpages html update detected', message: `File ${path} was changed` });
		});
		watch(paths.errorpagessass.src, gulp.series('errorpagessass', reload)).on('change', path => { 
			log({ title: 'errorpages css edit detected', message: `File ${path} was changed` });
		});
		watch(paths.errorpagesjs.src, gulp.series('errorpagesjs', reload)).on('change', path => { 
			log({ title: 'errorpages js edit detected', message: `File ${path} was changed` });
		});
		watch(paths.errorpagesimg.src, gulp.series('errorpagesimg', reload)).on('change', path => { 
			log({ title: 'errorpages images update detected', message: `File ${path} was changed` });
		});
		watch(paths.backgroundvdohtml.src, gulp.series('backgroundvdohtml', reload)).on('change', path => { 
			log({ title: 'backgroundvdo html update detected', message: `File ${path} was changed` });
		});
		watch(paths.backgroundvdosass.src, gulp.series('backgroundvdosass', reload)).on('change', path => { 
			log({ title: 'backgroundvdo css edit detected', message: `File ${path} was changed` });
		});
		watch(paths.backgroundvdojs.src, gulp.series('backgroundvdojs', reload)).on('change', path => { 
			log({ title: 'backgroundvdo js edit detected', message: `File ${path} was changed` });
		});
		watch(paths.backgroundvdoimg.src, gulp.series('backgroundvdoimg', reload)).on('change', path => { 
			log({ title: 'backgroundvdo images update detected', message: `File ${path} was changed` });
		});
		watch(paths.backgroundvdo.src, gulp.series('backgroundvdo', reload)).on('change', path => { 
			log({ title: 'backgroundvdo video update detected', message: `File ${path} was changed` });
		});
		watch(paths.departuresboard.src, gulp.series('departuresboard', reload)).on('change', path => { 
			log({ title: 'departuresboard edit detected', message: `File ${path} was changed` });
		});
		watch(paths.departuresboardsass.src, gulp.series('departuresboardsass', reload)).on('change', path => { 
			log({ title: 'departuresboard sass edit detected', message: `File ${path} was changed` });
		});
		watch(paths.departuresboard3p.src, gulp.series('departuresboard3p', reload)).on('change', path => { 
			log({ title: 'departuresboard 3p js edit detected', message: `File ${path} was changed` });
		});
		watch(paths.embedmediumcss.src, gulp.series('embedmediumcss', reload)).on('change', path => { 
			log({ title: 'embedmedium css edit detected', message: `File ${path} was changed` });
		});
		watch(paths.embedmediumjs.src, gulp.series('embedmediumjs', reload)).on('change', path => { 
			log({ title: 'embedmedium js edit detected', message: `File ${path} was changed` });
		});
		watch(paths.visdatahtml.src, gulp.series('visdatahtml', reload)).on('change', path => { 
			log({ title: 'visdata html update detected', message: `File ${path} was changed` });
		});
		watch(paths.visdataimg.src, gulp.series('visdataimg', reload)).on('change', path => { 
			log({ title: 'visdata images update detected', message: `File ${path} was changed` });
		});
		watch(paths.visdatasass.src, gulp.series('visdatasass', reload)).on('change', path => { 
			log({ title: 'visdata sass edit detected', message: `File ${path} was changed` });
		});
		watch(paths.visdatacss.src, gulp.series('visdatacss', reload)).on('change', path => { 
			log({ title: 'visdata css edit detected', message: `File ${path} was changed` });
		});
		watch(paths.visdatajs.src, gulp.series('visdatajs', reload)).on('change', path => { 
			log({ title: 'visdata js edit detected', message: `File ${path} was changed` });
		});
		watch(paths.visdata3p.src, gulp.series('visdata3p', reload)).on('change', path => { 
			log({ title: 'visdata 3p js edit detected', message: `File ${path} was changed` });
		});
		watch(paths.visdatafonts.src, gulp.series('visdatafonts', reload)).on('change', path => { 
			log({ title: 'visdata fonts update detected', message: `File ${path} was changed` });
		});
		watch(paths.progressmarkercss.src, gulp.series('progressmarkercss', reload)).on('change', path => { 
			log({ title: 'progressmarker css edit detected', message: `File ${path} was changed` });
		});
		watch(paths.progressmarkerjs.src, gulp.series('progressmarkerjs', reload)).on('change', path => { 
			log({ title: 'progressmarker js edit detected', message: `File ${path} was changed` });
		});
		watch(paths.testimonialshtml.src, gulp.series('testimonialshtml', reload)).on('change', path => { 
			log({ title: 'testimonials html edit detected', message: `File ${path} was changed` });
		});
		watch(paths.testimonialssass.src, gulp.series('testimonialssass', reload)).on('change', path => { 
			log({ title: 'testimonials sass edit detected', message: `File ${path} was changed` });
		});
		watch(paths.testimonialsimg.src, gulp.series('testimonialsimg', reload)).on('change', path => { 
			log({ title: 'testimonials images edit detected', message: `File ${path} was changed` });
		});
		watch(paths.twitcker.src, gulp.series('twitcker', reload)).on('change', path => { 
			log({ title: 'twitcker edit detected', message: `File ${path} was changed` });
		});
		watch(paths.video.src, gulp.series('video', reload)).on('change', path => { 
			log({ title: 'video update detected', message: `File ${path} was changed` });
		});
		watch(paths.fonts.src, gulp.series('fonts', reload)).on('change', path => { 
			log({ title: 'fonts update detected', message: `File ${path} was changed` });
		});
		watch(paths.images.src, gulp.series('images', reload)).on('change', path => { 
			log({ title: 'images update detected', message: `File ${path} was changed` });
		});
		watch(paths.downloads.src, gulp.series('downloads', reload)).on('change', path => { 
			log({ title: 'downloads file(s) update detected', message: `File ${path} was changed` });
		});

		// Clear the spinner when the watch task finishes
		process.on('exit', () => {
			stopspinner();
			resolve();
		});
	});
}

// Build
export const build = series(
    clean,
    root,
	sensorjs,
    atf,
    globalvars,
    styles,
	errorpageshtml,
	errorpagesjs,
	errorpagessass,
	errorpagesimg,
	backgroundvdohtml,
	backgroundvdojs,
	backgroundvdosass,
	backgroundvdoimg,
	backgroundvdo,
    gtranslatecss,
    gtranslatejsmin,
	elementjsmin,
	gtranslateimg,
	settingssass,
    settingsjs,
	settingshtml,
	sunrisesunset,
	sunrisesunsetdarkmode,
    bootstrapcss,
    bootstrapjs,
    embedmediumcss,
    embedmediumjs,
	testimonialshtml,
	testimonialssass,
	testimonialsimg,
	threedmenuhtml,
	threedmenusass,
	visdatafonts,
	visdatasass,
	visdatacss,
    visdatajs,
	visdata3p,
	visdatahtml,
	visdataimg,
    departuresboard,
	departuresboardsass,
	departuresboard3p,
	progressmarkercss,
	progressmarkerjs,
    twitcker,
    vendorjs,
    scripts,
    downloads,
    templates,
    fonts,
    video,
    audio
);
export { build as default };

const launch = series(build, images, serve, deploy);
export { launch };

const push = series(build, rootmin, imagesmin, gtranslatemin, serve, deploy);
export { push };