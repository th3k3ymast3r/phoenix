#!/bin/bash

SG_gold="\033[38;2;255;171;29m" # Gold
SG_reset="\033[0m"

spin() {
	spin_chars_anticlockwise=("-" "\\" "|" "/")
	spin_chars_clockwise=("-" "/" "|" "\\")
	
	while :; do
		for ((i=0; i<${#spin_chars_anticlockwise[@]}; i++)); do
			echo -en "\r${SG_gold}${spin_chars_anticlockwise[$i]} [:: :: :: watching :: :: ::] ${spin_chars_clockwise[$i]}${reset_color}"
			sleep 0.1
		done
	done
}

spin
