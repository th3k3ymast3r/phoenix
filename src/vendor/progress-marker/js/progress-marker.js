let engageScrollMarker = function(outlineArray) {
	if (debug) debugLog('* engageScrollMarker triggered *');
	if (debug) debugLog(`outline detected as: ${outlineArray}`);

	// Check if the aside is active
	if (isAside()) {
		const scrollMarker = document.querySelector('.display--content .descriptor');
		const bulletProgressContainer = document.querySelector('.bullet-progress');

		// clear existing list content inside bullet-progress
		bulletProgressContainer.innerHTML = ''; // clear only the UL content

		// add event listeners for scroll and resize events
		scrollMarker.addEventListener('scroll', event => {
			handleClasses();
		}, { passive: true });

		scrollMarker.addEventListener('resize', event => {
			handleClasses();
		}, { passive: true });

		if (debug) debugLog(`outline state: ${outlineArray}`);

		// dynamically create the <ul> and <li> elements from the array
		if (outlineArray !== false) {
			// create the <ul> element
			const ulElement = document.createElement('ul');
			bulletProgressContainer.appendChild(ulElement);

			let classes = JSON.parse(outlineArray);

			// iterate through the array to create <li> elements
			classes.forEach(className => {
				let liElement = document.createElement('li');
				liElement.classList.add(className); // Assign class from the array
				
				// replace hyphens with spaces for the data-bookmark-title attribute
				let bookmarkTitle = className.replace(/-/g, ' ');

				liElement.setAttribute('data-bookmark-title', bookmarkTitle); // set the data-bookmark-title

				ulElement.appendChild(liElement); // append each <li> to the <ul>
			});

			if (debug) debugLog('Bullet progress markers created for the provided outlineArray');
		} else {
			if (debug) debugLog('engageScrollMarker handleClasses fn ~ progress markers not required');
		}

		// set button links ~ button.js (need to cater for starOutline array)
		if (outlineArray !== false) {
			btnsBulletProgress(outlineArray);
		}

		// function to handle the classes during scrolling or resizing
		let handleClasses = function() {
			if (debug) debugLog('* handleClasses triggered *');

			if (outlineArray !== false) {
				// function to check if a class is visible in the viewport
				function checkVisibility(className) {
					let checkClassInDom = document.querySelector('section#' + className);
					if (checkClassInDom) {
						return checkClassInDom.isInViewport();
					} else {
						if (debug) debugLog(`checkVisibility fn ~ * section#${className} * not found`);
					}
				}

				let classes = JSON.parse(outlineArray);
				classes.forEach(function(className) {
					let tMark = document.querySelector('.bullet-progress .' + className);
					if (debug) debugLog(`tMark: ${tMark.className}`);

					if (tMark) {
						if (checkVisibility(className)) {
							tMark.classList.add('active');
							if (tMark.previousElementSibling) {
								tMark.previousElementSibling.classList.add('near');
							}
							if (tMark.nextElementSibling) {
								tMark.nextElementSibling.classList.add('far');
							}
						} else {
							tMark.classList.remove('active');
							if (tMark.previousElementSibling) {
								tMark.previousElementSibling.classList.remove('near');
							}
							if (tMark.nextElementSibling) {
								tMark.nextElementSibling.classList.remove('far');
							}
						}
					} else {
						if (debug) debugLog('Bullet progress classes not found in DOM');
					}
				});
			} else {
				if (debug) debugLog('engageScrollMarker handleClasses fn ~ progress markers not required');
			}
		}
	} else {
		if (debug) debugLog('Aside is not active, bullet progress not executed');
	}
};