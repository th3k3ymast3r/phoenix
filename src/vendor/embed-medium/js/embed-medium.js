let embedFeatures = function(){

	// Comparison image
	let initComparisons = (() => {
		if (debug) debugLog("* initComparisons function triggered *");

		function compareImages(img) {
			if (debug) debugLog(`compareImages fn ~ args: ${img.className}`);

			function slide(x) {
				// resize the image: 
				img.style.width = x + "px";
				// position the slider: 
				slider.style.left = img.offsetWidth - (slider.offsetWidth / 2) + "px";
			}

			function getCursorPos(e) {
				var a, x = 0;
				e = (e.changedTouches) ? e.changedTouches[0] : e;
				// get the x positions of the image: 
				a = img.getBoundingClientRect();
				// calculate the cursor's x coordinate, relative to the image: 
				x = e.pageX - a.left;
				// consider any page scrolling: 
				x = x - window.pageXOffset;
				return x;
			}

			function slideReady(e) {
				// prevent any other actions that may occur when moving over the image: 
				e.preventDefault();
				// the slider is now clicked and ready to move: 
				clicked = 1;
				// execute a function when the slider is moved: 
				window.addEventListener("mousemove", slideMove);
				window.addEventListener("touchmove", slideMove);
			}
			function slideFinish() {
				// the slider is no longer clicked: 
				clicked = 0;
			}
			function slideMove(e) {
				var pos;
				// if the slider is no longer clicked, exit this function: 
				if (clicked == 0) return false;
				// get the cursor's x position: 
				pos = getCursorPos(e);
				// prevent the slider from being positioned outside the image: 
				if (pos < 0) pos = 0;
				if (pos > 700) pos = 700;
				// execute a function that will resize the overlay image according to the cursor: 
				slide(pos);
			}

			var slider, img, clicked = 0, w, h;
			// get the width and height of the img element 
			w = 700;
			h = 244;
			// set the width of the img element to 50%:
			img.style.width = (w / 2) + "px";
			 
			img.style.width = 0 + "px";
			// create slider handle 
			handle = document.createElement("span");
			// create slider: 
			slider = document.createElement("div");
			slider.appendChild(handle);
			slider.setAttribute("class", "comp-slider");
			slider.classList.add("echo-signal", "animated", "fadeIn");
			// insert slider 
			img.parentElement.insertBefore(slider, img);
			// position the slider in the center vertically: 
			slider.style.top = (244 / 2) - (40 / 2) + "px";
			// center horizontally
				slider.style.left = (700 / 2) - (40 / 2) + "px";
			
			slider.style.left = -20 + "px";
			// execute a function when the mouse button is pressed: 
			slider.addEventListener("mousedown", slideReady);
			// and another function when the mouse button is released: 
			window.addEventListener("mouseup", slideFinish);
			// or touched (for touch screens: */
			slider.addEventListener("touchstart", slideReady);
			 // and released (for touch screens: */
			window.addEventListener("touchend", slideFinish);
		}

		let x = document.querySelector(".comp-overlay");

		let validateOverlay = function(){
			if(x !== null){
				if (debug) debugLog(`Check .comp-overlay exists: ${(x !== null)}`);
				compareImages(x);
			} else {
				// retry after a short delay if element is not found
				setTimeout(function() {
					x = document.querySelector(".comp-overlay"); // recheck the element
					validateOverlay(); // recursively call the function
				}, 100); // 100ms delay
			}
		};

		// initial call to start the process
		validateOverlay();
	})();
};