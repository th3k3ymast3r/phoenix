// READme install vis-data sample of dashboard prototype for metisx.io
// Requires Gulp V5 + Node v21.7.3
// Plugins: 
// plumber (gulp-plumber), 
// sourcemaps (gulp-sourcemaps), 
// sass (gulp-sass), 
// sassGlob (gulp-sass-glob), 
// autoprefixer (gulp-autoprefixer), 
// cleanCSS (gulp-clean-css), 
// rename (gulp-rename), 
// gzip (gulp-gzip),
// log (fancy-log)

// Paths code
// const paths = {
    // Not required if using SASS
    // visdatacss : {
	// 	src : "src/vendor/vis-data/css/*.css",
	// 	dest : "dist/assets/css/vis-data"
	// },
	visdatasass : {
		src : [
			"src/vendor/vis-data/sass/*.sass",
			"src/vendor/vis-data/sass/partials/*.scss"
		],
		dest : "dist/assets/css/vis-data"
	},
	visdatajs : {
		src : "src/vendor/vis-data/js/*.js",
		dest : "dist/assets/js/vis-data"
	},
	visdatahtml : {
		src : "src/vendor/vis-data/*.html",
		dest : "dist"
	}
// };


// Process function -> file.{sass, scss} = file.{css, gzip}
// For SASS/SCSS + GZIP files and minifying
export function cssminifyandgzip({ source, destination, paths, task }) {
	return src(source, { since : lastRun(task), encoding : false })
		.pipe(plumber({ errorHandler: handleerror }))
		.pipe(sourcemaps.init())
		.pipe(sassGlob())
		.pipe(sassCompiler({
			outputStyle: 'compressed',
			includePaths: paths,
			errLogToConsole: true
		}).on('error', sassCompiler.logError))
		.pipe(autoprefixer("last 2 version", "safari 5", "ie 8", "ie 9", "opera 12.1", "ios 6", "android 4"))
		.pipe(cleanCSS({debug: true}, (details) => {
			// Extract the relative file name without extension
			const basename = path.basename(details.name, path.extname(details.name));
			const originalsizekb = details.stats.originalSize / 1024;
			const minifiedsizekb = details.stats.minifiedSize / 1024;
			log(`${basename}.css` + `: ${originalsizekb.toFixed(3)} kb`);
			log(`${basename}.min.css` + `: \x1b[38;2;249;42;114m${minifiedsizekb.toFixed(3)} kb\x1b[0m`);
		}))
		.pipe(sourcemaps.write({
			includeContent : false,
			sourceRoot : file => `file:///Users/th3k3ymast3r/dev/phoenix/src/css`
		}))
		.pipe(rename({
			suffix: ".min"
		}))
		.pipe(dest(destination))
		.pipe(gzip())
		.pipe(dest(destination))
}

// For Copy + GZIP only
export function copyandgzip({ source, destination, basement, task }) {
	let copiedFiles = [];

	return src(source, { since : lastRun(task), base : basement, encoding : false })
		.on('data', (file) => {
			const basename = path.basename(file.relative, path.extname(file.relative));

			if (basename && !copiedFiles.includes(basename)) {
				log(`\x1b[1m⇧\x1b[0m '\x1b[38;2;102;217;239m${basename}${file.extname}\x1b[0m'`);
			}
			if (basename && basename !== '' && basename !== null && basename !== undefined && basename !== '-1') {
				copiedFiles.push(basename);
			}
		})
		.pipe(dest(destination))
		.pipe(gzip())
		.pipe(dest(destination));
}

// Export function
export function visdatafonts() {
	return copyandgzip({
		source : paths.visdatafonts.src,
		destination : paths.visdatafonts.dest,
		basement: "src/vendor/vis-data/fonts",
		task : "visdatafonts"
	});
}

export function visdatasass() {
	return cssminifyandgzip({
		source : paths.visdatasass.src,
		destination : paths.visdatasass.dest,
		paths : "src/vendor/vis-data/partials",
		task : "visdatasass"
	});
}

// Not required if using visdatasass function
// export function visdatacss() {
// 	return cssminifyandgzip({
// 		source : paths.visdatacss.src,
// 		destination : paths.visdatacss.dest,
// 		task : "visdatacss"
// 	});
// }

export function visdatajs() {
	return jsandgzip({
		source : paths.visdatajs.src,
		destination : paths.visdatajs.dest,
		basename : "vis-data",
		ext : ".js",
		task : "visdatajs"
	});
}

export function visdatahtml() {
	return copyandgzip({
		source : paths.visdatahtml.src,
		destination : paths.visdatahtml.dest,
		basement: "src/vendor/vis-data",
		task : "visdatahtml"
	});
}

// Watch function
watch(paths.visdatahtml.src, gulp.series('visdatahtml', reload)).on('change', path => { 
    log({ title: 'visdata html update detected', message: `File ${path} was changed` });
});
watch(paths.visdatasass.src, gulp.series('visdatasass', reload)).on('change', path => { 
    log({ title: 'visdata css edit detected', message: `File ${path} was changed` });
});
// Not required if using visdatasass watch
// watch(paths.visdatacss.src, gulp.series('visdatacss', reload)).on('change', path => { 
//     log({ title: 'visdata css edit detected', message: `File ${path} was changed` });
// });
watch(paths.visdatajs.src, gulp.series('visdatajs', reload)).on('change', path => { 
    log({ title: 'visdata js edit detected', message: `File ${path} was changed` });
});
watch(paths.visdatafonts.src, gulp.series('visdatafonts', reload)).on('change', path => { 
    log({ title: 'visdata fonts update detected', message: `File ${path} was changed` });
});

//Build task
// Build
// export const build = series(
	visdatafonts,
	visdatasass,
    // Not required if using visdatasass function
    // visdatacss,
    visdatajs,
	visdatahtml
// );
// export { build as default };