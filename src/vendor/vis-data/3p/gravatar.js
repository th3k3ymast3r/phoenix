document.addEventListener('DOMContentLoaded', function() {
	// Replace with the user's email address
	const email = 'leigh.laguisma@gmail.com';
	// Convert the email to lowercase
	const emailLower = email.trim().toLowerCase();
	// Generate the MD5 hash of the email
	const hash = md5(emailLower);
	// Construct the Gravatar URL
	const gravatarUrl = `https://www.gravatar.com/avatar/${hash}`;

	// Set the avatar image src attribute
	const avatarImg = document.getElementById('avatar');
	avatarImg.src = gravatarUrl;
});