function halfBake(color) {
    // Match the rgb color format and extract the color values
    const rgbMatch = color.match(/rgb\((\d+),\s*(\d+),\s*(\d+)\)/);
    if (rgbMatch) {
        const r = rgbMatch[1];
        const g = rgbMatch[2];
        const b = rgbMatch[3];
        // Return the rgba color format with 50% opacity
        return `rgba(${r}, ${g}, ${b}, 0.5)`;
    } else {
        // If the input is not in the expected format, return the original color
        return color;
    }
}

function rawBake(color) {
    // Match the rgb color format and extract the color values
    const rgbMatch = color.match(/rgb\((\d+),\s*(\d+),\s*(\d+)\)/);
    if (rgbMatch) {
        const r = rgbMatch[1];
        const g = rgbMatch[2];
        const b = rgbMatch[3];
        // Return the rgba color format with 50% opacity
        return `rgba(${r}, ${g}, ${b}, 0)`;
    } else {
        // If the input is not in the expected format, return the original color
        console.log("color not in expected format (RGB)");
        return color;
    }
}

// Function to get query variable from URL
function getQueryVariable(variable) {
    const query = window.location.search.substring(1);
    const vars = query.split("&");
    for (let i = 0; i < vars.length; i++) {
        const pair = vars[i].split("=");
        if (pair[0] === variable) {
            return pair[1];
        }
    }
    return false;
}

//debug mode
const debug = getQueryVariable("debug");
const darkmode = getQueryVariable("darkmode");

//darkmode if at or after sunset
const calculateSunriseSunset = function () {
    let d = new Date();
    let lat = "29.8010104";
    let lng = "-95.8007111";
    let z = parseInt(102);
    // local offset could be calculated with the Google TimeZone API or others
    let o = parseInt(-7);

    let katy = new SunriseSunset(d.getFullYear(), d.getMonth() + 1, d.getDate(), lat, lng, z);

    let rise = Math.floor(katy.sunriseLocalHours(o));
    let set = Math.floor(katy.sunsetLocalHours(o));

    let cTime = d.getHours();

    (set < cTime || cTime < rise) ? $('html').addClass('dark-knight') : null;

    if (debug) console.log('sunset: '+set+' sunrise: '+rise+' currentTime: '+cTime);
    (darkmode) ? $('html').addClass('dark-knight') : null;
};calculateSunriseSunset();

//Various-ables

let chartPrimaryColor = 'rgb(174, 19, 71)';
let chartSemiStopColor = halfBake(chartPrimaryColor);
let chartStopColor = rawBake(chartPrimaryColor);
let chartSecondaryColor = 'rgb(0, 167, 97)';
let chartSecondarySemiStopColor = halfBake(chartSecondaryColor);
let chartSecondaryStopColor = rawBake(chartSecondaryColor);
let chartInfoColor = '#555';
let chartNumbers = '#555';
let chartAxis = '#333';
let chartTitle = '#555';
let tooltipBackgroundColor = 'rgba(0,0,0,.80)';
let tooltipColor = '#fff';
let gridStrokeColor = '#ccc';
let predictiveColor = '#f3e600';
let lowStrokeColor = 'rgb(123, 64, 227)';
let lowStrokeMidColor = 'rgba(0, 107, 255, 0.5)';
let lowStrokeSemiColor = halfBake(lowStrokeColor);
let lowStrokeStopColor = 'rgba(0, 107, 255, 0)';
let highStrokeColor = 'rgb(255, 107, 8)';
let highStrokeMidColor = 'rgba(254, 188, 46, 0.5)';
let highStrokeSemiColor = halfBake(highStrokeColor);
let highStrokeStopColor = 'rgba(254, 188, 46, 0)';

if (debug) console.log("default colors declared");

// Function to check for the 'dark-knight' class and update colors
function updateColorsForDarkMode() {
    const htmlTag = document.documentElement;
    let darkness = htmlTag.classList.contains('dark-knight') == true; 
    if (debug) console.log("darkmode determined: "+darkness);

    if (darkness) {
        // Update color variables for dark mode
        chartPrimaryColor = 'rgb(75, 148, 194)';
        chartSemiStopColor = 'rgba(138, 116, 178, .5)';
        chartStopColor = 'rgba(138, 116, 178, .0)';
        chartSecondaryColor = 'rgb(0, 167, 97)';
        chartSecondarySemiStopColor = halfBake(chartSecondaryColor);
        chartSecondaryStopColor = rawBake(chartSecondaryColor);
        chartInfoColor = '#555';
        chartNumbers = 'rgb(89,181,152)';
        chartAxis = 'rgb(89,181,152)';
        chartTitle = '#fff';
        tooltipBackgroundColor = 'rgba(0,0,0,.80)';
        tooltipColor = '#fff';
        gridStrokeColor = '#101013';
        predictiveColor = '#f3e600';
        lowStrokeColor = 'rgb(123, 64, 227)';
        lowStrokeMidColor = 'rgba(0, 107, 255, 0.5)';
        lowStrokeSemiColor = halfBake(lowStrokeColor);
        lowStrokeStopColor = 'rgba(0, 107, 255, 0)';
        highStrokeColor = 'rgb(255, 107, 8)';
        highStrokeMidColor = 'rgba(254, 188, 46, 0.5)';
        highStrokeSemiColor = halfBake(highStrokeColor);
        highStrokeStopColor = 'rgba(254, 188, 46, 0)';
    } else {
        // Reset to original color variables
        chartPrimaryColor = 'rgb(174, 19, 71)';
        chartSemiStopColor = halfBake(chartPrimaryColor);
        chartStopColor = rawBake(chartPrimaryColor);
        chartSecondaryColor = 'rgb(0, 167, 97)';
        chartSecondarySemiStopColor = halfBake(chartSecondaryColor);
        chartSecondaryStopColor = rawBake(chartSecondaryColor);
        chartInfoColor = '#555';
        chartNumbers = '#333';
        chartAxis = '#555';
        chartTitle = '#555';
        tooltipBackgroundColor = 'rgba(0,0,0,.80)';
        tooltipColor = '#fff';
        gridStrokeColor = '#ccc';
        predictiveColor = '#555';
        lowStrokeColor = 'rgb(123, 64, 227)';
        lowStrokeMidColor = 'rgba(0, 107, 255, 0.5)';
        lowStrokeSemiColor = halfBake(lowStrokeColor);
        lowStrokeStopColor = 'rgba(0, 107, 255, 0)';
        highStrokeColor = 'rgb(255, 107, 8)';
        highStrokeMidColor = 'rgba(254, 188, 46, 0.5)';
        highStrokeSemiColor = halfBake(highStrokeColor);
        highStrokeStopColor = 'rgba(254, 188, 46, 0)';
    }
};

const tooltipBorderRadius = '8px';
const tooltipFont = 'san-serif';
const tooltipFontSize = '12px';
const tooltipPadding = '6px';

//Apply fun-ctions
function createVerticalGradient(ctx, height) {
    const gradient = ctx.createLinearGradient(0, 0, 0, height);
    gradient.addColorStop(0, chartPrimaryColor);
    gradient.addColorStop(1, chartStopColor);
    return gradient;
}

function createHorizontalGradient(ctx, width) {
    const gradient = ctx.createLinearGradient(0, 0, width, 0);
    gradient.addColorStop(0, chartStopColor);
    gradient.addColorStop(1, chartPrimaryColor);
    return gradient;
}

function createLineGradient(ctx, height, startColor, endColor) {
    const gradient = ctx.createLinearGradient(0, 0, 0, height);
    gradient.addColorStop(0, startColor);
    gradient.addColorStop(1, endColor);
    return gradient;
}

function createLineHorizontalGradient(ctx, width, startColor, endColor) {
    const gradient = ctx.createLinearGradient(0, 0, width, 0);
    gradient.addColorStop(0, startColor);
    gradient.addColorStop(1, endColor);
    return gradient;
}

function createLowGradient(ctx, height) {
    const gradient = ctx.createLinearGradient(0, 0, 0, height);
    gradient.addColorStop(0, lowStrokeColor);
    gradient.addColorStop(0.9, lowStrokeMidColor);
    gradient.addColorStop(1, lowStrokeStopColor);
    return gradient;
}

function createLowSolidGradient(ctx, height) {
    const gradient = ctx.createLinearGradient(0, 0, 0, height);
    gradient.addColorStop(0, lowStrokeColor);
    gradient.addColorStop(1, lowStrokeMidColor);
    return gradient;
}

function createHighGradient(ctx, height) {
    const gradient = ctx.createLinearGradient(0, 0, 0, height);
    gradient.addColorStop(0, highStrokeColor);
    gradient.addColorStop(0.9, highStrokeMidColor);
    gradient.addColorStop(1, highStrokeStopColor);
    return gradient;
}

function createHighSolidGradient(ctx, height) {
    const gradient = ctx.createLinearGradient(0, 0, 0, height);
    gradient.addColorStop(0, highStrokeColor);
    gradient.addColorStop(1, highStrokeMidColor);
    return gradient;
}

// Keep track of chart instances
let charts = {};

//Common options for all charts
const commonResponsive = true;
const commonPlugins = {
    legend: {
        display: false,
        position: 'bottom'
    },
    tooltip: {
        mode: 'index',
        intersect: false,
    },
};
const commonHover = {
    mode: 'nearest',
    intersect: true,
};
const commonScales = {
    x: {
        display: true,
        title: {
            display: true,
            text: 'DATES',
            font: {
                size: 10,
                weight: 'bold',
                family: 'Arial',
                textTransform: 'uppercase'
            },
            color: chartTitle
        },
        ticks: {
            font: {
                size: 10,
                weight: 'bold',
                family: 'Arial',
                textTransform: 'uppercase'
            },
            color: chartNumbers,
            major: {
                enabled: true
            }
        },
        grid: {
            color: gridStrokeColor,
            borderColor: chartAxis
        },
        borderColor: chartAxis,
        border: {
            display: true,
            color: chartAxis // Color of the x-axis line
        }
    },
    y: {
        display: true,
        title: {
            display: true,
            text: 'PRICE',
            font: {
            size: 10,
            weight: 'bold',
            family: 'Arial',
            textTransform: 'uppercase'
            },
            color: chartTitle
        },
        ticks: {
            font: {
            size: 10,
            weight: 'bold',
            family: 'Arial',
            textTransform: 'uppercase'
            },
            color: chartNumbers,
            major: {
                enabled: true
            }
        },
        grid: {
            color: gridStrokeColor,
            borderColor: chartAxis
        },
        borderColor: chartAxis,
        border: {
            display: true,
            color: chartAxis // Color of the y-axis line
        }
    }
};

//Context for non D3.js ID charts
let ctxADX = document.getElementById('adx-canvas').getContext('2d');
let ctxBar = document.getElementById('bar-canvas').getContext('2d');
let ctxLine = document.getElementById('line-canvas').getContext('2d');
let ctxMACD = document.getElementById('macd-canvas').getContext('2d');
let ctxMovingAverage = document.getElementById('moving-average-canvas').getContext('2d');
let ctxPivot = document.getElementById('pivot-canvas').getContext('2d');
let ctxRSI = document.getElementById('rsi-canvas').getContext('2d');
let ctxVolume = document.getElementById('volume-canvas').getContext('2d');

// clear all charts
function initializeContexts() {
    console.log('Initializing charts');

    ctxADX = document.getElementById('adx-canvas').getContext('2d');
    ctxATR = document.getElementById('atr-svg').innerHTML = "";
    ctxBar = document.getElementById('bar-canvas').getContext('2d');
    ctxBollinger = document.getElementById('bollinger-svg').innerHTML = "";
    ctxCandleStick = document.getElementById('candlestick-svg').innerHTML = "";
    ctxFibonacci = document.getElementById('fibonacci-svg').innerHTML = "";
    ctxIchimoku = document.getElementById('ichimoku-svg').innerHTML = "";
    ctxLine = document.getElementById('line-canvas').getContext('2d');
    ctxMACD = document.getElementById('macd-canvas').getContext('2d');
    ctxMovingAverage = document.getElementById('moving-average-canvas').getContext('2d');
    ctxPivot = document.getElementById('pivot-canvas').getContext('2d');
    ctxRSI = document.getElementById('rsi-canvas').getContext('2d');
    ctxStochastic = document.getElementById('stochastic-svg').innerHTML = "";
    ctxVolume = document.getElementById('volume-canvas').getContext('2d');
}

// charts-json.min.js

//function to return question-data source, extend play click
function getCurrentQuestionKey() {
    return 'santaRally'; // Placeholder, update with actual logic
}

function applyLineGradientToData(data, ctx) {

    data.datasets = data.datasets.map((dataset) => {
        let gradient;
        if (dataset.label === 'RSI (Between)') {
            return {
                ...dataset,
                backgroundColor: createLineGradient(ctx, ctx.canvas.clientHeight, chartSemiStopColor, chartStopColor),
                borderColor: createLineHorizontalGradient(ctx, ctx.canvas.clientWidth, chartStopColor, chartPrimaryColor),
                borderWidth: 3,
                fill: true,
                tension: 0.1,
                pointRadius: 3,
                order: 5
            };
        } else if (dataset.label === 'RSI (Above)') {
            return {
                ...dataset,
                backgroundColor: createHighGradient(ctx, ctx.canvas.clientHeight),
                borderColor: createHighGradient(ctx, ctx.canvas.clientHeight),
                borderWidth: 2,
                fill: true,
                tension: 0.1,
                pointRadius: 3,
                order: 4
            };
        } else if (dataset.label === 'RSI (Below)') {
            return {
                ...dataset,
                backgroundColor: createLowGradient(ctx, ctx.canvas.clientHeight),
                borderColor: createLowGradient(ctx, ctx.canvas.clientHeight),
                borderWidth: 2,
                fill: true,
                tension: 0.1,
                pointRadius: 3,
                pointBackgroundColor: lowStrokeColor,
                order: 3
            };
        } else if (dataset.label === 'Upper Limit') {
            return {
                ...dataset,
                borderColor: highStrokeSemiColor,
                borderWidth: 1,
                borderDash: [5, 5],
                fill: false,
                pointRadius: 0,
                order: 2
            }
        } else if (dataset.label === 'Lower Limit') {
            return {
                ...dataset,
                borderColor: lowStrokeSemiColor,
                borderWidth: 1,
                borderDash: [5, 5],
                fill: false,
                pointRadius: 0,
                order: 1
            }
        }

        if (gradient) {
            return {
                ...dataset,
                backgroundColor: gradient,
                borderColor: gradient,
                fill: true,
                pointBackgroundColor: dataset.borderColor,
                pointBorderColor: dataset.borderColor
            };
        }

        return dataset;
    });

    return data;
}

function createAtrChart(data, containerId) {
    const margin = { top: 8, right: 16, bottom: 36, left: 42 },
        width = 352 - margin.left - margin.right,
        height = 226 - margin.top - margin.bottom;

    // Clear previous chart content
    d3.select(containerId).selectAll('*').remove();

    const svg = d3.select(containerId)
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    const x = d3.scaleBand()
        .domain(data.labels)
        .range([0, width])
        .padding(0.1);

    const y = d3.scaleLinear()
        .domain([0, 220]) // Adjust according to your data range
        .range([height, 0]);

    const yATR = d3.scaleLinear()
        .domain([0, 40]) // ATR line range
        .range([height, 0]);

    const xAxis = d3.axisBottom(x).tickValues(data.labels.filter((_, i) => i % 2 === 0));
    const yAxis = d3.axisLeft(y);

    //Add gridlines
    svg.append("g")
        .attr("class", "grid")
        .attr("transform", "translate(0," + height + ")")
        .style("stroke", gridStrokeColor)
        .call(d3.axisBottom(x)
            .tickSize(-height)
            .tickFormat("")
        )
        .selectAll("line")
        .attr("stroke", gridStrokeColor);

    svg.append("g")
        .attr("class", "grid")
        .call(d3.axisLeft(y)
            .tickSize(-width)
            .tickFormat("")
        )
        .selectAll("line")
        .attr("stroke", gridStrokeColor);

    // Add the x-axis
    svg.append("g")
        .attr("transform", "translate(0," + height + ")")
        .call(xAxis)
        .selectAll("path, line")
        .attr("stroke", chartAxis);  // Set axis lines and ticks color

    // Add the y-axis
    svg.append("g")
        .call(yAxis)
        .selectAll("path, line")
        .attr("stroke", chartAxis);  // Set axis lines and ticks color

    // Add the x-axis title
    svg.append("text")
        .attr("x", width / 2)
        .attr("y", height + margin.bottom - 5)
        .attr("fill", chartTitle)
        .style("text-anchor", "middle")
        .style("font-family", "Arial")
        .style("font-size", "10px")
        .style("font-weight", "bold")
        .text("1WK TRADING TIME (HRS)");

    // Add the y-axis title
    svg.append("text")
        .attr("transform", "rotate(-90)")
        .attr("x", -height / 2)
        .attr("y", -margin.left + 11)
        .attr("fill", chartTitle)
        .style("text-anchor", "middle")
        .style("font-family", "Arial")
        .style("font-size", "10px")
        .style("font-weight", "bold")
        .text("PRICE");

    // Tooltip
    const tooltip = d3.select("body").append("div")
        .attr("class", "tooltip")
        .style("background", tooltipBackgroundColor)
        .style("border", "0px")
        .style("border-radius", tooltipBorderRadius)
        .style("color", tooltipColor)
        .style("font-size", "12px")
        .style("opacity", 0)
        .style("padding", "2px")
        .style("pointer-events", "none")
        .style("position", "absolute");

    // Add the Price Line (candlestick)
    const candlestick = svg.selectAll(".candlestick")
        .data(data.datasets[0].data)
        .enter()
        .append("g")
        .attr("class", "candlestick");

    candlestick.each(function (d, i) {
        let wickColor = chartSemiStopColor;
        let candleColor = chartPrimaryColor;

        if (i > 0 && d.c > data.datasets[0].data[i - 1].c) {
            wickColor = chartSecondarySemiStopColor;
            candleColor = chartSecondaryColor;
        }

        // Draw wicks
        d3.select(this)
            .append("line")
            .attr("x1", x(d.hour) + x.bandwidth() / 2)
            .attr("x2", x(d.hour) + x.bandwidth() / 2)
            .attr("y1", y(d.h))
            .attr("y2", y(d.l))
            .attr("stroke", wickColor);

        // Draw top wicks
        d3.select(this)
            .append("line")
            .attr("x1", x(d.hour) + x.bandwidth() / 2)
            .attr("x2", x(d.hour) + x.bandwidth() / 2)
            .attr("y1", y(Math.max(d.o, d.c)) - 8)
            .attr("y2", y(Math.max(d.o, d.c)))
            .attr("stroke", wickColor);

        // Draw bottom wicks
        d3.select(this)
            .append("line")
            .attr("x1", x(d.hour) + x.bandwidth() / 2)
            .attr("x2", x(d.hour) + x.bandwidth() / 2)
            .attr("y1", y(Math.min(d.o, d.c)))
            .attr("y2", y(Math.min(d.o, d.c)) + 8)
            .attr("stroke", wickColor);

        d3.select(this)
            .append("rect")
            .attr("x", x(d.hour))
            .attr("y", y(Math.max(d.o, d.c)))
            .attr("width", x.bandwidth())
            .attr("height", Math.abs(y(d.o) - y(d.c)))
            .attr("fill", d.o > d.c ? chartStopColor : candleColor);

        // Add transparent overlay rectangle for larger mouseover area
        d3.select(this)
            .append("rect")
            .attr("x", x(d.hour) - x.bandwidth() / 2)
            .attr("width", x.bandwidth() * 2)
            .attr("y", 0)
            .attr("height", height)
            .attr("fill", "none")
            .attr("pointer-events", "all")
            .on("mouseover", function (event) {
                tooltip.transition()
                    .duration(200)
                    .style("opacity", 1)
                    .style("background-color", tooltipBackgroundColor)
                    .style("color", tooltipColor)
                    .style("font-family", "inherit")
                    .style("font-size", tooltipFontSize)
                    .style("border-radius", tooltipBorderRadius)
                    .style("width", "fit-content")
                    .style("padding", tooltipPadding)
                    .style("position", "absolute");
                tooltip.html(`<strong>Hour: ${d.hour}</strong><br>Open: ${d.o}<br>High: ${d.h}<br>Low: ${d.l}<br>Close: ${d.c}`)
                    .style("left", (event.pageX + 5) + "px")
                    .style("top", (event.pageY - 28) + "px");
            })
            .on("mouseout", function () {
                tooltip.transition()
                    .duration(500)
                    .style("opacity", 0);
            });
    });

    const addLineWithTooltip = (lineData, strokeColor, label) => {
        svg.append("path")
            .datum(lineData.map((d, i) => ({ value: d, label: data.labels[i] })))
            .attr("class", "line")
            .attr("d", d3.line()
                .x(d => x(d.label) + x.bandwidth() / 2)
                .y(d => y(d.value))
            )
            .attr("fill", "none")
            .attr("stroke", strokeColor)
            .attr("stroke-width", 1)
            .on("mouseover", function (event, d) {
                const [xPos] = d3.pointer(event);
                const index = Math.floor(xPos / (width / data.labels.length));
                tooltip.transition()
                    .duration(200)
                    .style("opacity", 1)
                    .style("background-color", tooltipBackgroundColor)
                    .style("color", tooltipColor)
                    .style("font-family", "inherit")
                    .style("font-size", tooltipFontSize)
                    .style("border-radius", tooltipBorderRadius)
                    .style("width", "fit-content")
                    .style("padding", tooltipPadding)
                    .style("position", "absolute");
                tooltip.html(`<div style="display: flex; align-items: center;"><div style="width: 12px; height: 12px; background-color: ${strokeColor}; margin-right: 5px;"></div><strong>${label}: ${lineData[index]}</strong></div>`)
                    .style("left", (event.pageX + 5) + "px")
                    .style("top", (event.pageY - 28) + "px");
            })
            .on("mouseout", function () {
                tooltip.transition()
                    .duration(500)
                    .style("opacity", 0);
            });
    };

    // Add the Activity Line
    addLineWithTooltip(data.datasets[1].data, highStrokeColor, 'Activity');

    // Add the ATR Line
    addLineWithTooltip(data.datasets[2].data, lowStrokeColor, 'ATR');
}

function createBollingerChart(data, containerId) {
    const margin = { top: 8, right: 16, bottom: 36, left: 42 },
        width = 352 - margin.left - margin.right,
        height = 226 - margin.top - margin.bottom;

    // Clear previous chart content
    d3.select(containerId).selectAll('*').remove();

    const svg = d3.select(containerId)
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    const x = d3.scaleBand()
        .domain(data.labels)
        .range([0, width])
        .padding(0.1);

    const y = d3.scaleLinear()
        .domain([0, 220]) // Adjust according to your data range
        .range([height, 0]);

    const xAxis = d3.axisBottom(x).tickValues(data.labels.filter((_, i) => i % 2 === 0));
    const yAxis = d3.axisLeft(y);

   //Add gridlines
   svg.append("g")
    .attr("class", "grid")
    .attr("transform", "translate(0," + height + ")")
    .style("stroke", gridStrokeColor)
    .call(d3.axisBottom(x)
        .tickSize(-height)
        .tickFormat("")
    )
    .selectAll("line")
    .attr("stroke", gridStrokeColor);

    svg.append("g")
    .attr("class", "grid")
    .call(d3.axisLeft(y)
        .tickSize(-width)
        .tickFormat("")
    )
    .selectAll("line")
    .attr("stroke", gridStrokeColor);

    // Add the x-axis
    svg.append("g")
        .attr("transform", "translate(0," + height + ")")
        .call(xAxis)
        .selectAll("path, line")
        .attr("stroke", chartAxis);  // Set axis lines and ticks color

    // Add the y-axis
    svg.append("g")
        .call(yAxis)
        .selectAll("path, line")
        .attr("stroke", chartAxis);  // Set axis lines and ticks color

    // Add the x-axis title
    svg.append("text")
        .attr("x", width / 2)
        .attr("y", height + margin.bottom - 5)
        .attr("fill", chartTitle)
        .style("text-anchor", "middle")
        .style("font-family", "Arial")
        .style("font-size", "10px")
        .style("font-weight", "bold")
        .text("1WK TRADING TIME (HRS)");

    // Add the y-axis title
    svg.append("text")
        .attr("transform", "rotate(-90)")
        .attr("x", -height / 2)
        .attr("y", -margin.left + 11)
        .attr("fill", chartTitle)
        .style("text-anchor", "middle")
        .style("font-family", "Arial")
        .style("font-size", "10px")
        .style("font-weight", "bold")
        .text("PRICE");

    // Tooltip
    const tooltip = d3.select("body").append("div")
        .attr("class", "tooltip")
        .style("background", tooltipBackgroundColor)
        .style("border", "0px")
        .style("border-radius", tooltipBorderRadius)
        .style("color", tooltipColor)
        .style("font-size", "12px")
        .style("opacity", 0)
        .style("padding", "2px")
        .style("pointer-events", "none")
        .style("position", "absolute");

    // Add the Price Line (candlestick)
    const candlestick = svg.selectAll(".candlestick")
        .data(data.datasets[0].data)
        .enter()
        .append("g")
        .attr("class", "candlestick");

    candlestick.each(function (d, i) {
        let wickColor = chartSemiStopColor;
        let candleColor = chartPrimaryColor;

        if (i > 0 && d.c > data.datasets[0].data[i - 1].c) {
            wickColor = chartSecondarySemiStopColor;
            candleColor = chartSecondaryColor;
        }

        // Draw wicks
        d3.select(this)
            .append("line")
            .attr("x1", x(d.hour) + x.bandwidth() / 2)
            .attr("x2", x(d.hour) + x.bandwidth() / 2)
            .attr("y1", y(d.h))
            .attr("y2", y(d.l))
            .attr("stroke", wickColor);

        // Draw top wicks
        d3.select(this)
            .append("line")
            .attr("x1", x(d.hour) + x.bandwidth() / 2)
            .attr("x2", x(d.hour) + x.bandwidth() / 2)
            .attr("y1", y(Math.max(d.o, d.c)) - 8)
            .attr("y2", y(Math.max(d.o, d.c)))
            .attr("stroke", wickColor);

        // Draw bottom wicks
        d3.select(this)
            .append("line")
            .attr("x1", x(d.hour) + x.bandwidth() / 2)
            .attr("x2", x(d.hour) + x.bandwidth() / 2)
            .attr("y1", y(Math.min(d.o, d.c)))
            .attr("y2", y(Math.min(d.o, d.c)) + 16)
            .attr("stroke", wickColor);

        d3.select(this)
            .append("rect")
            .attr("x", x(d.hour))
            .attr("y", y(Math.max(d.o, d.c)))
            .attr("width", x.bandwidth())
            .attr("height", Math.abs(y(d.o) - y(d.c)) * 3) // Make the rectangles three times taller
            .attr("fill", d.o > d.c ? chartStopColor : candleColor);

        // Add transparent overlay rectangle for larger mouseover area
        d3.select(this)
            .append("rect")
            .attr("x", x(d.hour) - x.bandwidth() / 2)
            .attr("width", x.bandwidth() * 2)
            .attr("y", 0)
            .attr("height", height)
            .attr("fill", "none")
            .attr("pointer-events", "all")
            .on("mouseover", function (event) {
                tooltip.transition()
                    .duration(200)
                    .style("opacity", 1)
                    .style("background-color", tooltipBackgroundColor)
                    .style("color", tooltipColor)
                    .style("font-family", "inherit")
                    .style("font-size", tooltipFontSize)
                    .style("border-radius", tooltipBorderRadius)
                    .style("width", "fit-content")
                    .style("padding", tooltipPadding)
                    .style("position", "absolute");
                tooltip.html(`<strong>Hour: ${d.hour}</strong><br>Open: ${d.o}<br>High: ${d.h}<br>Low: ${d.l}<br>Close: ${d.c}`)
                    .style("left", (event.pageX + 5) + "px")
                    .style("top", (event.pageY - 28) + "px");
            })
            .on("mouseout", function () {
                tooltip.transition()
                    .duration(500)
                    .style("opacity", 0);
            });
    });

    const addLineWithTooltip = (lineData, strokeColor, label) => {
        svg.append("path")
            .datum(lineData.map((d, i) => ({ value: d, label: data.labels[i] })))
            .attr("class", "line")
            .attr("d", d3.line()
                .x(d => x(d.label) + x.bandwidth() / 2)
                .y(d => y(d.value))
            )
            .attr("fill", "none")
            .attr("stroke", strokeColor)
            .attr("stroke-width", 1)
            .on("mouseover", function (event, d) {
                const [xPos] = d3.pointer(event);
                const index = Math.floor(xPos / (width / data.labels.length));
                tooltip.transition()
                    .duration(200)
                    .style("opacity", 1)
                    .style("background-color", tooltipBackgroundColor)
                    .style("color", tooltipColor)
                    .style("font-family", "inherit")
                    .style("font-size", tooltipFontSize)
                    .style("border-radius", tooltipBorderRadius)
                    .style("width", "fit-content")
                    .style("padding", tooltipPadding)
                    .style("position", "absolute");
                tooltip.html(`<div style="display: flex; align-items: center;"><div style="width: 12px; height: 12px; background-color: ${strokeColor}; margin-right: 5px;"></div><strong>${label}: ${lineData[index]}</strong></div>`)
                    .style("left", (event.pageX + 5) + "px")
                    .style("top", (event.pageY - 28) + "px");
            })
            .on("mouseout", function () {
                tooltip.transition()
                    .duration(500)
                    .style("opacity", 0);
            });
    };

    // Add the Upper Band Line
    addLineWithTooltip(data.datasets[1].data, chartSecondaryColor, 'Upper Band');

    // Add the Lower Band Line
    addLineWithTooltip(data.datasets[2].data, chartPrimaryColor, 'Lower Band');

    // Define the area generator for the cloud
    const area = d3.area()
        .x((d, i) => x(data.labels[i]) + x.bandwidth() / 2)
        .y0(d => y(d.upperBand < d.lowerBand ? d.upperBand : d.lowerBand)) // Senkou Span A
        .y1(d => y(d.upperBand > d.lowerBand ? d.upperBand : d.lowerBand)); // Senkou Span B

    // Prepare the data for the area generator
    const cloudData = data.labels.map((label, i) => ({
        label,
        upperBand: data.datasets[1].data[i],
        lowerBand: data.datasets[2].data[i]
    }));

    // Add the gradient definition
    const gradient = svg.append("defs")
        .append("linearGradient")
        .attr("id", "bollinger-gradient")
        .attr("x1", "0%")
        .attr("y1", "0%")
        .attr("x2", "0%")
        .attr("y2", "100%");

    gradient.append("stop")
        .attr("offset", "0%")
        .attr("stop-color", chartPrimaryColor)
        .attr("stop-opacity", 1);

    gradient.append("stop")
        .attr("offset", "100%")
        .attr("stop-color", chartStopColor)
        .attr("stop-opacity", 1);

    // Add the cloud area
    svg.append("path")
        .datum(cloudData)
        .attr("class", "area")
        .attr("d", area)
        .style("fill", "url(#bollinger-gradient)");
}

function createCandlestickChart(data, containerId) {
    const margin = { top: 8, right: 16, bottom: 36, left: 42 },
        width = 352 - margin.left - margin.right,
        height = 226 - margin.top - margin.bottom;

    // Clear previous chart content
    d3.select(containerId).selectAll('*').remove();

    const svg = d3.select(containerId)
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    const x = d3.scaleBand()
        .domain(data.labels)
        .range([0, width])
        .padding(0.1);

    const y = d3.scaleLinear()
        .domain([0, 220]) // Adjust according to your data range
        .range([height, 0]);

    const xAxis = d3.axisBottom(x).tickValues(data.labels.filter((_, i) => i % 2 === 0));
    const yAxis = d3.axisLeft(y);

    //Add gridlines
    svg.append("g")
        .attr("class", "grid")
        .attr("transform", "translate(0," + height + ")")
        .style("stroke", gridStrokeColor)
        .call(d3.axisBottom(x)
            .tickSize(-height)
            .tickFormat("")
        )
        .selectAll("line")
        .attr("stroke", gridStrokeColor);

    svg.append("g")
        .attr("class", "grid")
        .call(d3.axisLeft(y)
            .tickSize(-width)
            .tickFormat("")
        )
        .selectAll("line")
        .attr("stroke", gridStrokeColor);

    // Add the x-axis
    svg.append("g")
        .attr("transform", "translate(0," + height + ")")
        .call(xAxis)
        .selectAll("path, line")
        .attr("stroke", chartAxis);  // Set axis lines and ticks color

    // Add the y-axis
    svg.append("g")
        .call(yAxis)
        .selectAll("path, line")
        .attr("stroke", chartAxis);  // Set axis lines and ticks color

    // Add the x-axis title
    svg.append("text")
        .attr("x", width / 2)
        .attr("y", height + margin.bottom - 5)
        .attr("fill", chartTitle)
        .style("text-anchor", "middle")
        .style("font-family", "Arial")
        .style("font-size", "10px")
        .style("font-weight", "bold")
        .text("1WK TRADING TIME (HRS)");

    // Add the y-axis title
    svg.append("text")
        .attr("transform", "rotate(-90)")
        .attr("x", -height / 2)
        .attr("y", -margin.left + 11)
        .attr("fill", chartTitle)
        .style("text-anchor", "middle")
        .style("font-size", "10px")
        .text("PRICE");

    // Tooltip
    const tooltip = d3.select("body").append("div")
        .attr("class", "tooltip")
        .style("background", tooltipBackgroundColor)
        .style("border", "0px")
        .style("border-radius", tooltipBorderRadius)
        .style("color", tooltipColor)
        .style("font-size", "12px")
        .style("opacity", 0)
        .style("padding", "2px")
        .style("pointer-events", "none")
        .style("position", "absolute");

    // Add the Kijun Line (candlestick)
    const candlestick = svg.selectAll(".candlestick")
        .data(data.datasets[0].data)
        .enter()
        .append("g")
        .attr("class", "candlestick");

    candlestick.each(function (d, i) {
        let wickColor = chartSemiStopColor;
        let candleColor = chartPrimaryColor;

        if (i > 0 && d.c > data.datasets[0].data[i - 1].c) {
            wickColor = chartSecondarySemiStopColor;
            candleColor = chartSecondaryColor;
        }

        // Draw wicks
        d3.select(this)
            .append("line")
            .attr("x1", x(d.hour) + x.bandwidth() / 2)
            .attr("x2", x(d.hour) + x.bandwidth() / 2)
            .attr("y1", y(d.h))
            .attr("y2", y(d.l))
            .attr("stroke", wickColor);

        // Draw top wicks
        d3.select(this)
            .append("line")
            .attr("x1", x(d.hour) + x.bandwidth() / 2)
            .attr("x2", x(d.hour) + x.bandwidth() / 2)
            .attr("y1", y(Math.max(d.o, d.c)) - 8)
            .attr("y2", y(Math.max(d.o, d.c)))
            .attr("stroke", wickColor);

        // Draw bottom wicks
        d3.select(this)
            .append("line")
            .attr("x1", x(d.hour) + x.bandwidth() / 2)
            .attr("x2", x(d.hour) + x.bandwidth() / 2)
            .attr("y1", y(Math.min(d.o, d.c)))
            .attr("y2", y(Math.min(d.o, d.c)) + 16)
            .attr("stroke", wickColor);

        d3.select(this)
            .append("rect")
            .attr("x", x(d.hour))
            .attr("y", y(Math.max(d.o, d.c)))
            .attr("width", x.bandwidth())
            .attr("height", Math.abs(y(d.o) - y(d.c)) * 3) // Make the rectangles three times taller
            .attr("fill", d.o > d.c ? chartStopColor : candleColor);

        // Add transparent overlay rectangle for larger mouseover area
        d3.select(this)
            .append("rect")
            .attr("x", x(d.hour) - x.bandwidth() / 2)
            .attr("width", x.bandwidth() * 2)
            .attr("y", 0)
            .attr("height", height)
            .attr("fill", "none")
            .attr("pointer-events", "all")
            .on("mouseover", function (event) {
                tooltip.transition()
                    .duration(200)
                    .style("opacity", 1)
                    .style("background-color", tooltipBackgroundColor)
                    .style("color", tooltipColor)
                    .style("font-family", "inherit")
                    .style("font-size", tooltipFontSize)
                    .style("border-radius", tooltipBorderRadius)
                    .style("width", "fit-content")
                    .style("padding", tooltipPadding)
                    .style("position", "absolute");
                tooltip.html(`<strong>Hour: ${d.hour}</strong><br>Open: ${d.o}<br>High: ${d.h}<br>Low: ${d.l}<br>Close: ${d.c}`)
                    .style("left", (event.pageX + 5) + "px")
                    .style("top", (event.pageY - 28) + "px");
            })
            .on("mouseout", function () {
                tooltip.transition()
                    .duration(500)
                    .style("opacity", 0);
            });
    });
}

function createFibonacciChart(data, containerId) {
    const margin = { top: 8, right: 16, bottom: 36, left: 42 },
        width = 352 - margin.left - margin.right,
        height = 226 - margin.top - margin.bottom;

    // Clear previous chart content
    d3.select(containerId).selectAll('*').remove();

    const svg = d3.select(containerId)
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    const parseDate = d3.timeParse("%H");

    // Parse the dates in the data and handle potential errors
    try {
        const parsedData = data.map(d => ({ ...d, date: parseDate(d.hour) }));

        const x = d3.scaleBand()
            .domain(parsedData.map(d => d.date))
            .range([0, width])
            .padding(0.1);

        const y = d3.scaleLinear()
            .domain([d3.min(parsedData, d => d.l), d3.max(parsedData, d => d.h)])
            .nice()
            .range([height, 0]);

        const xAxis = d3.axisBottom(x)
            .tickFormat((d, i) => (i % 2 === 0 ? i + 1 : '')); // Display every other number
        const yAxis = d3.axisLeft(y);

        //Add gridlines
        svg.append("g")
            .attr("class", "grid")
            .attr("transform", "translate(0," + height + ")")
            .style("stroke", gridStrokeColor)
            .call(d3.axisBottom(x)
                .tickSize(-height)
                .tickFormat("")
            )
            .selectAll("line")
            .attr("stroke", gridStrokeColor);

        svg.append("g")
            .attr("class", "grid")
            .call(d3.axisLeft(y)
                .tickSize(-width)
                .tickFormat("")
            )
            .selectAll("line")
            .attr("stroke", gridStrokeColor);

        svg.append("g")
            .attr("transform", "translate(0," + height + ")")
            .call(xAxis)
            .append("text")
            .attr("y", 30) // Adjust y position of x-axis title
            .attr("x", width / 2)
            .attr("dy", "0.3em")
            .style("text-anchor", "middle")
            .style("font-family", "Arial")
            .style("font-size", "10px")
            .style("font-weight", "bold")
            .style("fill", chartTitle)
            .attr("class", "axis-title")
            .text("1WK TRADING TIME (HRS)");

        svg.append("g")
            .call(yAxis)
            .append("text")
            .attr("transform", "rotate(-90)")
            .attr("y", -36)
            .attr("x", -height / 2)
            .attr("dy", ".71em")
            .style("text-anchor", "middle")
            .style("font-family", "Arial")
            .style("font-size", "10px")
            .style("font-weight", "bold")
            .style("fill", chartTitle)
            .attr("class", "axis-title")
            .text("PRICE");

        // Create area gradient
        const area = d3.area()
            .x(d => x(d.date) + x.bandwidth() / 2)
            .y0(height)
            .y1(d => y(d.c));

        svg.append("path")
            .datum(parsedData)
            .attr("fill", "url(#area-gradient)")
            .attr("d", area);

        // Define the gradient
        svg.append("defs").append("linearGradient")
            .attr("id", "area-gradient")
            .attr("x1", "0%")
            .attr("y1", "0%")
            .attr("x2", "0%")
            .attr("y2", "100%")
            .selectAll("stop")
            .data([
                { offset: "0%", color: chartSemiStopColor, opacity: 1 },
                { offset: "100%", color: chartStopColor, opacity: 0 }
            ])
            .enter().append("stop")
            .attr("offset", d => d.offset)
            .attr("stop-color", d => d.color)
            .attr("stop-opacity", d => d.opacity);

        // Tooltip
        const tooltip = d3.select("body").append("div")
            .attr("class", "tooltip")
            .style("position", "absolute")
            .style("width", "120px")
            .style("height", "fit-content")
            .style("padding", "2px")
            .style("font-size", tooltipFontSize)
            .style("font-family", tooltipFont)
            .style("background", tooltipBackgroundColor)
            .style("border", "0px")
            .style("border-radius", tooltipBorderRadius)
            .style("pointer-events", "none")
            .style("opacity", 0);

        const candlestick = svg.selectAll(".candlestick")
            .data(parsedData)
            .enter()
            .append("g")
            .attr("class", "candlestick");

        candlestick.each(function (d, i) {
            let wickColor = chartSemiStopColor;
            let candleColor = chartPrimaryColor;

            if (i > 0 && d.c > parsedData[i - 1].c) {
                wickColor = chartSecondarySemiStopColor;
                candleColor = chartSecondaryColor;
            }

            // Draw wicks
            d3.select(this)
                .append("line")
                .attr("x1", x(d.date) + x.bandwidth() / 2)
                .attr("x2", x(d.date) + x.bandwidth() / 2)
                .attr("y1", y(d.h))
                .attr("y2", y(d.l))
                .attr("stroke", wickColor);

            // Draw top wicks
            d3.select(this)
                .append("line")
                .attr("x1", x(d.date) + x.bandwidth() / 2)
                .attr("x2", x(d.date) + x.bandwidth() / 2)
                .attr("y1", y(Math.max(d.o, d.c)) - 8)
                .attr("y2", y(Math.max(d.o, d.c)))
                .attr("stroke", wickColor);

            // Draw bottom wicks
            d3.select(this)
                .append("line")
                .attr("x1", x(d.date) + x.bandwidth() / 2)
                .attr("x2", x(d.date) + x.bandwidth() / 2)
                .attr("y1", y(Math.min(d.o, d.c)))
                .attr("y2", y(Math.min(d.o, d.c)) + 8)
                .attr("stroke", wickColor);

            d3.select(this)
                .append("rect")
                .attr("x", x(d.date))
                .attr("y", y(Math.max(d.o, d.c)))
                .attr("width", x.bandwidth())
                .attr("height", Math.abs(y(d.o) - y(d.c)))
                .attr("fill", d.o > d.c ? chartStopColor : candleColor);

            // Add transparent overlay rectangle for larger mouseover area
            d3.select(this)
                .append("rect")
                .attr("x", x(d.date) - x.bandwidth() / 2)
                .attr("width", x.bandwidth() * 2)
                .attr("y", 0)
                .attr("height", height)
                .attr("fill", "none")
                .attr("pointer-events", "all")
                .on("mouseover", function (event) {
                    tooltip.transition()
                        .duration(200)
                        .style("opacity", 1)
                        .style("background-color", tooltipBackgroundColor)
                        .style("color", tooltipColor)
                        .style("font-family", "inherit")
                        .style("font-size", tooltipFontSize)
                        .style("border-radius", tooltipBorderRadius)
                        .style("width", "fit-content")
                        .style("padding", tooltipPadding)
                        .style("position", "absolute");
                    tooltip.html(`<strong>${d3.timeFormat("%H")(d.date)}</strong><br>Open: ${d.o}<br>High: ${d.h}<br>Low: ${d.l}<br>Close: ${d.c}`)
                        .style("left", (event.pageX + 5) + "px")
                        .style("top", (event.pageY - 28) + "px");
                })
                .on("mouseout", function () {
                    tooltip.transition()
                        .duration(500)
                        .style("opacity", 0);
                });
        });
    } catch (error) {
        console.error("Error parsing data for Fibonacci chart:", error);
    }
}

function createIchimokuChart(data, containerId) {
    const margin = { top: 8, right: 16, bottom: 36, left: 42 },
        width = 352 - margin.left - margin.right,
        height = 226 - margin.top - margin.bottom;

    // Clear previous chart content
    d3.select(containerId).selectAll('*').remove();

    const svg = d3.select(containerId)
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    const x = d3.scaleBand()
        .domain(data.labels)
        .range([0, width])
        .padding(0.1);

    const y = d3.scaleLinear()
        .domain([0, 220]) // Adjust according to your data range
        .range([height, 0]);

    const xAxis = d3.axisBottom(x).tickValues(data.labels.filter((_, i) => i % 2 === 0));
    const yAxis = d3.axisLeft(y);

    //Add gridlines
    svg.append("g")
        .attr("class", "grid")
        .attr("transform", "translate(0," + height + ")")
        .style("stroke", gridStrokeColor)
        .call(d3.axisBottom(x)
            .tickSize(-height)
            .tickFormat("")
        )
        .selectAll("line")
        .attr("stroke", gridStrokeColor);

    svg.append("g")
        .attr("class", "grid")
        .call(d3.axisLeft(y)
            .tickSize(-width)
            .tickFormat("")
        )
        .selectAll("line")
        .attr("stroke", gridStrokeColor);

    // Add the x-axis
    svg.append("g")
        .attr("transform", "translate(0," + height + ")")
        .call(xAxis)
        .selectAll("path, line")
        .attr("stroke", chartAxis);  // Set axis lines and ticks color

    // Add the y-axis
    svg.append("g")
        .call(yAxis)
        .selectAll("path, line")
        .attr("stroke", chartAxis);  // Set axis lines and ticks color

    // Add the x-axis title
    svg.append("text")
        .attr("x", width / 2)
        .attr("y", height + margin.bottom - 5)
        .attr("fill", chartTitle)
        .style("text-anchor", "middle")
        .style("font-family", "Arial")
        .style("font-size", "10px")
        .style("font-weight", "bold")
        .text("1WK TRADING TIME (HRS)");

    // Add the y-axis title
    svg.append("text")
        .attr("transform", "rotate(-90)")
        .attr("x", -height / 2)
        .attr("y", -margin.left + 11)
        .attr("fill", chartTitle)
        .style("text-anchor", "middle")
        .style("font-family", "Arial")
        .style("font-size", "10px")
        .style("font-weight", "bold")
        .text("PRICE");

    // Tooltip
    const tooltip = d3.select("body").append("div")
        .attr("class", "tooltip")
        .style("background", tooltipBackgroundColor)
        .style("border", "0px")
        .style("border-radius", tooltipBorderRadius)
        .style("color", tooltipColor)
        .style("font-size", "12px")
        .style("opacity", 0)
        .style("padding", "2px")
        .style("pointer-events", "none")
        .style("position", "absolute");

    // Add the Kijun Line (candlestick)
    const candlestick = svg.selectAll(".candlestick")
        .data(data.datasets[0].data)
        .enter()
        .append("g")
        .attr("class", "candlestick");

    candlestick.each(function (d, i) {
        let wickColor = chartSemiStopColor;
        let candleColor = chartPrimaryColor;

        if (i > 0 && d.c > data.datasets[0].data[i - 1].c) {
            wickColor = chartSecondarySemiStopColor;
            candleColor = chartSecondaryColor;
        }

        // Draw wicks
        d3.select(this)
            .append("line")
            .attr("x1", x(d.hour) + x.bandwidth() / 2)
            .attr("x2", x(d.hour) + x.bandwidth() / 2)
            .attr("y1", y(d.h))
            .attr("y2", y(d.l))
            .attr("stroke", wickColor);

        // Draw top wicks
        d3.select(this)
            .append("line")
            .attr("x1", x(d.hour) + x.bandwidth() / 2)
            .attr("x2", x(d.hour) + x.bandwidth() / 2)
            .attr("y1", y(Math.max(d.o, d.c)) - 8)
            .attr("y2", y(Math.max(d.o, d.c)))
            .attr("stroke", wickColor);

        // Draw bottom wicks
        d3.select(this)
            .append("line")
            .attr("x1", x(d.hour) + x.bandwidth() / 2)
            .attr("x2", x(d.hour) + x.bandwidth() / 2)
            .attr("y1", y(Math.min(d.o, d.c)))
            .attr("y2", y(Math.min(d.o, d.c)) + 16)
            .attr("stroke", wickColor);

        d3.select(this)
            .append("rect")
            .attr("x", x(d.hour))
            .attr("y", y(Math.max(d.o, d.c)))
            .attr("width", x.bandwidth())
            .attr("height", Math.abs(y(d.o) - y(d.c)) * 3) // Make the rectangles three times taller
            .attr("fill", d.o > d.c ? chartStopColor : candleColor);

        // Add transparent overlay rectangle for larger mouseover area
        d3.select(this)
            .append("rect")
            .attr("x", x(d.hour) - x.bandwidth() / 2)
            .attr("width", x.bandwidth() * 2)
            .attr("y", 0)
            .attr("height", height)
            .attr("fill", "none")
            .attr("pointer-events", "all")
            .on("mouseover", function (event) {
                tooltip.transition()
                    .duration(200)
                    .style("opacity", 1)
                    .style("background-color", tooltipBackgroundColor)
                    .style("color", tooltipColor)
                    .style("font-family", "inherit")
                    .style("font-size", tooltipFontSize)
                    .style("border-radius", tooltipBorderRadius)
                    .style("width", "fit-content")
                    .style("padding", tooltipPadding)
                    .style("position", "absolute");
                tooltip.html(`<strong>Hour: ${d.hour}</strong><br>Open: ${d.o}<br>High: ${d.h}<br>Low: ${d.l}<br>Close: ${d.c}`)
                    .style("left", (event.pageX + 5) + "px")
                    .style("top", (event.pageY - 28) + "px");
            })
            .on("mouseout", function () {
                tooltip.transition()
                    .duration(500)
                    .style("opacity", 0);
            });
    });

    const addLineWithTooltip = (lineData, strokeColor, label) => {
        svg.append("path")
            .datum(lineData.map((d, i) => ({ value: d, label: data.labels[i] })))
            .attr("class", "line")
            .attr("d", d3.line()
                .x(d => x(d.label) + x.bandwidth() / 2)
                .y(d => y(d.value))
            )
            .attr("fill", "none")
            .attr("stroke", strokeColor)
            .attr("stroke-width", 1)
            .on("mouseover", function (event, d) {
                const [xPos] = d3.pointer(event);
                const index = Math.floor(xPos / (width / data.labels.length));
                tooltip.transition()
                    .duration(200)
                    .style("opacity", 1)
                    .style("background-color", tooltipBackgroundColor)
                    .style("color", tooltipColor)
                    .style("font-family", "inherit")
                    .style("font-size", tooltipFontSize)
                    .style("border-radius", tooltipBorderRadius)
                    .style("width", "fit-content")
                    .style("padding", tooltipPadding)
                    .style("position", "absolute");
                tooltip.html(`<div style="display: flex; align-items: center;"><div style="width: 12px; height: 12px; background-color: ${strokeColor}; margin-right: 5px;"></div><strong>${label}: ${lineData[index]}</strong></div>`)
                    .style("left", (event.pageX + 5) + "px")
                    .style("top", (event.pageY - 28) + "px");
            })
            .on("mouseout", function () {
                tooltip.transition()
                    .duration(500)
                    .style("opacity", 0);
            });
    };

    // Add the Tenkan Line
    addLineWithTooltip(data.datasets[1].data, lowStrokeColor, 'Tenkan');

    // Add the Chikou Span
    addLineWithTooltip(data.datasets[2].data, highStrokeColor, 'Chikou');

    // Add the Senkou Span A Line
    addLineWithTooltip(data.datasets[3].data, chartSecondaryColor, 'Senkou A');

    // Add the Senkou Span B Line
    addLineWithTooltip(data.datasets[4].data, chartPrimaryColor, 'Senkou B');

    // Define the area generator for the cloud
    const area = d3.area()
        .x((d, i) => x(data.labels[i]) + x.bandwidth() / 2)
        .y0(d => y(d.senkouA < d.senkouB ? d.senkouA : d.senkouB)) // Senkou Span A
        .y1(d => y(d.senkouA > d.senkouB ? d.senkouA : d.senkouB)); // Senkou Span B

    // Prepare the data for the area generator
    const cloudData = data.labels.map((label, i) => ({
        label,
        senkouA: data.datasets[3].data[i],
        senkouB: data.datasets[4].data[i]
    }));

    // Add the gradient definition
    const gradient = svg.append("defs")
        .append("linearGradient")
        .attr("id", "kumo-gradient")
        .attr("x1", "0%")
        .attr("y1", "0%")
        .attr("x2", "0%")
        .attr("y2", "100%");

    gradient.append("stop")
        .attr("offset", "0%")
        .attr("stop-color", chartPrimaryColor)
        .attr("stop-opacity", 1);

    gradient.append("stop")
        .attr("offset", "100%")
        .attr("stop-color", chartStopColor)
        .attr("stop-opacity", 1);

    // Add the cloud area
    svg.append("path")
        .datum(cloudData)
        .attr("class", "area")
        .attr("d", area)
        .style("fill", "url(#kumo-gradient)");
}

function createRSIChart(ctx, data) {
    const margin = { top: 10, right: 30, bottom: 50, left: 70 },
        width = 370 - margin.left - margin.right,
        height = 260 - margin.top - margin.bottom;

    // Apply gradient to the data
    const modifiedData = applyLineGradientToData(data, ctx);

    return new Chart(ctx, {
        type: 'line',
        data: modifiedData,
        options: {
            responsive: commonResponsive,
            plugins: commonPlugins,
            hover: commonHover,
            scales: {
                x: {
                    ...commonScales.x,
                    title: {
                        ...commonScales.x.title,
                        color: chartTitle
                    },
                    grid: {
                        ...commonScales.x.grid,
                        color: gridStrokeColor,
                        borderColor: chartAxis
                    },
                    ticks: {
                        ...commonScales.x.ticks,
                        color: chartNumbers
                    },
                    border: {
                        display: true,
                        color: chartAxis
                    }
                },
                y: {
                    ...commonScales.y,
                    beginAtZero: true,
                    title: {
                        ...commonScales.y.title,
                        text: 'RELATIVE STRENGTH INDEX (ADX)',
                        color: chartTitle
                    },
                    grid: {
                        ...commonScales.y.grid,
                        color: gridStrokeColor,
                        borderColor: chartAxis
                    },
                    ticks: {
                        ...commonScales.y.ticks,
                        color: chartNumbers
                    },
                    border: {
                        display: true,
                        color: chartAxis
                    }
                }
            },
            elements: {
                point: {
                    radius: 3,
                    borderWidth: 1,
                    hoverRadius: 5
                }
            },
            plugins: {
                legend: {
                    display: false
                }
            }
        }
    });
}

function createStochasticChart(data, containerId) {
    const margin = { top: 8, right: 16, bottom: 36, left: 42 },
        width = 352 - margin.left - margin.right,
        height = 226 - margin.top - margin.bottom;

    // Clear previous chart content
    d3.select(containerId).selectAll('*').remove();

    const svg = d3.select(containerId)
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    const parseDate = d3.timeParse("%H");

    // Parse the dates in the data and handle potential errors
    try {
        const parsedData = data.map(d => ({ ...d, date: parseDate(d.hour) }));

        const x = d3.scaleBand()
            .domain(parsedData.map(d => d.date))
            .range([0, width])
            .padding(0.1);

        const y = d3.scaleLinear()
            .domain([d3.min(parsedData, d => d.l), d3.max(parsedData, d => d.h)])
            .nice()
            .range([height, 0]);

        const xAxis = d3.axisBottom(x)
            .tickFormat((d, i) => (i % 2 === 0 ? i + 1 : '')); // Display every other number
        const yAxis = d3.axisLeft(y);

        //Add gridlines
        svg.append("g")
        .attr("class", "grid")
        .attr("transform", "translate(0," + height + ")")
        .style("stroke", gridStrokeColor)
        .call(d3.axisBottom(x)
            .tickSize(-height)
            .tickFormat("")
        )
        .selectAll("line")
        .attr("stroke", gridStrokeColor);

        svg.append("g")
            .attr("class", "grid")
            .call(d3.axisLeft(y)
                .tickSize(-width)
                .tickFormat("")
            )
            .selectAll("line")
            .attr("stroke", gridStrokeColor);

        svg.append("g")
            .attr("transform", "translate(0," + height + ")")
            .call(xAxis)
            .append("text")
            .attr("y", 30) // Adjust y position of x-axis title
            .attr("x", width / 2)
            .attr("dy", "0.3em")
            .style("text-anchor", "middle")
            .style("font-family", "Arial")
            .style("font-size", "10px")
            .style("font-weight", "bold")
            .style("fill", chartTitle)
            .attr("class", "axis-title")
            .text("1WK TRADING TIME (HRS)");

        svg.append("g")
            .call(yAxis)
            .append("text")
            .attr("transform", "rotate(-90)")
            .attr("y", -36)
            .attr("x", -height / 2)
            .attr("dy", ".71em")
            .style("text-anchor", "middle")
            .style("font-family", "Arial")
            .style("font-size", "10px")
            .style("font-weight", "bold")
            .style("fill", chartTitle)
            .attr("class", "axis-title")
            .text("PRICE");

        // Create area gradient
        const area = d3.area()
            .x(d => x(d.date) + x.bandwidth() / 2)
            .y0(height)
            .y1(d => y(d.c));

        svg.append("path")
            .datum(parsedData)
            .attr("fill", "url(#area-gradient)")
            .attr("d", area);

        // Define the gradient
        svg.append("defs").append("linearGradient")
            .attr("id", "area-gradient")
            .attr("x1", "0%")
            .attr("y1", "0%")
            .attr("x2", "0%")
            .attr("y2", "100%")
            .selectAll("stop")
            .data([
                { offset: "0%", color: chartSemiStopColor, opacity: 1 },
                { offset: "100%", color: chartStopColor, opacity: 0 }
            ])
            .enter().append("stop")
            .attr("offset", d => d.offset)
            .attr("stop-color", d => d.color)
            .attr("stop-opacity", d => d.opacity);

        // Tooltip
        const tooltip = d3.select("body").append("div")
            .attr("class", "tooltip")
            .style("position", "absolute")
            .style("width", "120px")
            .style("height", "fit-content")
            .style("padding", "2px")
            .style("font-size", tooltipFontSize)
            .style("font-family", tooltipFont)
            .style("background", tooltipBackgroundColor)
            .style("border", "0px")
            .style("border-radius", tooltipBorderRadius)
            .style("pointer-events", "none")
            .style("opacity", 0);

        const candlestick = svg.selectAll(".candlestick")
            .data(parsedData)
            .enter()
            .append("g")
            .attr("class", "candlestick");

        candlestick.each(function (d, i) {
            let wickColor = chartSemiStopColor;
            let candleColor = chartPrimaryColor;

            if (i > 0 && d.c > parsedData[i - 1].c) {
                wickColor = chartSecondarySemiStopColor;
                candleColor = chartSecondaryColor;
            }

            // Draw wicks
            d3.select(this)
                .append("line")
                .attr("x1", x(d.date) + x.bandwidth() / 2)
                .attr("x2", x(d.date) + x.bandwidth() / 2)
                .attr("y1", y(d.h))
                .attr("y2", y(d.l))
                .attr("stroke", wickColor);

            // Draw top wicks
            d3.select(this)
                .append("line")
                .attr("x1", x(d.date) + x.bandwidth() / 2)
                .attr("x2", x(d.date) + x.bandwidth() / 2)
                .attr("y1", y(Math.max(d.o, d.c)) - 8)
                .attr("y2", y(Math.max(d.o, d.c)))
                .attr("stroke", wickColor);

            // Draw bottom wicks
            d3.select(this)
                .append("line")
                .attr("x1", x(d.date) + x.bandwidth() / 2)
                .attr("x2", x(d.date) + x.bandwidth() / 2)
                .attr("y1", y(Math.min(d.o, d.c)))
                .attr("y2", y(Math.min(d.o, d.c)) + 8)
                .attr("stroke", wickColor);

            d3.select(this)
                .append("rect")
                .attr("x", x(d.date))
                .attr("y", y(Math.max(d.o, d.c)))
                .attr("width", x.bandwidth())
                .attr("height", Math.abs(y(d.o) - y(d.c)))
                .attr("fill", d.o > d.c ? chartStopColor : candleColor);

            // Add transparent overlay rectangle for larger mouseover area
            d3.select(this)
                .append("rect")
                .attr("x", x(d.date) - x.bandwidth() / 2)
                .attr("width", x.bandwidth() * 2)
                .attr("y", 0)
                .attr("height", height)
                .attr("fill", "none")
                .attr("pointer-events", "all")
                .on("mouseover", function (event) {
                    tooltip.transition()
                        .duration(200)
                        .style("opacity", 1)
                        .style("background-color", tooltipBackgroundColor)
                        .style("color", tooltipColor)
                        .style("font-family", "inherit")
                        .style("font-size", tooltipFontSize)
                        .style("border-radius", tooltipBorderRadius)
                        .style("width", "fit-content")
                        .style("padding", tooltipPadding)
                        .style("position", "absolute");
                    tooltip.html(`<strong>${d3.timeFormat("%H")(d.date)}</strong><br>Open: ${d.o}<br>High: ${d.h}<br>Low: ${d.l}<br>Close: ${d.c}`)
                        .style("left", (event.pageX + 5) + "px")
                        .style("top", (event.pageY - 28) + "px");
                })
                .on("mouseout", function () {
                    tooltip.transition()
                        .duration(500)
                        .style("opacity", 0);
                });
        });
    } catch (error) {
        console.error("Error parsing data for Stochastic chart:", error);
    }
}

//load chart data
function loadChartData(questionKey) {
    if (typeof chartsData === 'undefined') {
        console.error("chartsData is not defined");
        return;
    }

    const data = chartsData[questionKey];

    if (!data) {
        console.error(`No data found for question key: ${questionKey}`);
        return;
    }

    // Ensure contexts are initialized
    console.log("Invoking initialContexts function...");
    initializeContexts();

    console.log("Contexts initialized:", ctxCandleStick, ctxBollinger, ctxLine, ctxBar, ctxVolume, ctxMovingAverage, ctxRSI, ctxMACD, ctxFibonacci, ctxPivot, ctxStochastic, ctxIchimoku, ctxATR, ctxADX);

    //Create D3.js charts
    createAtrChart(data.atrData, "#atr-svg");
    createBollingerChart(data.bollingerData, "#bollinger-svg");
    createCandlestickChart(data.candlestickData, "#candlestick-svg");
    createFibonacciChart(data.fibonacciData, "#fibonacci-svg");
    createIchimokuChart(data.ichimokuData, "#ichimoku-svg");
    createStochasticChart(data.stochasticData, "#stochastic-svg");
    
    // Destroy existing ChartJS charts if they exist
    if (charts.adxChart) {
        charts.adxChart.destroy();
    }
    if (charts.barChart) {
        charts.barChart.destroy();
    }
    if (charts.lineChart) {
        charts.lineChart.destroy();
    }
    if (charts.macdChart) {
        charts.macdChart.destroy();
    }
    if (charts.movingAverageChart) {
        charts.movingAverageChart.destroy();
    }
    if (charts.pivotChart) {
        charts.pivotChart.destroy();
    }
    if (charts.rsiChart) {
        charts.rsiChart.destroy();
    }
    if (charts.volumeChart) {
        charts.volumeChart.destroy();
    }
    
    // Debugging: Log the data being loaded
    console.log('Loaded data for:', questionKey, data);
    console.log('chartAxis color is: ' + chartAxis);
    console.log('chartPrimaryColor color is: ' + chartPrimaryColor);
    console.log('gradient values for chartSemiStopColor & chartStopColor are: '+chartSemiStopColor, chartStopColor);

    // Update the ADX chart
    charts.adxChart = new Chart(ctxADX, {
        type: 'line',
        data: {
            ...data.adxData,
            datasets: [{
                ...data.adxData.datasets[0],
                backgroundColor: createLineGradient(ctxADX, ctxADX.canvas.clientHeight, chartSemiStopColor, chartStopColor),
                borderColor: createLineHorizontalGradient(ctxADX, ctxADX.canvas.clientWidth, chartStopColor, chartPrimaryColor),
                borderWidth: 3,
                fill: true,
                pointBackgroundColor: chartPrimaryColor,
                tension: 0.6
            }]
        },
        options: {
            responsive: commonResponsive,
            plugins: commonPlugins,
            hover: commonHover,
            scales: {
                x: {
                    ...commonScales.x,
                    title: {
                        ...commonScales.x.title,
                        color: chartTitle
                    },
                    grid: {
                        ...commonScales.x.grid,
                        color: gridStrokeColor,
                        borderColor: chartAxis
                    },
                    ticks: {
                        ...commonScales.x.ticks,
                        color: chartNumbers
                    },
                    border: {
                        display: true,
                        color: chartAxis
                    }
                },
                y: {
                    ...commonScales.y,
                    beginAtZero: true,
                    title: {
                        ...commonScales.y.title,
                        text: 'AVERAGE DIRECTIONAL INDEX',
                        color: chartTitle
                    },
                    grid: {
                        ...commonScales.y.grid,
                        color: gridStrokeColor,
                        borderColor: chartAxis
                    },
                    ticks: {
                        ...commonScales.y.ticks,
                        color: chartNumbers
                    },
                    border: {
                        display: true,
                        color: chartAxis
                    }
                }
            },
            elements: {
                point: {
                    radius: 3,
                    backgroundColor: chartPrimaryColor,
                }
            }
        }
    });

    // Update the bar chart
    charts.barChart = new Chart(ctxBar, {
        type: 'bar',
        data: {
            ...data.barData,
            datasets: [{
                ...data.barData.datasets[0],
                backgroundColor: createVerticalGradient(ctxBar, ctxBar.canvas.clientHeight),
                borderColor: chartPrimaryColor,
                borderWidth: 1
            }]
        },
        options: {
            responsive: commonResponsive,
            plugins: commonPlugins,
            hover: commonHover,
            scales: {
                x: {
                    ...commonScales.x,
                    title: {
                        ...commonScales.x.title,
                        color: chartTitle
                    },
                    grid: {
                        ...commonScales.x.grid,
                        color: gridStrokeColor,
                        borderColor: chartAxis
                    },
                    ticks: {
                        ...commonScales.x.ticks,
                        color: chartNumbers
                    },
                    border: {
                        display: true,
                        color: chartAxis
                    }
                },
                y: {
                    ...commonScales.y,
                    beginAtZero: true,
                    title: {
                        ...commonScales.y.title,
                        color: chartTitle
                    },
                    grid: {
                        ...commonScales.y.grid,
                        color: gridStrokeColor,
                        borderColor: chartAxis
                    },
                    ticks: {
                        ...commonScales.y.ticks,
                        color: chartNumbers
                    },
                    border: {
                        display: true,
                        color: chartAxis
                    }
                }
            },
            elements: {
                bar: {
                    backgroundColor: createVerticalGradient(ctxBar, ctxBar.canvas.clientHeight),
                    borderColor: chartPrimaryColor,
                    borderWidth: 1
                }
            }
        }
    });

    // Update the line chart
    charts.lineChart = new Chart(ctxLine, {
        type: 'line',
        data: {
            ...data.lineData,
            datasets: [{
                ...data.lineData.datasets[0],
                backgroundColor: createLineGradient(ctxLine, ctxLine.canvas.clientHeight, chartSemiStopColor, chartStopColor),
                borderColor: createLineHorizontalGradient(ctxLine, ctxLine.canvas.clientWidth, chartStopColor, chartPrimaryColor),
                borderWidth: 3,
                fill: true,
                tension: 0.1,
                pointBackgroundColor: chartPrimaryColor
            }]
        },
        options: {
            responsive: commonResponsive,
            plugins: commonPlugins,
            hover: commonHover,
            scales: {
                x: {
                    ...commonScales.x,
                    title: {
                        ...commonScales.x.title,
                        color: chartTitle
                    },
                    grid: {
                        ...commonScales.x.grid,
                        color: gridStrokeColor,
                        borderColor: chartAxis
                    },
                    ticks: {
                        ...commonScales.x.ticks,
                        color: chartNumbers
                    },
                    border: {
                        display: true,
                        color: chartAxis
                    }
                },
                y: {
                    ...commonScales.y,
                    beginAtZero: true,
                    max: 160,
                    title: {
                        ...commonScales.y.title,
                        text: 'AVERAGE DIRECTIONAL INDEX (ADX)',
                        color: chartTitle
                    },
                    grid: {
                        ...commonScales.y.grid,
                        color: gridStrokeColor,
                        borderColor: chartAxis
                    },
                    ticks: {
                        ...commonScales.y.ticks,
                        color: chartNumbers
                    },
                    border: {
                        display: true,
                        color: chartAxis
                    }
                }
            },
            elements: {
                point: {
                    radius: 3,
                    backgroundColor: chartPrimaryColor,
                }
            },
            plugins: {
                legend: {
                    display: false
                },
                datalabels: {
                    anchor: 'end',
                    align: 'top',
                    color: chartNumbers,
                    font: {
                        size: 9,
                        weight: 'bold'
                    },
                    formatter: function(value, context) {
                        return value;
                    },
                    rotation: -45
                }
            }
        },
        plugins: [ChartDataLabels]
    });

    // Update the MACD chart
    charts.macdChart = new Chart(ctxMACD, {
        type: 'bar',
        data: {...data.macdData,
            datasets: [{
                ...data.macdData.datasets[0],
                    type: 'line',
                    borderColor: predictiveColor,
                    borderWidth: 2,
                    borderDash: [5, 5],
                    fill: false,
                    tension: 0.4,
                    pointBackgroundColor: predictiveColor,
                    pointRadius: 0
                },{
                    ...data.macdData.datasets[1],
                    type: 'bar',
                    borderColor: chartPrimaryColor,
                    borderWidth: 2,
                    fill: false,
                    pointRadius: 0,
                    tension: 0,
                    borderDash: [],
                    backgroundColor: 'transparent'
                },{
                    ...data.macdData.datasets[1],
                    type: 'bar',
                    backgroundColor: createLineGradient(ctxMACD, ctxMACD.canvas.clientHeight, chartSemiStopColor, chartStopColor),
                    fill: true
                },{
                    ...data.macdData.datasets[2],
                    type: 'bar',
                    borderColor: lowStrokeColor,
                    borderWidth: 2,
                    fill: false,
                    pointRadius: 0,
                    tension: 0,
                    borderDash: [],
                    backgroundColor: 'transparent'
                },{
                    ...data.macdData.datasets[2],
                    type: 'bar',
                    backgroundColor: createLineGradient(ctxMACD, ctxMACD.canvas.clientHeight, lowStrokeMidColor, lowStrokeSemiColor),
                    fill: true
                }]
        },
        options: {
            responsive: commonResponsive,
            plugins: commonPlugins,
            hover: commonHover,
            scales: {
                x: {
                    ...commonScales.x,
                    title: {
                        ...commonScales.x.title,
                        color: chartTitle
                    },
                    grid: {
                        ...commonScales.x.grid,
                        color: gridStrokeColor,
                        borderColor: chartAxis
                    },
                    ticks: {
                        ...commonScales.x.ticks,
                        color: chartNumbers
                    },
                    border: {
                        display: true,
                        color: chartAxis
                    },
                    stacked: true
                },
                y: {
                    ...commonScales.y,
                    beginAtZero: true,
                    title: {
                        ...commonScales.y.title,
                        text: 'MACD',
                        color: chartTitle
                    },
                    grid: {
                        ...commonScales.y.grid,
                        color: gridStrokeColor,
                        borderColor: chartAxis
                    },
                    ticks: {
                        ...commonScales.y.ticks,
                        color: chartNumbers
                    },
                    border: {
                        display: true,
                        color: chartAxis
                    }
                }
            },
            elements: {
                point: {
                    radius: 3,
                    backgroundColor: chartPrimaryColor
                }
            }
        }
    });
    
    // Update the moving average chart
    charts.movingAverageChart = new Chart(ctxMovingAverage, {
        type: 'line',
        data: {
            ...data.movingAverageData,
            datasets: [{
                ...data.movingAverageData.datasets[0],
                borderColor: predictiveColor,
                borderWidth: 2,
                borderDash: [5, 5],
                fill: false,
                tension: 0.4, // Smooth curved line
                showLine: true,
                pointRadius: 0 // No nodes
            },{
                ...data.movingAverageData.datasets[1],
                borderColor: createLineHorizontalGradient(ctxMovingAverage, ctxMovingAverage.canvas.clientWidth, chartStopColor, chartPrimaryColor),
                borderWidth: 3,
                fill: true,
                backgroundColor: createLineGradient(ctxMovingAverage, ctxMovingAverage.canvas.clientHeight, chartSemiStopColor, chartStopColor),
                tension: 0.1, // Straight connecting lines
                showLine: true,
                pointRadius: 3, // With nodes
                pointBackgroundColor: chartPrimaryColor // Node color
            }]
        },
        options: {
            responsive: commonResponsive,
            plugins: commonPlugins,
            hover: commonHover,
            scales: {
                x: {
                    ...commonScales.x,
                    title: {
                        ...commonScales.x.title,
                        color: chartTitle
                    },
                    grid: {
                        ...commonScales.x.grid,
                        color: gridStrokeColor,
                        borderColor: chartAxis
                    },
                    ticks: {
                        ...commonScales.x.ticks,
                        color: chartNumbers
                    },
                    border: {
                        display: true,
                        color: chartAxis
                    }
                },
                y: {
                    ...commonScales.y,
                    beginAtZero: true,
                    title: {
                        ...commonScales.y.title,
                        text: 'MOVING AVERAGE',
                        color: chartTitle
                    },
                    grid: {
                        ...commonScales.y.grid,
                        color: gridStrokeColor,
                        borderColor: chartAxis
                    },
                    ticks: {
                        ...commonScales.y.ticks,
                        color: chartNumbers
                    },
                    border: {
                        display: true,
                        color: chartAxis
                    }
                }
            },
            elements: {
                point: {
                    radius: 3,
                    backgroundColor: chartPrimaryColor
                }
            }
        }
    });

    // Update the Pivot Points chart
    charts.pivotChart = new Chart(ctxPivot, {
        type: 'pie',
        data: {
            ...data.pivotData,
            datasets: [{
                ...data.pivotData.datasets[0],
                hoverOffset : 16,
                borderWidth: 2,
                backgroundColor: [
                    createHighSolidGradient(ctxPivot, ctxPivot.canvas.clientHeight),
                    createLineGradient(ctxPivot, ctxPivot.canvas.clientHeight, chartPrimaryColor, chartSemiStopColor), 
                    createLowSolidGradient(ctxPivot, ctxPivot.canvas.clientHeight),
                    
                ],
                borderColor: [
                    createHighSolidGradient(ctxPivot, ctxPivot.canvas.clientHeight),
                    createLineGradient(ctxPivot, ctxPivot.canvas.clientHeight, chartPrimaryColor, chartSemiStopColor), 
                    createLowSolidGradient(ctxPivot, ctxPivot.canvas.clientHeight)
                ],
            }]
        },
        options: {
            plugins: {
                legend: {
                    display: true,
                    position: 'right',
                    labels: {
                        color: chartTitle,
                        size: 10
                    }
                },
            },
            layout: {
                padding: {
                    top: 0,
                    right: 0,
                    bottom: 0,
                    left: 5
                }
            },
            cutout: '66%',
            maintainAspectRatio: false,
            rotation: -45
        }
    });

    // Update the RSI chart
    charts.rsiChart = createRSIChart(ctxRSI, data.rsiData);

    // Update the volume chart
    charts.volumeChart = new Chart(ctxVolume, {
        type: 'bar',
        data: {
            ...data.volumeData,
            datasets: [{
                ...data.volumeData.datasets[0],
                backgroundColor: createHorizontalGradient(ctxVolume, ctxVolume.canvas.clientWidth), // Use width for horizontal gradient
                borderColor: chartPrimaryColor,
                borderWidth: 1
            }]
        },
        options: {
            responsive: commonResponsive,
            plugins: commonPlugins,
            hover: commonHover,
            indexAxis: 'y', // Set the indexAxis to 'y' to switch the chart to horizontal bars
            scales: {
                x: {
                    ...commonScales.y,
                    title: {
                        ...commonScales.y.title,
                        text: 'PRICE LEVEL',
                        color: chartTitle
                    },
                    grid: {
                        ...commonScales.y.grid,
                        color: gridStrokeColor,
                        borderColor: chartAxis
                    },
                    ticks: {
                        ...commonScales.y.ticks,
                        color: chartNumbers
                    },
                    border: {
                        display: true,
                        color: chartAxis
                    }
                },
                y: {
                    ...commonScales.x,
                    beginAtZero: true,
                    title: {
                        ...commonScales.x.title,
                        text: '(PB) VOLUME',
                        color: chartTitle
                    },
                    grid: {
                        ...commonScales.x.grid,
                        color: gridStrokeColor,
                        borderColor: chartAxis
                    },
                    ticks: {
                        ...commonScales.x.ticks,
                        color: chartNumbers
                    },
                    border: {
                        display: true,
                        color: chartAxis
                    }
                }
            },
            elements: {
                bar: {
                    backgroundColor: createVerticalGradient(ctxVolume, ctxVolume.canvas.clientWidth), // Adjust for horizontal bars
                    borderColor: chartPrimaryColor,
                    borderWidth: 1
                }
            }
        }
    });   
}

// Call this function to initialize all charts once the page is fully loaded
window.onload = function() {
    updateColorsForDarkMode();
    const currentQuestionKey = getCurrentQuestionKey();
    console.log("Loading chart data for key:", getCurrentQuestionKey());
    loadChartData(currentQuestionKey);
    console.log("Chart data loaded.");
};

//On darkmode
const renderCharts = function() {
    const currentQuestionKey = getCurrentQuestionKey();
    console.log("Loading chart data for key:", getCurrentQuestionKey());
    loadChartData(currentQuestionKey);
    console.log("Chart data loaded.");
}

// Optionally, add an event listener to handle changes in dark mode dynamically
// document.addEventListener('DOMContentLoaded', () => {
//     document.documentElement.classList.toggle('dark-knight', /* condition to check if it's dark mode */);
//     updateColorsForDarkMode();
//     const currentQuestionKey = getCurrentQuestionKey();
//     loadChartData(currentQuestionKey);
// });