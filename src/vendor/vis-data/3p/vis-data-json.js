const data = [
  {
    "question": "How markets fared in the second half of December during the so-called Santa Claus rally?",
    "answers": [
      {
        "answer": "Santa did not show up for a rally last December in 2022. The S&P 500 index fell -5.9% for the Christmas month last year and finished 2022 down",
        "cite": "investingcaffeine.com was indexed by Google more than 10 years ago",
        "search_engine": "Google"
      },
      {
        "answer": "Almost as predictable as the big jolly man himself, many on Wall Street are eagerly waiting for the so-called “Santa Claus rally” to further fuel stock-market gains that have already put investors in a holiday mood.",
        "cite": "https://www.marketwatch.com was indexed by Bing",
        "search_engine": "Bing"
      },
      {
        "answer": "The S&P 500 has gained an average of 1.4% in the second half of December in so-called Santa Claus rallies, compared with a 0.1% gain in the first half, according to LPL's analysis",
        "cite": "https://finance.yahoo.com was indexed by Yahoo Finance",
        "search_engine": "Yahoo Finance"
      }
    ],
    "stats": {
      "hits": "157,000",
      "duration": "(0.40 seconds)",
      "timestamp": "05/23/24 @16:26 GMT(-6)",
      "source": "MTX engine 1.0.0",
      "query_engine": "Metisx"
    }
  },
  {
    "question": "What were the main factors driving the stock market rally in Q1 2023?",
    "answers": [
      {
        "answer": "The stock market rally in Q1 2023 was primarily driven by positive corporate earnings and investor optimism about economic recovery.",
        "cite": "bloomberg.com was indexed by Google",
        "search_engine": "Google"
      },
      {
        "answer": "Analysts attribute the Q1 2023 stock market rally to a combination of strong earnings reports and easing inflation concerns.",
        "cite": "https://www.reuters.com was indexed by Bing",
        "search_engine": "Bing"
      },
      {
        "answer": "Key factors for the Q1 2023 rally included favorable monetary policy and robust economic indicators, according to experts.",
        "cite": "https://www.cnbc.com was indexed by Yahoo Finance",
        "search_engine": "Yahoo Finance"
      }
    ],
    "stats": {
      "hits": "123,000",
      "duration": "(0.35 seconds)",
      "timestamp": "05/23/24 @16:30 GMT(-6)",
      "source": "MTX engine 1.0.0",
      "query_engine": "Metisx"
    }
  },
  {
    "question": "How did the Fed's interest rate changes affect the bond market in 2023?",
    "answers": [
      {
        "answer": "The Fed's interest rate hikes in 2023 led to a decline in bond prices as yields rose, affecting investor portfolios.",
        "cite": "barrons.com was indexed by Google",
        "search_engine": "Google"
      },
      {
        "answer": "Rising interest rates in 2023 resulted in lower bond prices, with many investors shifting to equities for better returns.",
        "cite": "https://www.ft.com was indexed by Bing",
        "search_engine": "Bing"
      },
      {
        "answer": "Higher interest rates increased bond yields, making new issues more attractive but reducing the value of existing bonds.",
        "cite": "https://www.wsj.com was indexed by Yahoo Finance",
        "search_engine": "Yahoo Finance"
      }
    ],
    "stats": {
      "hits": "98,000",
      "duration": "(0.28 seconds)",
      "timestamp": "05/23/24 @16:35 GMT(-6)",
      "source": "MTX engine 1.0.0",
      "query_engine": "Metisx"
    }
  },
  {
    "question": "What impact did geopolitical tensions have on global markets in early 2024?",
    "answers": [
      {
        "answer": "Geopolitical tensions in early 2024 led to increased volatility in global markets, with investors seeking safe-haven assets.",
        "cite": "forbes.com was indexed by Google",
        "search_engine": "Google"
      },
      {
        "answer": "Markets experienced significant fluctuations in early 2024 due to geopolitical uncertainties, affecting investor confidence.",
        "cite": "https://www.economist.com was indexed by Bing",
        "search_engine": "Bing"
      },
      {
        "answer": "Geopolitical events in early 2024 caused a spike in market volatility, with many sectors experiencing sharp declines.",
        "cite": "https://www.bloomberg.com was indexed by Yahoo Finance",
        "search_engine": "Yahoo Finance"
      }
    ],
    "stats": {
      "hits": "145,000",
      "duration": "(0.45 seconds)",
      "timestamp": "05/23/24 @16:40 GMT(-6)",
      "source": "MTX engine 1.0.0",
      "query_engine": "Metisx"
    }
  },
  {
    "question": "How did the tech sector perform in the first half of 2023?",
    "answers": [
      {
        "answer": "The tech sector saw substantial growth in the first half of 2023, driven by strong earnings reports and innovative product launches.",
        "cite": "techcrunch.com was indexed by Google",
        "search_engine": "Google"
      },
      {
        "answer": "In H1 2023, tech stocks surged as companies reported better-than-expected earnings and continued to lead in innovation.",
        "cite": "https://www.wired.com was indexed by Bing",
        "search_engine": "Bing"
      },
      {
        "answer": "Tech sector growth in the first half of 2023 was buoyed by increased investment in AI and cloud computing technologies.",
        "cite": "https://www.theverge.com was indexed by Yahoo Finance",
        "search_engine": "Yahoo Finance"
      }
    ],
    "stats": {
      "hits": "112,000",
      "duration": "(0.33 seconds)",
      "timestamp": "05/23/24 @16:45 GMT(-6)",
      "source": "MTX engine 1.0.0",
      "query_engine": "Metisx"
    }
  },
  {
    "question": "What were the key trends in the cryptocurrency market in 2023?",
    "answers": [
      {
        "answer": "The cryptocurrency market in 2023 was marked by increased regulation and the growing adoption of digital currencies by mainstream institutions.",
        "cite": "cointelegraph.com was indexed by Google",
        "search_engine": "Google"
      },
      {
        "answer": "2023 saw significant volatility in the crypto market, driven by regulatory changes and major technological advancements.",
        "cite": "https://www.coindesk.com was indexed by Bing",
        "search_engine": "Bing"
      },
      {
        "answer": "Key trends in the 2023 cryptocurrency market included the rise of central bank digital currencies (CBDCs) and enhanced blockchain security measures.",
        "cite": "https://www.cryptoslate.com was indexed by Yahoo Finance",
        "search_engine": "Yahoo Finance"
      }
    ],
    "stats": {
      "hits": "130,000",
      "duration": "(0.30 seconds)",
      "timestamp": "05/23/24 @16:50 GMT(-6)",
      "source": "MTX engine 1.0.0",
      "query_engine": "Metisx"
    }
  }
];
