const dmode = function(){
    const darkModeButton = document.getElementById('darkmode');
    const autoDark = document.querySelector('html');

    darkModeButton.classList.add('darkmode');

    // Override autoDark
    darkModeButton.onclick = (event) => {
        const expanded = autoDark.classList.contains('dark-knight');

        if (!expanded) {
            // apply param
            // location.href = 'vis-data.html?darkmode=true';
            autoDark.classList.add('dark-knight');
            updateColorsForDarkMode();
            const currentQuestionKey = getCurrentQuestionKey();
            loadChartData(currentQuestionKey);
        } else {
            // remove param
            // location.href = 'vis-data.html';
            autoDark.classList.remove('dark-knight');
            updateColorsForDarkMode();
            const currentQuestionKey = getCurrentQuestionKey();
            loadChartData(currentQuestionKey);
        }
        darkModeButton.setAttribute('aria-expanded', !expanded);
    }
}();