
//User.Agent
const snoopAgent = function(){
	let deviceId = document.querySelector('html');

	const resetSnoop = function(){
		deviceId.classList.remove('ie11');
		deviceId.classList.remove('ie10');
		deviceId.classList.remove('ie9');
		deviceId.classList.remove('iPhone');
		deviceId.classList.remove('iPad');
	}();

  //determine client browser append hook
	if(navigator.userAgent.match(/Trident.*rv\:11\./)) {
		deviceId.setAttribute('class', 'ie11');
	}
	if(navigator.userAgent.match(/Trident\/6\./)) {
		deviceId.setAttribute('class', 'ie10');
	}
	if(navigator.userAgent.match(/Trident\/5\./)) {
		deviceId.setAttribute('class', 'ie9');
	}
	if(navigator.userAgent.match(/Edge/)) {
		deviceId.setAttribute('class', 'Edge');
	}
	if(navigator.userAgent.match(/iPhone/)) {
		deviceId.setAttribute('class', 'iPhone');
	}
	if(navigator.userAgent.match(/iPad/)) {
		deviceId.setAttribute('class', 'iPad');
	}
}();

const cardsContainer = document.getElementById('cards-container');

data.forEach((item, itemIndex) => {
	const card = document.createElement('article');
	card.className = 'card';
	card.setAttribute('aria-labelledby', `question-${itemIndex}`);

	const cardPanel = document.createElement('div');
	cardPanel.className = 'card-panel';

	const resultsUi = document.createElement('div');
	resultsUi.className = 'results-ui';

	const question = document.createElement('div');
	question.className = 'question';
	question.id = `question-${itemIndex}`;
	question.setAttribute('role', 'heading');
	question.setAttribute('aria-level', '1');
	question.title = item.question;
	question.textContent = item.question;
	resultsUi.appendChild(question);

	const answersContainer = document.createElement('div');
	answersContainer.className = 'answers-container';
	answersContainer.classList.add('hit-results');
	answersContainer.setAttribute('aria-live', 'polite');
	answersContainer.id = `answers-${itemIndex}`;

	item.answers.forEach((answerItem, answerIndex) => {
		const answer = document.createElement('div');
		answer.className = 'answer info-heading';

		const answerText = document.createElement('span');
		answerText.className = 'answer-text';
		answerText.classList.add(`answer-${itemIndex}-${answerIndex}`);
		answerText.id = `answer-${itemIndex}-${answerIndex}`;
		answerText.textContent = answerItem.answer.length > 44 ? answerItem.answer.substring(0, 44) + '...' : answerItem.answer;
		answerText.title = answerItem.answer;

		const button = document.createElement('button');
		button.className = 'system-button';
		button.textContent = 'Expand';
		button.setAttribute('aria-controls', `answer-${itemIndex}-${answerIndex}`);
		button.setAttribute('aria-expanded', 'false');
		button.onclick = () => {
			const expanded = button.getAttribute('aria-expanded') === 'true';
			answerText.innerHTML = expanded ? answerItem.answer.substring(0, 44) + '...' : `${answerItem.answer} <cite>${answerItem.cite}</cite>`;
			button.textContent = expanded ? 'Expand' : 'Less';
			button.setAttribute('aria-expanded', !expanded);
			(!expanded) ? answersContainer.classList.add('answer-reveal') : null;
			(!expanded) ? answerText.classList.add('highlight') : answerText.classList.remove('highlight') ;
		};

		answer.appendChild(answerText);
		answer.appendChild(button);
		answersContainer.appendChild(answer);
	});

	resultsUi.appendChild(answersContainer);

	const timestamp = document.createElement('div');
	timestamp.className = 'timestamp info-heading';
	timestamp.textContent = item.stats.timestamp;
	timestamp.setAttribute('aria-label', `Timestamp: ${item.stats.timestamp}`);
	resultsUi.appendChild(timestamp);

	const duration = document.createElement('div');
	duration.className = 'duration info-heading';
	duration.innerHTML = `<span>${item.stats.hits} RESULTS</span> <span>${item.stats.duration}</span>`;
	duration.setAttribute('aria-label', `Duration: ${item.stats.hits} results in ${item.stats.duration}`);
	resultsUi.appendChild(duration);

	const searchEngine = document.createElement('div');
	searchEngine.className = 'search-engine info-heading';
	searchEngine.innerHTML = `<span>${item.stats.query_engine}: </span><span>${item.stats.source}</span>`;
	searchEngine.setAttribute('aria-label', `Search engine: ${item.stats.query_engine}, Source: ${item.stats.source}`);
	resultsUi.appendChild(searchEngine);

	const leftButtons = document.createElement('div');
	leftButtons.className = 'left-buttons';

	const playButton = document.createElement('button');
	playButton.className = 'pill-button play';
	playButton.setAttribute('title', 'RUN query');
	playButton.setAttribute('aria-label', 'Run query once and expand answers');
	playButton.setAttribute('aria-controls', `answers-${itemIndex}`);
	playButton.setAttribute('aria-expanded', 'false');
	playButton.innerHTML = '<i class="fas fa-play"></i> Play <i class="fa-sharp fa-solid fa-info"></i>';
	leftButtons.appendChild(playButton);

	playButton.onclick = (event) => {
		const expanded = playButton.getAttribute('aria-expanded') === 'true';
		const currentCard = event.currentTarget.closest('.card'); // Find the closest .card element

		if (expanded) {
			answersContainer.classList.remove('expanded');
			answersContainer.classList.remove('answer-reveal');
			question.classList.remove('dim'); // Restore question prominence
			document.querySelectorAll('.card').forEach(card => {
				if (card !== currentCard) { // Skip the current card
					card.classList.remove('blip');
					card.classList.remove('hide');
				}
			});
			document.querySelectorAll('.chart').forEach(chart => chart.classList.remove('active'));
			deleteButton.classList.remove('hide');
			resetExpandButtons(answersContainer); // Reset expand buttons when collapsing
		} else {
			answersContainer.classList.add('expanded');
			question.classList.add('dim'); //Dim question
			document.querySelectorAll('.card').forEach(card => {
				if (card !== currentCard) { // Skip the current card
					card.classList.add('hide');
				}
			});
			currentCard.classList.remove('hide'); // Ensure current card is not hidden
			document.querySelectorAll('.chart').forEach(chart => chart.classList.add('active'));
			deleteButton.classList.add('hide'); // Prevent card deletion when open state

			setTimeout(() => {
				document.querySelectorAll('.card').forEach(card => {
					if (card !== currentCard) { // Skip the current card
						card.classList.add('blip');
					}
				});
				currentCard.classList.remove('blip'); // Ensure current card does not have 'blip'
			}, 1000); // 2 seconds timeout for the animation to complete

			// Load the appropriate data based on the current question
			const currentQuestionKey = getCurrentQuestionKey(); // Implement this function to return the current question key

			// Destroy existing SVG charts if they exist
			d3.select("#candlestick-svg").selectAll("*").remove();
			d3.select("#bollinger-svg").selectAll("*").remove();

			loadChartData(currentQuestionKey);
		}
		playButton.setAttribute('aria-expanded', !expanded);
	};

	const repeatButton = document.createElement('button');
	repeatButton.className = 'pill-button repeat';
	repeatButton.setAttribute('title', 'SET query frequency');
	repeatButton.setAttribute('aria-label', 'Set repeat query');
	repeatButton.innerHTML = '<i class="fas fa-stopwatch"></i>  <i class="fa-sharp fa-solid fa-info"></i>';
	leftButtons.appendChild(repeatButton);

	resultsUi.appendChild(leftButtons);

	const rightButtons = document.createElement('div');
	rightButtons.className = 'right-buttons';

	const deleteButton = document.createElement('button');
	deleteButton.className = 'delete';
	deleteButton.setAttribute('title', 'DELETE query');
	deleteButton.setAttribute('aria-label', 'Delete query');
	deleteButton.innerHTML = '<i class="fas fa-trash"></i> Delete';
	deleteButton.onclick = () => {
		card.classList.add('animate__animated', 'animate__hinge');
		setTimeout(() => {
			cardsContainer.removeChild(card);
		}, 2000); // 2 seconds timeout for the animation to complete
	};

	rightButtons.appendChild(deleteButton);
	resultsUi.appendChild(rightButtons);

	cardPanel.appendChild(resultsUi);
	card.appendChild(cardPanel);

	const localSettingsPanel = document.createElement('div');
	localSettingsPanel.className = 'local-settings-panel';
	localSettingsPanel.setAttribute('aria-live', 'polite');
	localSettingsPanel.id = `local-settings-${itemIndex}`;

	const editTitle = document.createElement('div');
	editTitle.className = 'edit-title info-heading';
	editTitle.textContent = 'Edit question:';
	editTitle.setAttribute('role', 'heading');
	editTitle.setAttribute('aria-level', '1');
	localSettingsPanel.appendChild(editTitle);

	const questionEdit = document.createElement('textarea');
	questionEdit.name = `question-edit-${itemIndex}`;
	questionEdit.className = 'question-edit';
	questionEdit.id = `question-edit-${itemIndex}`;
	questionEdit.title = `EDIT: ${item.question}`;
	questionEdit.setAttribute('tabindex','-1');
	questionEdit.textContent = item.question;
	questionEdit.setAttribute('rows', '3');
	localSettingsPanel.appendChild(questionEdit);

	questionEdit.addEventListener('input', () => {
		question.title = questionEdit.value;
		question.textContent = questionEdit.value;
	});

	const sourceTitle = document.createElement('div');
	sourceTitle.className = 'source-title info-heading';
	sourceTitle.textContent = 'Source selections:';
	sourceTitle.setAttribute('role', 'heading');
	sourceTitle.setAttribute('aria-level', '2');
	localSettingsPanel.appendChild(sourceTitle);

	const sourceDb = document.createElement('ul');
	sourceDb.className = 'source-db';

	const sources = [
		{ name: 'Google', checked: item.answers.some(answer => answer.search_engine === 'Google') },
		{ name: 'Bing', checked: item.answers.some(answer => answer.search_engine === 'Bing') },
		{ name: 'Yahoo Finance', checked: item.answers.some(answer => answer.search_engine === 'Yahoo Finance') },
		{ name: 'Public DB', checked: false },
		{ name: 'Private DB', checked: false },
		{ name: 'AI (β)', checked: false }
	];

	sources.forEach((source, sourceIndex) => {
		const defaultCheck = document.createElement('li');
		defaultCheck.className = `default-check-${itemIndex}`;

		const sourceCheck = document.createElement('input');
		sourceCheck.name = `source-check-${itemIndex}-${sourceIndex}`;
		sourceCheck.className = 'source-check active';
		sourceCheck.id = `source-check-${itemIndex}-${sourceIndex}`;
		sourceCheck.checked = source.checked;
		sourceCheck.type = 'checkbox';
		sourceCheck.value = source.name;

		const sourceLabel = document.createElement('label');
		sourceLabel.className = `source-label info-heading active`;
		sourceLabel.setAttribute('for', `source-check-${itemIndex}-${sourceIndex}`);
		sourceLabel.textContent = source.name;

		sourceCheck.addEventListener('change', () => {
			if (sourceCheck.checked) {
			sourceCheck.classList.add('active');
			sourceLabel.classList.add('active');
			} else {
			sourceCheck.classList.remove('active');
			sourceLabel.classList.remove('active');
			}
		});

		defaultCheck.appendChild(sourceCheck);
		defaultCheck.appendChild(sourceLabel);
		sourceDb.appendChild(defaultCheck);
	});

	localSettingsPanel.appendChild(sourceDb);
	cardsContainer.appendChild(card);

	// Create the feedback button
	const feedbackButton = document.createElement('button');
	feedbackButton.className = 'feedback';
	feedbackButton.setAttribute('title', 'SEND feedback');
	feedbackButton.setAttribute('aria-label', 'Send feedback');
	feedbackButton.innerHTML = '<i class="fa-solid fa-square-pen"></i>';

	// Generate the mailto link
	const mailtoLink = `mailto:hello@metisx.io?subject=${encodeURIComponent(item.stats.timestamp + ' ' + item.stats.source + ' Feedback report')}&body=・Your%20opinion%20on%20the%20accuracy%3F%0D%0A・Any%20other%20topics%20of%20concern%3F`;
	feedbackButton.onclick = () => {
		console.log(mailtoLink);
		window.location.href = mailtoLink;
	};

	// Append the feedback button to the local settings panel
	localSettingsPanel.appendChild(feedbackButton);

	cardPanel.appendChild(localSettingsPanel);

	const editButton = document.createElement('button');
	editButton.className = 'edit';
	editButton.setAttribute('title', 'EDIT question');
	editButton.setAttribute('aria-label', 'Edit question');
	editButton.setAttribute('aria-controls', `local-settings-${itemIndex}`);
	editButton.setAttribute('aria-expanded', 'false');
	editButton.innerHTML = '<i class="fas fa-cog"></i> Edit';

	editButton.onclick = () => {
		const expanded = editButton.getAttribute('aria-expanded') === 'true';
		if (expanded) {
			localSettingsPanel.classList.remove('active');
			questionEdit.setAttribute('tabindex', '-1');
			updateTabindex(localSettingsPanel, '-1');
			deleteButton.setAttribute('tabindex', '0');
		} else {
			localSettingsPanel.classList.add('active');
			questionEdit.setAttribute('tabindex', '0');
			updateTabindex(localSettingsPanel, '0');
			deleteButton.setAttribute('tabindex', '-1');
			setTimeout(() => {
			questionEdit.focus();
			}, 0);
		}
		editButton.setAttribute('aria-expanded', !expanded);
	};

	cardPanel.appendChild(editButton);
	cardsContainer.appendChild(card);
});

//Add stats dash

const statAdxCanvas = document.createElement('div');
statAdxCanvas.className = 'chart adx-canvas';
statAdxCanvas.innerHTML =`
<h3>ADX Chart</h3>
<canvas id="adx-canvas"></canvas>`;
cardsContainer.appendChild(statAdxCanvas);

const statAtrCanvas = document.createElement('div');
statAtrCanvas.className = 'chart atr-svg';
statAtrCanvas.innerHTML =`
<h3>ATR Chart</h3>
<svg id="atr-svg"></svg>`;
cardsContainer.appendChild(statAtrCanvas);

const statBarCanvas = document.createElement('div');
statBarCanvas.className = 'chart bar-canvas';
statBarCanvas.innerHTML =`
<h3>Bar Chart</h3>
<canvas id="bar-canvas"></canvas>`;
cardsContainer.appendChild(statBarCanvas);

const statBollingerCanvas = document.createElement('div');
statBollingerCanvas.className = 'chart bollinger-svg';
statBollingerCanvas.innerHTML =`
<h3>Bollinger Chart</h3>
<svg id="bollinger-svg"></svg>`;
cardsContainer.appendChild(statBollingerCanvas);

const statCandlestick = document.createElement('div');
statCandlestick.className = 'chart candlestick-svg';
statCandlestick.innerHTML =`
<h3>Candlestick Chart</h3>
<svg id="candlestick-svg"></svg>`;
cardsContainer.appendChild(statCandlestick);

const statFibonacciCanvas = document.createElement('div');
statFibonacciCanvas.className = 'chart fibonacci-svg';
statFibonacciCanvas.innerHTML =`
<h3>Fibonacci Chart</h3>
<svg id="fibonacci-svg"></svg>`;
cardsContainer.appendChild(statFibonacciCanvas);

const statIchimokuCanvas = document.createElement('div');
statIchimokuCanvas.className = 'chart ichimoku-svg';
statIchimokuCanvas.innerHTML =`
<h3>Ichimoku Chart</h3>
<svg id="ichimoku-svg"></svg>`;
cardsContainer.appendChild(statIchimokuCanvas);

const statLineCanvas = document.createElement('div');
statLineCanvas.className = 'chart line-canvas';
statLineCanvas.innerHTML =`
<h3>Line Chart</h3>
<canvas id="line-canvas"></canvas>`;
cardsContainer.appendChild(statLineCanvas);

const statMacdCanvas = document.createElement('div');
statMacdCanvas.className = 'chart macd-canvas';
statMacdCanvas.innerHTML =`
<h3>MACD Chart</h3>
<canvas id="macd-canvas"></canvas>`;
cardsContainer.appendChild(statMacdCanvas);

const statMovingCanvas = document.createElement('div');
statMovingCanvas.className = 'chart moving-average-canvas';
statMovingCanvas.innerHTML =`
<h3>Moving Average Chart</h3>
<canvas id="moving-average-canvas"></canvas>`;
cardsContainer.appendChild(statMovingCanvas);

const statPivotCanvas = document.createElement('div');
statPivotCanvas.className = 'chart pivot-canvas';
statPivotCanvas.innerHTML =`
<h3>Pivot Chart</h3>
<canvas id="pivot-canvas"></canvas>`;
cardsContainer.appendChild(statPivotCanvas);

const statRsiCanvas = document.createElement('div');
statRsiCanvas.className = 'chart rsi-canvas';
statRsiCanvas.innerHTML =`
<h3>RSI Chart</h3>
<canvas id="rsi-canvas"></canvas>`;
cardsContainer.appendChild(statRsiCanvas);

const statStochasticCanvas = document.createElement('div');
statStochasticCanvas.className = 'chart stochastic-svg';
statStochasticCanvas.innerHTML =`
<h3>Stochastic Chart</h3>
<svg id="stochastic-svg"></svg>`;
cardsContainer.appendChild(statStochasticCanvas);

const statVolumeCanvas = document.createElement('div');
statVolumeCanvas.className = 'chart volume-canvas';
statVolumeCanvas.innerHTML =`
<h3>Volume Chart</h3>
<canvas id="volume-canvas"></canvas>`;
cardsContainer.appendChild(statVolumeCanvas);

const asideMenu = document.querySelector('aside');
const targetLogo = document.querySelector('.company-name');
asideMenu.addEventListener("mouseover", () => {
targetLogo.classList.add('active');
});

asideMenu.addEventListener("mouseout", () => {
targetLogo.classList.remove('active');
});

function resizeTextarea(textarea) {
	textarea.style.height = 'auto';
	textarea.style.height = textarea.scrollHeight + 'px';
	if (textarea.scrollHeight > 40) {
		textarea.classList.add('resized');
	} else {
		textarea.classList.remove('resized');
	}
}

function toggleButtonState(textarea, tag) {
	if (textarea.value.length >= 3) {
		tag.classList.add('active');
	} else {
		tag.classList.remove('active');
	}
}

function clearTextarea(textarea, button) {
	textarea.value = '';
	resizeTextarea(textarea);
	toggleButtonState(textarea, button);
}

let textarea = document.querySelector(".mtx-search-input");
let safeSearch = document.querySelector(".mtx-search");
let clearButton = document.querySelector(".mtx-search-clear");

clearButton.addEventListener("click", () => {
	clearTextarea(textarea, safeSearch);
});

textarea.addEventListener("input", () => {
	resizeTextarea(textarea);
	toggleButtonState(textarea, safeSearch);
});

textarea.addEventListener("keyup", () => resizeTextarea(textarea));

resizeTextarea(textarea);
toggleButtonState(textarea, safeSearch);

function updateTabindex(container, value) {
	const focusableElements = container.querySelectorAll('textarea, input, label');
	focusableElements.forEach(element => {
		element.setAttribute('tabindex', value);
	});
}

function resetExpandButtons(container) {
	container.querySelectorAll('.answer button').forEach((button) => {
		button.setAttribute('aria-expanded', 'false');
		button.textContent = 'Expand';
		const answerText = button.previousElementSibling;
		if (answerText) {
			const answer = answerText.getAttribute('title');
			answerText.innerHTML = answer.length > 44 ? answer.substring(0, 44) + '...' : answer;
		}
	});
}

function updateAnswerButtonsTabindex(container, value) {
	const buttons = container.querySelectorAll('.answer button');
	buttons.forEach(button => {
		button.setAttribute('tabindex', value);
	});
}

// Handles the menu
function openAside() {
	const aside = document.querySelector('aside');

	// function to open the aside
	function open() {
		aside.setAttribute('aria-expanded', 'true');
		aside.classList.add('active');

		// add event listener to close the aside when clicking outside of it
		document.addEventListener('click', closeOnClickOutside);
		document.addEventListener('touchstart', closeOnClickOutside);
	}

	// function to close the aside
	function close() {
		aside.setAttribute('aria-expanded', 'false');
		aside.classList.remove('active');

		// remove the event listener when the aside is closed
		document.removeEventListener('click', closeOnClickOutside);
		document.removeEventListener('touchstart', closeOnClickOutside);
	}

	// function to handle closing the aside when clicking outside of it
	function closeOnClickOutside(event) {
		if (!aside.contains(event.target)) {
			close();
		}
	}

	// Add event listener to the aside tag to open it when clicked
	aside.addEventListener('click', function(event) {
		// prevent the click event from propagating to the document
		event.stopPropagation();

		// open the aside
		open();
	});
}

// Call the function to initialize the functionality
openAside();