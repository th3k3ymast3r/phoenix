// Darkmode if at or after sunset
const calculateSunriseSunset = (() => {
	let d = new Date();
	let lat = parseFloat(businessLatitude);
	let lng = parseFloat(businessLongitude);
	let z = 102;

	// local offset could be calculated with the Google TimeZone API or others
	let o = -7;
	let katy = new SunriseSunset(d.getFullYear(), d.getMonth() + 1, d.getDate(), lat, lng, z);
	let rise = Math.floor(katy.sunriseLocalHours(o));
	let set = Math.floor(katy.sunsetLocalHours(o));
	let cTime = d.getHours();

	if (set < cTime || cTime < rise){
		htmlElement.classList.add('dark-knight');
		if(document.querySelector('.dark-mode')){
			document.querySelector('.dark-mode').classList.add('enabled');
		}
	}

	if (debug) debugLog(`sunset: ${set} sunrise: ${rise} currentTime: ${cTime}`);
})();