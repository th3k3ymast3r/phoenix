// SunriseSunset Class (2011-01-30)
//   Implementation of http://williams.best.vwh.net/sunrise_sunset_algorithm.htm
//
//   Copyright (c) 2011, Preston Hunt <me@prestonhunt.com>
//   All rights reserved.
//
//   Redistribution and use in source and binary forms, with or without
//   modification, are permitted provided that the following conditions
//   are met:
//
//   Redistributions of source code must retain the above copyright
//   notice, this list of conditions and the following disclaimer.
//   Redistributions in binary form must reproduce the above copyright
//   notice, this list of conditions and the following disclaimer in the
//   documentation and/or other materials provided with the
//   distribution.
//
//   The name of Preston Hunt may not be used to endorse or promote
//   products derived from this software without specific prior written
//   permission.
//
//   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
//   FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
//   COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
//   INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//   (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
//   SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
//   HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
//   STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
//   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
//   OF THE POSSIBILITY OF SUCH DAMAGE.
//
//   Provides sunrise and sunset times for specified date and position.
//   All dates are UTC.  Year is 4-digit.  Month is 1-12.  Day is 1-31.
//   Longitude is positive for east, negative for west.
//
//   Sample usage:
//   var tokyo = new SunriseSunset( 2011, 1, 19, 35+40/60, 139+45/60);
//   tokyo.sunriseUtcHours()	  --> 21.8199 = 21:49 GMT
//   tokyo.sunsetUtcHours()	   --> 7.9070  = 07:54 GMT
//   tokyo.sunriseLocalHours(9)   --> 6.8199  = 06:49 at GMT+9
//   tokyo.sunsunsetLocalHours(9) --> 16.9070 = 16:54 at GMT+9
//   tokyo.isDaylight(1.5)		--> true
//
//   var losangeles = new SunriseSunset( 2011, 1, 19, 34.05, -118.233333333 );
//   etc.
(function(Math, window) {

var MPI = Math.PI;

function sin(deg) {
	return Math.sin(deg * MPI / 180);
};
function cos(deg) {
	return Math.cos(deg * MPI / 180);
};
function tan(deg) {
	return Math.tan(deg * MPI / 180);
};
function asin(x) {
	return (180 / MPI) * Math.asin(x);
};
function acos(x) {
	return (180 / MPI) * Math.acos(x);
};
function atan(x) {
	return (180 / MPI) * Math.atan(x);
};


var SunriseSunset = function(utcFullYear, utcMonth, utcDay, latitude, longitude) {
	this.zenith = 90.83;
	this.utcFullYear = utcFullYear;
	this.utcMonth = utcMonth;
	this.utcDay = utcDay;
	this.latitude = latitude;
	this.longitude = longitude;
	this.lngHour = longitude / 15;
	this.rising = true;
};

SunriseSunset.prototype = {
	getDOY: function() {
		var ndate = new Date();
		return Math.ceil((ndate - (new Date(ndate.getFullYear(), 0, 1))) / 86400000);
	},

	approximateTime: function() {
		return this.getDOY() + ((this.rising ? 6 : 18 - this.lngHour) / 24)
	},

	meanAnomaly: function() {
		return (0.9856 * this.approximateTime()) - 3.289;
	},

	trueLongitude: function() {
		var M = this.meanAnomaly();
		return (M + (1.916 * sin(M)) + (0.020 * sin(2 * M)) + 282.634) % 360;
	},

	rightAscension: function() {
		var L = this.trueLongitude(),
			RA = (atan(0.91764 * tan(L))) % 360,
			Lquadrant  = (Math.floor( L/90)) * 90,
			RAquadrant = (Math.floor(RA/90)) * 90;
		return (RA + (Lquadrant - RAquadrant)) / 15;
	},

	sinDec: function() {
		return 0.39782 * sin(this.trueLongitude());
	},

	cosDec: function() {
		return cos(asin(this.sinDec()));
	},

	localMeanTime: function() {
		var cosH = (cos(this.zenith) - (this.sinDec() * sin(this.latitude))) / (this.cosDec() * cos(this.latitude)),
			RA = this.rightAscension(),
			t = this.approximateTime(),
			H = (this.rising ? 360 - acos(cosH) : acos(cosH)) / 15,
			T = H + RA - (0.06571 * t) - 6.622;
		return (cosH > 1) ?
				"the sun never rises on this location (on the specified date)" : (cosH < -1) ?
			"the sun never sets on this location (on the specified date)" : T;
	},

	UTCTime: function() {
		return (this.localMeanTime() - this.lngHour) % 24;
	},

	sunriseUtcHours: function() {
		this.rising = true;
		return this.UTCTime();
	},

	sunsetUtcHours: function() {
		this.rising = false;
		return this.UTCTime();
	},

	hoursRange: function(h) {
		return (h >= 24) ? (h - 24) : ((h < 0) ? (h + 24) : h);
	},

	sunriseLocalHours: function(gmt) {
		return this.hoursRange(gmt + this.sunriseUtcHours());
	},

	sunsetLocalHours: function(gmt) {
		return this.hoursRange(gmt + this.sunsetUtcHours());
	},

	isDaylight: function( utcCurrentHours ) {
		var sunriseHours = this.sunriseUtcHours(),
			sunsetHours = this.sunsetUtcHours();
		return (sunsetHours < sunriseHours) ?
			(utcCurrentHours > sunriseHours || utcCurrentHours < sunsetHours) ?
				true : false : (utcCurrentHours >= sunriseHours) ?
			(utcCurrentHours < sunsetHours) : false;
	}
};

window.SunriseSunset = SunriseSunset;

})(Math, this);
