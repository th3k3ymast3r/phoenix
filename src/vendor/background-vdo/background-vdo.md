// READme install video background for site desktop viewport only
// Requires Gulp V5 + Node v21.7.3
// Plugins: 
// plumber (gulp-plumber), 
// sourcemaps (gulp-sourcemaps), 
// sass (gulp-sass), 
// sassGlob (gulp-sass-glob), 
// autoprefixer (gulp-autoprefixer), 
// cleanCSS (gulp-clean-css), 
// rename (gulp-rename), 
// gzip (gulp-gzip),
// log (fancy-log)

// Paths code
// const paths = {
    // Not required if using SASS
    // backgroundvdocss : {
	// 	src : "src/vendor/background-vdo/css/*.css",
	// 	dest : "dist/assets/css/background-vdo"
	// },
	backgroundvdosass : {
		src : [
			"src/vendor/background-vdo/sass/*.sass",
			"src/vendor/background-vdo/sass/partials/*.scss"
		],
		dest : "dist/assets/css/background-vdo"
	},
	backgroundvdojs : {
		src : "src/vendor/background-vdo/js/**/*.js",
		dest : "dist/assets/js/background-vdo"
	},
	backgroundvdohtml : {
		src : "src/vendor/background-vdo/*.ejs",
		dest : "dist"
	},
    backgroundvdoimg : {
		src : "src/vendor/background-vdo/images/*.jpg",
		dest : "dist/assets/images/background-vdo"
	},
    backgroundvdo : {
		src : "src/vendor/background-vdo/video/*.{mp4,webm,vtt}",
		dest : "dist/assets/video/background-vdo"
	},
// };


// Process function -> file.{sass, scss} = file.{css, gzip}
// For SASS/SCSS + GZIP files and minifying
export function cssminifyandgzip({ source, destination, paths, task }) {
	return src(source, { since : lastRun(task), encoding : false })
		.pipe(plumber({ errorHandler: handleerror }))
		.pipe(sourcemaps.init())
		.pipe(sassGlob())
		.pipe(sassCompiler({
			outputStyle: 'compressed',
			includePaths: paths,
			errLogToConsole: true
		}).on('error', sassCompiler.logError))
		.pipe(autoprefixer("last 2 version", "safari 5", "ie 8", "ie 9", "opera 12.1", "ios 6", "android 4"))
		.pipe(cleanCSS({debug: true}, (details) => {
			// Extract the relative file name without extension
			const basename = path.basename(details.name, path.extname(details.name));
			const originalsizekb = details.stats.originalSize / 1024;
			const minifiedsizekb = details.stats.minifiedSize / 1024;
			log(`${basename}.css` + `: ${originalsizekb.toFixed(3)} kb`);
			log(`${basename}.min.css` + `: \x1b[38;2;249;42;114m${minifiedsizekb.toFixed(3)} kb\x1b[0m`);
		}))
		.pipe(sourcemaps.write({
			includeContent : false,
			sourceRoot : file => `file:///Users/th3k3ymast3r/dev/phoenix/src/css`
		}))
		.pipe(rename({
			suffix: ".min"
		}))
		.pipe(dest(destination))
		.pipe(gzip())
		.pipe(dest(destination))
}

//For EJS -> HTML + GZIP
export function htmlandgzip({ source, destination, ext, task }) {
	return src(source, { since : lastRun(task), encoding : false })
		.pipe(plumber({ errorHandler: handleerror }))
		.pipe(ejs())
		.pipe(rename({ extname: ext }))
		.pipe(ignore.exclude('partials/**'))
		.pipe(dest(destination))
		.pipe(gzip())
		.pipe(dest(destination));
}

//For JS + GZIP
function jsandgzip({ source, destination, basename, ext, task }) {
	return src(source, { since : lastRun(task), encoding : false })
		.pipe(plumber({ errorHandler: handleerror }))
		// Condition = background-vdo.js
		.pipe(gulpif(basename+ext==="background-vdo.js", concat("background-vdo.js")))
		.pipe(gulpif(basename+ext==="background-vdo.js", include()))
		.pipe(uglify())
		.pipe(rename({
			suffix: ".min",
			extname: ext
		}))
		.pipe(dest(destination))
		.pipe(gzip())
		.pipe(dest(destination))
}

// For Copy + GZIP only
export function copyandgzip({ source, destination, basement, task }) {
	let copiedFiles = [];

	return src(source, { since : lastRun(task), base : basement, encoding : false })
		.on('data', (file) => {
			const basename = path.basename(file.relative, path.extname(file.relative));

			if (basename && !copiedFiles.includes(basename)) {
				log(`\x1b[1m⇧\x1b[0m '\x1b[38;2;102;217;239m${basename}${file.extname}\x1b[0m'`);
			}
			if (basename && basename !== '' && basename !== null && basename !== undefined && basename !== '-1') {
				copiedFiles.push(basename);
			}
		})
		.pipe(dest(destination))
		.pipe(gzip())
		.pipe(dest(destination));
}

// Export function

export function backgroundvdosass() {
	return cssminifyandgzip({
		source : paths.backgroundvdosass.src,
		destination : paths.backgroundvdosass.dest,
		paths : "src/vendor/background-vdo/partials",
		task : "backgroundvdosass"
	});
}

// Not required if using backgroundvdosass function
// export function backgroundvdocss() {
// 	return cssminifyandgzip({
// 		source : paths.backgroundvdocss.src,
// 		destination : paths.backgroundvdocss.dest,
// 		task : "backgroundvdocss"
// 	});
// }

export function backgroundvdojs() {
	return jsandgzip({
		source : paths.backgroundvdojs.src,
		destination : paths.backgroundvdojs.dest,
		basename : "background-vdo",
		ext : ".js",
		task : "backgroundvdojs"
	});
}

export function backgroundvdohtml() {
	return htmlandgzip({
		source : paths.backgroundvdohtml.src,
		destination : paths.backgroundvdohtml.dest,
        ext : ".html",
		task : "backgroundvdohtml"
	});
}

export function backgroundvdoimg() {
	return compressimg({
		source : paths.backgroundvdoimg.src,
		destination : paths.backgroundvdoimg.dest,
		task : "backgroundvdoimg"
	});
}

export function backgroundvdo() {
	return copyandgzip({
		source : paths.backgroundvdo.src,
		destination : paths.backgroundvdo.dest,
		task : "backgroundvdo"
	});
}

// Watch function
watch(paths.backgroundvdohtml.src, gulp.series('backgroundvdohtml', reload)).on('change', path => { 
    log({ title: 'backgroundvdo html update detected', message: `File ${path} was changed` });
});
watch(paths.backgroundvdosass.src, gulp.series('backgroundvdosass', reload)).on('change', path => { 
    log({ title: 'backgroundvdo css edit detected', message: `File ${path} was changed` });
});
// Not required if using backgroundvdosass watch
// watch(paths.backgroundvdocss.src, gulp.series('backgroundvdocss', reload)).on('change', path => { 
//     log({ title: 'backgroundvdo css edit detected', message: `File ${path} was changed` });
// });
watch(paths.backgroundvdojs.src, gulp.series('backgroundvdojs', reload)).on('change', path => { 
    log({ title: 'backgroundvdo js edit detected', message: `File ${path} was changed` });
});
watch(paths.backgroundvdoimg.src, gulp.series('backgroundvdoimg', reload)).on('change', path => { 
    log({ title: 'backgroundvdo images update detected', message: `File ${path} was changed` });
});
watch(paths.backgroundvdo.src, gulp.series('backgroundvdo', reload)).on('change', path => { 
    log({ title: 'backgroundvdo video update detected', message: `File ${path} was changed` });
});

//Build task
// Build
// export const build = series(
	backgroundvdofonts,
	backgroundvdosass,
    // Not required if using backgroundvdosass function
    // backgroundvdocss,
    backgroundvdojs,
	backgroundvdohtml
// );
// export { build as default };