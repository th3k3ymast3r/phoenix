const bgvid = document.getElementById('bgvid');
const cube1 = document.querySelector('.cube[aria-label="Methodologies"]')
const cube2 = document.querySelector('.cube[aria-label="Design overviews"]')
let cubeAvailable = (cube1 && cube2);

// Pause 3D menu
const toggle3Dmenu = function(){
	if (debug) debugLog('toggle3Dmenu fn triggered');
	if (cubeAvailable){
		if (isDesktop && isAside()) cube1.classList.add('pause');
		if (isDesktop && isAside()) cube2.classList.add('pause');
	}
	if (isDesktop && !isAside()){
		document.querySelectorAll('.cube').forEach(function(element){
			element.classList.remove('pause');
		});
	}
}

// Pause background video for performance
if (bgvid){
	let asidePause = function(){
		if (debug) debugLog('asidePause fn ~ Pausing video triggered');
		cube1.classList.add('aside-pause');
		cube2.classList.add('aside-pause');

		if (!bgvid.paused) {
			bgvid.pause();
		}
	}

	let asidePlay = function(){
		if (debug) debugLog('asidePlay fn ~ Playing video triggered');
		document.querySelector('.cube').classList.remove('aside-pause');
		if (bgvid.paused) {
			bgvid.play();
		}
	}

	if (isAside()){
		const observer = new MutationObserver((mutations) => {
			mutations.forEach((mutation) => {
				if (mutation.attributeName === 'class') {
					if (isAside()) {
						asidePause();
					} else {
						asidePlay();
					}
				}
			});
		});

		observer.observe(aside, { attributes: true });
	}
} else {
	if (debug) debugLog('No video element found with ID "bgvid".');
}