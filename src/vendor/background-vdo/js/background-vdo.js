// Constants and initial settings
const vidContainer = document.getElementById('background-vdo');
const portableDevice = !navigator.userAgent.match(/iPad/) && window.innerWidth <= 768;
const hashParam = [...Array(10)].map(()=>(~~(Math.random()*36)).toString(36)).join('');

// Apply theme via URL
const themeOveride = () => {
	if (debug) debugLog('* themeOveride fn triggered *')
	if (!theme) return;
	
	//clear preset season
	document.documentElement.classList.remove('spring', 'summer', 'fall', 'winter');

	if (debug) debugLog(`themeOveride theme detected as: ${theme}`);

	//overide with URL param theme
	if (theme === 'spring'){
		updateBackground(window.sensor.springFname);
		updateThemeAssets(theme, window.sensor.springFname);
	}
	if (theme === 'summer'){
		updateBackground(window.sensor.summerFname);
		updateThemeAssets(theme, window.sensor.summerFname);
	}
	if (theme === 'fall'){
		updateBackground(window.sensor.fallFname);
		updateThemeAssets(theme, window.sensor.fallFname);
	}
	if (theme === 'winter'){
		updateBackground(window.sensor.winterFname);
		updateThemeAssets(theme, window.sensor.winterFname);
	}
};

// Apply browser light theme
const applyBrowserTheme = () => {
	if (debug) debugLog('* applyBrowserTheme fn triggered *');

	if (!document.documentElement.classList.contains('dark-knight')){
		if (document.documentElement.classList.contains('spring')){
			document.querySelector(`meta[media='(prefers-color-scheme: light)']`).setAttribute('content', window.sensor.springColor);
			document.querySelector(`meta[name='msapplication-navbutton-color']`).setAttribute('content', window.sensor.springColor);
			document.querySelector(`meta[name='apple-mobile-web-app-status-bar-style']`).setAttribute('content', window.sensor.springColor);
		}
		if (document.documentElement.classList.contains('summer')){
			document.querySelector(`meta[media='(prefers-color-scheme: light)']`).setAttribute('content', window.sensor.summerColor);
			document.querySelector(`meta[name='msapplication-navbutton-color']`).setAttribute('content', window.sensor.summerColor);
			document.querySelector(`meta[name='apple-mobile-web-app-status-bar-style']`).setAttribute('content', window.sensor.summerColor);
		}
		if (document.documentElement.classList.contains('fall')){
			document.querySelector(`meta[media='(prefers-color-scheme: light)']`).setAttribute('content', window.sensor.fallColor);
			document.querySelector(`meta[name='msapplication-navbutton-color']`).setAttribute('content', window.sensor.fallColor);
			document.querySelector(`meta[name='apple-mobile-web-app-status-bar-style']`).setAttribute('content', window.sensor.fallColor);
		}
		if (document.documentElement.classList.contains('winter')){
			document.querySelector(`meta[media='(prefers-color-scheme: light)']`).setAttribute('content', window.sensor.winterColor);
			document.querySelector(`meta[name='msapplication-navbutton-color']`).setAttribute('content', window.sensor.winterColor);
			document.querySelector(`meta[name='apple-mobile-web-app-status-bar-style']`).setAttribute('content', window.sensor.winterColor);
		}
	} else {
		document.querySelector(`meta[media='(prefers-color-scheme: light)']`).setAttribute('content', window.sensor.darkColor);
		document.querySelector(`meta[name='msapplication-navbutton-color']`).setAttribute('content', window.sensor.darkColor);
		document.querySelector(`meta[name='apple-mobile-web-app-status-bar-style']`).setAttribute('content', window.sensor.darkColor);
	}
}

// Apply season theme to DOM
function applyTheme(season){
	if (debug) debugLog(`applyTheme fn ~ season is: ${season}, class added`);
	document.documentElement.classList.add(season);
};

// Update season button state
function updateSeasonBtn(season){
	if (debug) debugLog(`updateSeasonBtn fn ~ season is: ${season}time, class added`);
	document.querySelector('.season-mode').classList.add(`${season}time`);
};

// Update twittercard background
function updateTwitterCard(filename){
	if (debug) debugLog(`updateTwitterCard fn ~ filename is:${filename}, -tc.jpg / -og.jpg`);
	document.querySelector('.twitter-card').setAttribute('content', `${window.location.origin}/assets/images/${filename}-tc.jpg?${hashParam}`);
	document.querySelector('.twitter-img').setAttribute('content', `${window.location.origin}/assets/images/${filename}-tc.jpg?${hashParam}`);
	document.querySelector('.open-graph').setAttribute('content', `${window.location.origin}/assets/images/${filename}-og.jpg?${hashParam}`);
};

// Apply season video background
function updateBackground(filename){
	if (debug) debugLog(`* updateBackground mode triggered * ~ ${filename} as args`);
	const video = document.getElementById('bgvid');

	if (!video) return;

	// remove all existing source elements
	const sources = video.querySelectorAll('source');
	sources.forEach(source => source.remove());
	if (debug) debugLog('updateBackground fn ~ sources removed *');

	// create and append a new source element
	const sourceElement = document.createElement('source');
	sourceElement.className = 'mp4';
	sourceElement.type = 'video/mp4';
	sourceElement.src = `/assets/video/background-vdo/${filename}.mp4`;
	if (debug) debugLog(`updateBackground fn ~ new source element created with ${filename}.mp4`);

	// remove all existing track elements
	const tracks = video.querySelectorAll('track');
	tracks.forEach(track => track.remove());
	if (debug) debugLog('updateBackground fn ~ tracks removed *');

	// create and append a new track element
	const trackElement = document.createElement('track');
	trackElement.setAttribute('default','');
	trackElement.className = 'vtt';
	trackElement.setAttribute('kind','captions');
	trackElement.setAttribute('srclang','en');
	trackElement.setAttribute('label','English');
	trackElement.src = `/assets/video/background-vdo/${filename}.vtt`;
	if (debug) debugLog(`updateBackground fn ~ new track element created with ${filename}.vtt`);

	video.appendChild(trackElement);
	video.appendChild(sourceElement);
	if (debug) debugLog('track and source elements appended to bgvid');

	video.setAttribute('poster', `/assets/images/background-vdo/${filename}.jpg?v=${hashParam}`);
	if (debug) debugLog(`updateBackground fn ~ poster attr set with ${filename}.jpg`);

	video.autoplay = true;
	video.load();

	if (debug) debugLog('* updateBackground mode complete * ~ video object set to autoplay and load');

	// unset #background-vdo background-image
	video.style.backgroundImage = '';
};

// Trigger theme asset states; htmlClass, season mode button and twittercard
function updateThemeAssets(season, filename){
	if (debug) debugLog(`updateThemeAssets fn ~ (invoked) ~ ${season} and ${filename} as args`);

	applyTheme(season);
	updateSeasonBtn(season);
	if (debug) debugLog('check season value as: '+ season);
	if (debug) debugLog('check filename value as: '+ filename);
	updateTwitterCard(filename);
};

// Trigger background-vdo load and theme-asset update
const loadBackground = function(){
	if (debug) debugLog('* loadBackground mode triggered *');

	// if seasonBtn is used, evaluate html class to update vdo
	if (htmlElement.classList.contains('season-override')){
		if (debug) debugLog('season-override mode is on');

		// remove current season
		htmlElement.classList.remove(isSeason);
		if (debug) debugLog(`season-override mode ~ html class ${isSeason} removed`);
		
		if (htmlElement.contains('spring')){
			updateBackground(window.sensor.springFname);
			updateThemeAssets('spring', window.sensor.springFname);
		}
		if (htmlElement.contains('summer')){
			updateBackground(window.sensor.summerFname);
			updateThemeAssets('summer', window.sensor.summerFname);
		}
		if (htmlElement.contains('fall')){
			updateBackground(window.sensor.fallFname);
			updateThemeAssets('fall', window.sensor.fallFname);
		}
		if (htmlElement.contains('winter')){
			updateBackground(window.sensor.winterFname);
			updateThemeAssets('winter', window.sensor.winterFname);
		}
	} else {
		if (debug) debugLog('season-override mode is off');

		if (isSeason === 'spring'){
			updateBackground(window.sensor.springFname);
			updateThemeAssets('spring', window.sensor.springFname);
		}
		if (isSeason === 'summer'){
			updateBackground(window.sensor.summerFname);
			updateThemeAssets('summer', window.sensor.summerFname);
		}
		if (isSeason === 'fall'){
			updateBackground(window.sensor.fallFname);
			updateThemeAssets('fall', window.sensor.fallFname);
		}
		if (isSeason === 'winter'){
			updateBackground(window.sensor.winterFname);
			updateThemeAssets('winter', window.sensor.winterFname);
		}
	
		if (debug) {
			if (isSeason === 'spring') debugLog(`loadBackground fn ~ Season detects ${isSeason} invokes updateBackground with ${springFname} and updateThemeAssets with 'spring', ${springFname} args`);
			if (isSeason === 'summer') debugLog(`loadBackground fn ~ Season detects ${isSeason} invokes updateBackground with ${summerFname} and updateThemeAssets with 'summer', ${summerFname} args`);
			if (isSeason === 'fall') debugLog(`loadBackground fn ~ Season detects ${isSeason} invokes updateBackground with ${fallFname} and updateThemeAssets with 'fall', ${fallFname} args`);
			if (isSeason === 'winter') debugLog(`loadBackground fn ~ Season detects ${isSeason} invokes updateBackground with ${winterFname} and updateThemeAssets with 'winter', ${winterFname} args`);
		}
	}
};

//Trigger theme-asset update only
const loadImgBackground = function(){
	if (debug) debugLog('* loadImgBackground mode triggered *');

	if (htmlElement.classList.contains('season-override')){
		if (debug) debugLog('season-override mode is on');

		// remove current season
		htmlElement.classList.remove(isSeason);
		if (debug) debugLog(`season-override mode ~ html class ${isSeason} removed`);
		
		if (htmlElement.contains('spring')){
			updateThemeAssets('spring', window.sensor.springFname);
		}
		if (htmlElement.contains('summer')){
			updateThemeAssets('summer', window.sensor.summerFname);
		}
		if (htmlElement.contains('fall')){
			updateThemeAssets('fall', window.sensor.fallFname);
		}
		if (htmlElement.contains('winter')){
			updateThemeAssets('winter', window.sensor.winterFname);
		}
	} else {
		if (isSeason === 'spring'){
			updateThemeAssets('spring', window.sensor.springFname);
		}
		if (isSeason === 'summer'){
			updateThemeAssets('summer', window.sensor.summerFname);
		}
		if (isSeason === 'fall'){
			updateThemeAssets('fall', window.sensor.fallFname);
		}
		if (isSeason === 'winter'){
			updateThemeAssets('winter', window.sensor.winterFname);
		}
	}
};

//Check if not a mobile device update background video
function suppressVidMobile(){
	if (debug) debugLog('* supressVidMobile triggered *');

	// isMobile and !isIPad is true -> remove video element
	if (!navigator.userAgent.match(/iPad/) && window.innerWidth <= 768){
		if (debug) debugLog(`portableDevice: ${portableDevice}`);

		// remove video tag for portable devices
		const vidSrc = document.getElementById('bgvid');
		if (vidSrc) {
			vidSrc.remove();
			if (debug) debugLog('suppressVidMobile fn ~ bgvid element removed');
		} else {
			// video tag undetected or already removed
			if (debug) debugLog('suppressVidMobile fn ~ bgvid element not found');
		}

		vidContainer.innerHTML = ""; // clear all child elements
		if (debug) debugLog('suppressVidMobile fn ~ vidContainer innerHTML cleared');

		// update theme assets only
		if (debug) debugLog('suppressVidMobile fn ~ (invoked) loadImgBackground ...');
		loadImgBackground();
	} else {
		// portableDevice is false check if container has children
		if (vidContainer.children.length === 0){
			const vidObject = document.createElement('video');
			vidObject.id = 'bgvid';
			vidObject.setAttribute('playsinline', '');
			vidObject.setAttribute('muted', '');
			vidObject.setAttribute('loop', '');
			vidObject.setAttribute('poster', '');

			vidContainer.appendChild(vidObject);

			if (debug) debugLog('suppressVidMobile fn ~ (invoked) loadImgBackground, Video element and empty attr created');
			loadBackground();
		} else {
			// if there's an existing video, just reload the background
			if (debug) debugLog('suppressVidMobile fn ~ (invoked) loadImgBackground, Video element detected load background data only');
			loadBackground();
		}
	}
};