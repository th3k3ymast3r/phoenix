function googleTranslateElementInit() {
	new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.FloatPosition.TOP_LEFT}, 'google_translate_element');
}

const gtranslateContent = (langTarget, langId) => {
	if (debug) debugLog(`target lang is: ${langTarget} and lang type is: ${langId}`);

	document.querySelector('.goog-te-combo').value = langId;
	if (debug) debugLog(`Language set to: ${langId}`);

	window.location = langTarget;
	location.reload();
};