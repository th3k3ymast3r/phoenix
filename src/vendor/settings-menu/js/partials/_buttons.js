//settings button
document.querySelector('.settings-btn').addEventListener('click', function(e) {
	if (debug) debugLog('* .settings-btn clicked *');

	// lazyload gtranslate.js and element.js
	//loadGoogleTranslate();

    this.classList.toggle('active');
    document.querySelector('.mode-container').classList.toggle('active');

    if (this.classList.contains('active')) {
        importContent('.mode-container .gtranslate', '/gtranslate');
    } else {
        document.querySelector('.gtranslate').innerHTML = '';
    }

    e.stopPropagation();
});

//settings button clicked outside
if (document.querySelector('.settings-btn').classList.contains('active')){
	document.addEventListener('click', function() {
		if (document.querySelector('.settings-btn').classList.contains('active')){
			if (debug) debugLog('* document clicked outside of .settings-btn *');

			document.querySelector('.settings-btn').classList.remove('active');
			document.querySelector('.mode-container').classList.remove('active');
			document.querySelector('.mode-container .gtranslate').innerHTML = '';
		}
	});
}

//dark-mode button
document.querySelector('.dark-mode').addEventListener('click', function() {
    this.classList.toggle('enabled');
    htmlElement.classList.toggle('dark-knight');
	// Update browser theme color
	applyBrowserTheme();
});

//access-mode button
document.querySelector('.access-mode').addEventListener('click', function(){
	this.classList.toggle('enabled');
	htmlElement.classList.toggle('accessify');
});

//generic disable button class
const disableBtn = document.querySelector('.disable-button');

if (disableBtn){
	disableBtn.addEventListener('click', function(e){
		e.preventDefault();
	});
}

//season-mode button
document.addEventListener('click', function(event) {
	// Use closest to make sure it selects the right ancestor with the class
	const seasonModeBtn = event.target.closest('.season-mode');
	
	// Ensure the element exists and matches the correct class
	if (seasonModeBtn) {
		if (!htmlElement.classList.contains('season-override')){
			document.documentElement.classList.add('season-override');
		}
		
		if (seasonModeBtn.classList.contains('springtime')){ // spring btn
			if (debug) debugLog('Season btn recognized: .springtime');

			seasonModeBtn.classList.add('summertime');
			seasonModeBtn.classList.remove('springtime');
			htmlElement.classList.add('summer');
			htmlElement.classList.remove('spring');
			updateBackground(summerFname);
		}
		else if (seasonModeBtn.classList.contains('summertime')){ // summer btn
			if (debug) debugLog('Season btn recognized: .summertime');

			seasonModeBtn.classList.add('falltime');
			seasonModeBtn.classList.remove('summertime');
			htmlElement.classList.add('fall');
			htmlElement.classList.remove('summer');
			updateBackground(fallFname);
		}
		else if (seasonModeBtn.classList.contains('falltime')){ // fall btn
			if (debug) debugLog('Season btn recognized: .falltime');

			seasonModeBtn.classList.add('wintertime');
			seasonModeBtn.classList.remove('falltime');
			htmlElement.classList.add('winter');
			htmlElement.classList.remove('fall');
			updateBackground(winterFname);
		}
		else if (seasonModeBtn.classList.contains('wintertime')){ // winter btn
			if (debug) debugLog('Season btn recognized: .wintertime');

			seasonModeBtn.classList.add('springtime');
			seasonModeBtn.classList.remove('wintertime');
			htmlElement.classList.add('spring');
			htmlElement.classList.remove('winter');
			updateBackground(springFname);
		}

		// Update browser theme color
		applyBrowserTheme();
	}
});


//Add label to circle path
document.querySelectorAll('.season-mode').forEach(function(element, index) {
    var characters = element.textContent.split("");

    element.innerHTML = ''; // Clear the element's content
    characters.forEach(function(char, i) {
        var span = document.createElement('span');
        span.className = 'r' + i;
        span.textContent = char;
        element.appendChild(span);
    });
});


//generic keystroke esc for close button
document.addEventListener('keydown', function(e) {
	if (e.key === 'Escape' || e.keyCode === 27) {
		if (debug) debugLog('* Esc keystroke detected *');

		e.preventDefault();

		// Escape mode menu
		const settingsBtn = document.querySelector('.settings-btn');
		const modeContainer = document.querySelector('.mode-container');

		if (settingsBtn && settingsBtn.classList.contains('active')) {
			settingsBtn.classList.remove('active');
			modeContainer.classList.remove('active');
		}
	}
});