// READme install settings menu for site translations, darkmode, accessibility mode and theme selector
// Requires Gulp V5 + Node v21.7.3
// Plugins: 
// plumber (gulp-plumber), 
// sourcemaps (gulp-sourcemaps), 
// sass (gulp-sass), 
// sassGlob (gulp-sass-glob), 
// autoprefixer (gulp-autoprefixer), 
// cleanCSS (gulp-clean-css), 
// rename (gulp-rename), 
// gzip (gulp-gzip),
// log (fancy-log)

// Paths code
// const paths = {
    // Not required if using SASS
    // settingscss : {
	// 	src : "src/vendor/settings-menu/css/*.css",
	// 	dest : "dist/assets/css/settings-menu"
	// },
	settingssass : {
		src : [
			"src/vendor/settings-menu/sass/*.sass",
			"src/vendor/settings-menu/sass/partials/*.scss"
		],
		dest : "dist/assets/css/settings-menu"
	},
	settingsjs : {
		src : "src/vendor/settings-menu/js/*.js",
		dest : "dist/assets/js/settings-menu"
	},
	settingshtml : {
		src : "src/vendor/settings-menu/*.ejs",
		dest : "dist"
	}
// };


// Process function -> file.{sass, scss} = file.{css, gzip}
// For SASS/SCSS + GZIP files and minifying
export function cssminifyandgzip({ source, destination, paths, task }) {
	return src(source, { since : lastRun(task), encoding : false })
		.pipe(plumber({ errorHandler: handleerror }))
		.pipe(sourcemaps.init())
		.pipe(sassGlob())
		.pipe(sassCompiler({
			outputStyle: 'compressed',
			includePaths: paths,
			errLogToConsole: true
		}).on('error', sassCompiler.logError))
		.pipe(autoprefixer("last 2 version", "safari 5", "ie 8", "ie 9", "opera 12.1", "ios 6", "android 4"))
		.pipe(cleanCSS({debug: true}, (details) => {
			// Extract the relative file name without extension
			const basename = path.basename(details.name, path.extname(details.name));
			const originalsizekb = details.stats.originalSize / 1024;
			const minifiedsizekb = details.stats.minifiedSize / 1024;
			log(`${basename}.css` + `: ${originalsizekb.toFixed(3)} kb`);
			log(`${basename}.min.css` + `: \x1b[38;2;249;42;114m${minifiedsizekb.toFixed(3)} kb\x1b[0m`);
		}))
		.pipe(sourcemaps.write({
			includeContent : false,
			sourceRoot : file => `file:///Users/th3k3ymast3r/dev/phoenix/src/css`
		}))
		.pipe(rename({
			suffix: ".min"
		}))
		.pipe(dest(destination))
		.pipe(gzip())
		.pipe(dest(destination))
}

//For EJS -> HTML + GZIP
export function htmlandgzip({ source, destination, ext, task }) {
	return src(source, { since : lastRun(task), encoding : false })
		.pipe(plumber({ errorHandler: handleerror }))
		.pipe(ejs())
		.pipe(rename({ extname: ext }))
		.pipe(ignore.exclude('partials/**'))
		.pipe(dest(destination))
		.pipe(gzip())
		.pipe(dest(destination));
}

//For JS + GZIP
function jsandgzip({ source, destination, basename, ext, task }) {
	return src(source, { since : lastRun(task), encoding : false })
		.pipe(plumber({ errorHandler: handleerror }))
		// Condition = settings-menu.js
		.pipe(gulpif(basename+ext==="settings-menu.js", concat("settings-menu.js")))
		.pipe(gulpif(basename+ext==="settings-menu.js", include()))
		.pipe(uglify())
		.pipe(rename({
			suffix: ".min",
			extname: ext
		}))
		.pipe(dest(destination))
		.pipe(gzip())
		.pipe(dest(destination))
}

// For Copy + GZIP only
export function copyandgzip({ source, destination, basement, task }) {
	let copiedFiles = [];

	return src(source, { since : lastRun(task), base : basement, encoding : false })
		.on('data', (file) => {
			const basename = path.basename(file.relative, path.extname(file.relative));

			if (basename && !copiedFiles.includes(basename)) {
				log(`\x1b[1m⇧\x1b[0m '\x1b[38;2;102;217;239m${basename}${file.extname}\x1b[0m'`);
			}
			if (basename && basename !== '' && basename !== null && basename !== undefined && basename !== '-1') {
				copiedFiles.push(basename);
			}
		})
		.pipe(dest(destination))
		.pipe(gzip())
		.pipe(dest(destination));
}

// Export function

export function settingssass() {
	return cssminifyandgzip({
		source : paths.settingssass.src,
		destination : paths.settingssass.dest,
		paths : "src/vendor/settings-menu/partials",
		task : "settingssass"
	});
}

// Not required if using settingssass function
// export function settingscss() {
// 	return cssminifyandgzip({
// 		source : paths.settingscss.src,
// 		destination : paths.settingscss.dest,
// 		task : "settingscss"
// 	});
// }

export function settingsjs() {
	return jsandgzip({
		source : paths.settingsjs.src,
		destination : paths.settingsjs.dest,
		basename : "settings-menu",
		ext : ".js",
		task : "settingsjs"
	});
}

export function settingshtml() {
	return htmlandgzip({
		source : paths.settingshtml.src,
		destination : paths.settingshtml.dest,
        ext : ".html",
		task : "settingshtml"
	});
}

// Watch function
watch(paths.settingshtml.src, gulp.series('settingshtml', reload)).on('change', path => { 
    log({ title: 'settings html update detected', message: `File ${path} was changed` });
});
watch(paths.settingssass.src, gulp.series('settingssass', reload)).on('change', path => { 
    log({ title: 'settings css edit detected', message: `File ${path} was changed` });
});
// Not required if using settingssass watch
// watch(paths.settingscss.src, gulp.series('settingscss', reload)).on('change', path => { 
//     log({ title: 'settings css edit detected', message: `File ${path} was changed` });
// });
watch(paths.settingsjs.src, gulp.series('settingsjs', reload)).on('change', path => { 
    log({ title: 'settings js edit detected', message: `File ${path} was changed` });
});
watch(paths.settingsfonts.src, gulp.series('settingsfonts', reload)).on('change', path => { 
    log({ title: 'settings fonts update detected', message: `File ${path} was changed` });
});

//Build task
// Build
// export const build = series(
	settingsfonts,
	settingssass,
    // Not required if using settingssass function
    // settingscss,
    settingsjs,
	settingshtml
// );
// export { build as default };