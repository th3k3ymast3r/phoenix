// READme install sensor which handles device type, Query vars, season calc, logging and exposes global vars
// Requires Gulp V5 + Node v21.7.3
// Plugins: 
// plumber (gulp-plumber), 
// sourcemaps (gulp-sourcemaps), 
// sass (gulp-sass), 
// sassGlob (gulp-sass-glob), 
// autoprefixer (gulp-autoprefixer), 
// cleanCSS (gulp-clean-css), 
// rename (gulp-rename), 
// gzip (gulp-gzip),
// log (fancy-log)

// Paths code
// const paths = {
	sensorjs : {
		src : "src/vendor/sensor/js/**/*.js",
		dest : "dist/assets/js/sensor"
	},
// };

//For JS + GZIP
function jsandgzip({ source, destination, basename, ext, task }) {
	return src(source, { since : lastRun(task), encoding : false })
		.pipe(plumber({ errorHandler: handleerror }))
		// Condition = sensor.js
		.pipe(gulpif(basename+ext==="sensor.js", concat("sensor.js")))
		.pipe(gulpif(basename+ext==="sensor.js", include()))
		.pipe(uglify())
		.pipe(rename({
			suffix: ".min",
			extname: ext
		}))
		.pipe(dest(destination))
		.pipe(gzip())
		.pipe(dest(destination))
}

// Export function

export function sensorjs() {
	return jsandgzip({
		source : paths.sensorjs.src,
		destination : paths.sensorjs.dest,
		basename : "sensor",
		ext : ".js",
		task : "sensorjs"
	});
}

// Watch function
watch(paths.sensorjs.src, gulp.series('sensorjs', reload)).on('change', path => { 
    log({ title: 'sensor js edit detected', message: `File ${path} was changed` });
});

//Build task
// Build
// export const build = series(
    sensorjs,
// );
// export { build as default };