// Get viewport width dynamically
const viewportWidth = () => {
	return window.innerWidth;
};

// Function to throttle events, ensuring it's only called every 100ms or so
function throttle(func, limit) {
    let lastFunc;
    let lastRan;
    return function(...args) {
        const context = this;
        if (!lastRan) {
            func.apply(context, args);
            lastRan = Date.now();
        } else {
            clearTimeout(lastFunc);
            lastFunc = setTimeout(function() {
                if (Date.now() - lastRan >= limit) {
                    func.apply(context, args);
                    lastRan = Date.now();
                }
            }, limit - (Date.now() - lastRan));
        }
    };
}

// Updates mobile/desktop classes on resize
const updateMobileDesktopClass = () => {
    const vpwMode = window.innerWidth <= 768; // recalculate viewport width directly
	const htmlElement = document.documentElement;

    if (debug) {
        debugLog('* updateMobileDesktop mode triggered *');
        debugLog(`Current viewport width: ${window.innerWidth}`);
        debugLog(`Mobile mode is: ${vpwMode}`);
    }

    // reset classes before applying the correct one
    htmlElement.classList.remove('mobile', 'desktop');

    // apply the correct class based on vpwMode
    if (vpwMode) {
        htmlElement.classList.add('mobile');
    } else {
        htmlElement.classList.add('desktop');
    }
};

// Throttled function for handling the resize event
const throttledResizeHandler = throttle(updateMobileDesktopClass, 100);

// Attach throttled event handler to resize
window.addEventListener('resize', throttledResizeHandler);

// Call initially to set classes based on the initial viewport width
updateMobileDesktopClass();
