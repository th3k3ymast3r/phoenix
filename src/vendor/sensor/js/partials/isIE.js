// Determine the designated MS browser type based on userAgent
const isIE11 = function(){
	let isIE11;

	if (navigator.userAgent.match(/Trident.*rv\:11\./)){
		isIE11 = true;
	} else {
		isIE11 = false;
	}
	
	return isIE11;
}();

const isIE10 = function(){
	let isIE10;

	if (navigator.userAgent.match(/Trident\/6\./)){
		isIE10 = true;
	} else {
		isIE10 = false;
	}
	
	return isIE10;
}();

const isIE9 = function(){
	let isIE9;

	if (navigator.userAgent.match(/Trident\/5\./)){
		isIE9 = true;
	} else {
		isIE9 = false;
	}
	
	return isIE9;
}();

const isEdge = function(){
	let isEdge;

	if (navigator.userAgent.match(/Edge/)){
		isEdge = true;
	} else {
		isEdge = false;
	}
	
	return isEdge;
}();