// Determine aside element has class active
const isAside = function(){
	const aside = document.querySelector('aside');
	return aside && aside.classList.contains('active');
};