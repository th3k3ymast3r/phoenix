// Shorten menu labels for mobile device based on viewport width -900
const isShortname = function(){
	let isShortname;

	if (window.innerWidth <= 768){
		if (debug) debugLog('isShortname fn ~ (direct) cond: '+ (window.innerWidth <= 768));
		isShortname = true;
	} else {
		if (debug) debugLog('isShortname fn ~ (direct) cond: '+ (window.innerWidth <= 768));
		isShortname = false;
	}
	
	return isShortname;
}();