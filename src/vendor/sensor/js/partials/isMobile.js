// Function to check if the device is mobile
const isMobile = () => {
    if (debug) debugLog('* isMobile triggered *');
    return window.innerWidth <= 768;
};

// Assign the return value of checkIfMobile() to the variable isMobile
// let isMobile = isMobileDevice();

if (debug) debugLog(`IsMobile fn ~ viewportWidth: ${viewportWidth()}px`);
if (debug) debugLog(`IsMobile fn ~ isMobile: ${isMobile()}`);