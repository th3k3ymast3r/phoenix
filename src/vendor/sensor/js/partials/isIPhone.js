// Determine the designated iPhone device based on userAgent
const isIPhone = function(){
	let isIPhone;

	if (navigator.userAgent.match(/iPhone/)){
		isIPhone = true;
	} else {
		isIPhone = false;
	}
	
	return isIPhone;
}();