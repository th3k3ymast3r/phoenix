// Handle query variables
const darkmode = getQueryVariable('darkmode');
const accessify = getQueryVariable('accessify') === 'true';
const ctitle = getQueryVariable('ctitle');
const iamMode = getQueryVariable('mode');
const iamThankyou = getQueryVariable('thankyou');
const theme = getQueryVariable('theme');
let isAccessible = false;

// Apply darkmode class if darkmode is true
if (darkmode) document.documentElement.classList.add('dark-knight');

// Apply accessify class and related changes if accessify is true
if (accessify) {
	document.documentElement.classList.add('accessify');
	isAccessible = true;
	document.querySelector('.access-mode').classList.add('enabled');
	document.querySelector('meta[name="viewport"]').setAttribute('content', 'width=device-width, initial-scale=1');
}

// Apply custom title if ctitle is present
if (ctitle) {
	const formatCtitle = "A " + decodeURIComponent(ctitle) + " ";
	document.querySelector('.header-title h1').setAttribute('data-custom-title', formatCtitle);
}

// Handle iamMode and iamThankyou
if (iamMode === "offline") document.documentElement.classList.add("offline");
if (iamMode === "calendly") document.querySelector('.booking').classList.add("calendly");
if (iamThankyou === "thankyou") document.querySelector('.booking').classList.add("thankyou");

//Update theme via URL query see background-vdo.js line:6