// Boolean check for body element containing class disable-scroll
const isScrollDisabled = (function() {
	const scrollDisabled = document.body;
	return scrollDisabled && scrollDisabled.classList.contains('disable-scroll');
})();

// Return vertical scroll position
const scrollTop = () => {
    // get the current scroll position
    let scrollTop = window.pageYOffset || document.documentElement.scrollTop;

    // log the value for debugging
    if (typeof debug !== 'undefined' && debug) {
        debugLog('ScrollTop fn ~ scrollTop (direct) position: ' + scrollTop + 'px');
    }

    // return or use the scrollTop value as needed
    return scrollTop;
};

const bodyScroll = () => {
	// Get the current scroll position
    // let scrollTop = window.pageYOffset || document.documentElement.scrollTop;

	// Debugging log
	if (typeof debug !== 'undefined' && debug) {
		if (isScrollDisabled) {
			debugLog('bodyScroll fn ~ isScrollDisabled: ' + isScrollDisabled);
		} else {
			debugLog('bodyScroll fn ~ (invoked) position: ' + scrollTop() + 'px');
		}
	}

	// Handle primary CTA display excluding target section
	let bookingPanel = document.querySelector('.booking');
	let ctaMenu = document.querySelector('.cta-tray');
	let navBarMenuBtn = document.querySelector('.navbar-nav .menubutton');
	
	if (bookingPanel && typeof bookingPanel.isInViewport() === 'boolean') {
		if (bookingPanel.isInViewport()) {
			ctaMenu.classList.add('fadeOutLeft');
			ctaMenu.classList.remove('fadeInLeft');
			navBarMenuBtn.classList.add('fadeOutDown');
			navBarMenuBtn.classList.remove('fadeInUp');
		} else {
			ctaMenu.classList.remove('fadeOutLeft');
			ctaMenu.classList.add('fadeInLeft');
			navBarMenuBtn.classList.add('fadeInUp');
			navBarMenuBtn.classList.remove('fadeOutDown');
		}
	} else {
		console.error('isInViewport function is not available on bookingPanel');
	}

	// Change CTA orientation
	let socialPanel = document.querySelector('.social-channels');
	if (socialPanel && typeof socialPanel.isInViewport() === 'boolean') {
		if (socialPanel.isInViewport()) {
			ctaMenu.classList.add('rotate');
		} else {
			ctaMenu.classList.remove('rotate');
		}
	}

	// Scrolling highlight
	let highlightPanel = document.querySelectorAll('.highlight');
	highlightPanel.forEach(function(element) {
		if (typeof element.isInViewport() === 'boolean' && element.isInViewport()) {
			element.classList.add('active');
		}
	});

	// Animate blockquote background
	let blockquotePanel = document.querySelectorAll('blockquote');
	blockquotePanel.forEach(function(element) {
		if (typeof element.isInViewport() === 'boolean' && element.isInViewport()) {
			element.classList.add('active');
		}
	});

	// Attention-seeking Booking title
	let bookingH2 = document.querySelector('.cards.booking header h2');
	if (bookingH2 && typeof bookingH2.isInViewport() === 'boolean') {
		if (bookingH2.isInViewport()) {
			bookingH2.classList.add('bounceIn');
		} else {
			bookingH2.classList.remove('bounceIn');
		}
	}

	// Handle menubar logo display
	let menuLogo = document.querySelector('.navbar-nav .menulogo');
	if (menuLogo) {
		menuLogo.style.display = scrollTop() <= 214 ? 'none' : 'list-item';
	}

	// Handle menubar CTA display
	let animatedNav = document.querySelector('.navbar-nav .animated');
	if (animatedNav) {
		animatedNav.style.display = scrollTop() <= 333 ? 'none' : 'flex';
	}

	// Handle mobile CTA display
	if (ctaMenu) {
		ctaMenu.style.display = scrollTop() <= 411 ? 'none' : 'block';
	}

	// Handle accessify nudge icon
	let navBarMenu = document.querySelector('.navbar');
	let scrollNudge = document.querySelector('.scroll-nudge');
	if (navBarMenu && scrollNudge) {
		if (scrollTop() === 0) {
			navBarMenu.classList.remove('scroll-past-0');
			scrollNudge.style.display = 'flex';
		} else {
			navBarMenu.classList.add('scroll-past-0');
			scrollNudge.style.display = 'none';
		}
	}
};

// Lazy load background images
const lazyLoad = () => {
	if (document.querySelector('.departures-board').isInViewport()){
		if (!document.querySelector('.departures-board').classList.contains('lazy-load')){
			document.querySelector('.departures-board').classList.add('lazy-load');
		}
	}

	if (document.querySelector('.questions').isInViewport()){
		if (!document.querySelector('.questions').classList.contains('lazy-load')){
			document.querySelector('.questions').classList.add('lazy-load');
		}
	}

	if (document.getElementById('testimonials').isInViewport()){
		if (!document.getElementById('testimonials').classList.contains('lazy-load')){
			document.getElementById('testimonials').classList.add('lazy-load');
		}
	}

	if (document.querySelector('.partner-0-button').isInViewport()){
		if (!document.querySelector('.partner-0-button').classList.contains('lazy-load')){
			document.querySelector('.partner-0-button').classList.add('lazy-load');
		}
	}

	if (document.querySelector('.partner-1-button').isInViewport()){
		if (!document.querySelector('.partner-1-button').classList.contains('lazy-load')){
			document.querySelector('.partner-1-button').classList.add('lazy-load');
		}
	}

	if (document.querySelector('.partner-2-button').isInViewport()){
		if (!document.querySelector('.partner-2-button').classList.contains('lazy-load')){
			document.querySelector('.partner-2-button').classList.add('lazy-load');
		}
	}

	if (document.querySelector('.partner-3-button').isInViewport()){
		if (!document.querySelector('.partner-3-button').classList.contains('lazy-load')){
			document.querySelector('.partner-3-button').classList.add('lazy-load');
		}
	}

	if (document.querySelector('.partner-4-button').isInViewport()){
		if (!document.querySelector('.partner-4-button').classList.contains('lazy-load')){
			document.querySelector('.partner-4-button').classList.add('lazy-load');
		}
	}

	if (document.querySelector('.partner-5-button').isInViewport()){
		if (!document.querySelector('.partner-5-button').classList.contains('lazy-load')){
			document.querySelector('.partner-5-button').classList.add('lazy-load');
		}
	}

	if (document.querySelector('.partner-6-button').isInViewport()){
		if (!document.querySelector('.partner-6-button').classList.contains('lazy-load')){
			document.querySelector('.partner-6-button').classList.add('lazy-load');
		}
	}

	if (document.querySelector('.partner-7-button').isInViewport()){
		if (!document.querySelector('.partner-7-button').classList.contains('lazy-load')){
			document.querySelector('.partner-7-button').classList.add('lazy-load');
		}
	}

	if (document.querySelector('.partner-8-button').isInViewport()){
		if (!document.querySelector('.partner-8-button').classList.contains('lazy-load')){
			document.querySelector('.partner-8-button').classList.add('lazy-load');
		}
	}

	if (document.querySelector('.partner-9-button').isInViewport()){
		if (!document.querySelector('.partner-9-button').classList.contains('lazy-load')){
			document.querySelector('.partner-9-button').classList.add('lazy-load');
		}
	}

	if (document.querySelector('.partner-10-button').isInViewport()){
		if (!document.querySelector('.partner-10-button').classList.contains('lazy-load')){
			document.querySelector('.partner-10-button').classList.add('lazy-load');
		}
	}

	if (document.querySelector('.partner-11-button').isInViewport()){
		if (!document.querySelector('.partner-11-button').classList.contains('lazy-load')){
			document.querySelector('.partner-11-button').classList.add('lazy-load');
		}
	}
};