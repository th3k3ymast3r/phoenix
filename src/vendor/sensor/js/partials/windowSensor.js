$(document).on('ready');

window.sensor = {
    viewportWidth, //Get viewport inner width
    isAside, //Popup is open/closed based on active class
    isSeason, //Name of season
    isDesktop, //Desktop based on viewportWidth >= 768
    isMobile, //Mobile based on viewportWidth <= 768
    isIPad, //iPad as determined by navigator.userAgent
    isIPhone, //iPhone as determined by navigator.userAgent
    isIE11, //ie11 browser as determined by navigator.userAgent
    isIE10, //ie10 browser as determined by navigator.userAgent
    isIE9, //ie9 browser as determined by navigator.userAgent
    isEdge, //Edge browser as determined by navigator.userAgent
    isShortname, //Shorten menu labels for mobile devices
    isOffline, //No network based on navigator object
    isAccessible, //WCAG color contrast
    isScrollDisabled, //Checks body class element for disable-scroll
    isFeedback, //Checks feedback-content class element for display--content
    scrollTop, //Return vertical scroll position
    debug, //Enable logging
    darkmode, //Darkmode
    accessify, //Query variable
    ctitle, //Custom title query variable
    iamMode, //Mode selector query variable
    iamThankyou, //Contact form confirm message
    versionTimestamp, //Append fragment to force file cache
    springFname, //Spring vdo src file as determined by config.ejs
    summerFname, //Summer vdo src file as determined by config.ejs
    fallFname, //Fall vdo src file as determined by config.ejs
    winterFname, //Winter vdo src file as determined by config.ejs
    darkColor, //Darkmode browser light theme color as determined by config.ejs
    springColor, //Spring browser light theme color as determined by config.ejs
    summerColor, //Summer browser light theme color as determined by config.ejs
    fallColor, //Fall browser light theme color as determined by config.ejs
    winterColor, //Winter browser light theme color as determined by config.ejs
    uxpClck, //0 ~ not clicked, 1 ~ clicked
    uxdClck, //0 ~ not clicked, 1 ~ clicked
    uxiClck, //0 ~ not clicked, 1 ~ clicked
    ikClck, //0 ~ not clicked, 1 ~ clicked
    imClck, //0 ~ not clicked, 1 ~ clicked
    icClck, //0 ~ not clicked, 1 ~ clicked
    ipClck, //0 ~ not clicked, 1 ~ clicked
    uxClck, //0 ~ not clicked, 1 ~ clicked
    uiClck, //0 ~ not clicked, 1 ~ clicked
    iaClck, //0 ~ not clicked, 1 ~ clicked
    ixdClck, //0 ~ not clicked, 1 ~ clicked
    fdbClck //0 ~ not clicked, 1 ~ clicked
}

if (debug) {
    debugLog('* Sensor initialized *: ' + JSON.stringify(window.sensor, null, 2));
}