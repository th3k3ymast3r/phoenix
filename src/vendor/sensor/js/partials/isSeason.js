// Determine the season based on the month returns season name
const isSeason = function(){
	const month = new Date().getMonth() + 1;
	let isSeason;

	if (month >= 3 && month <= 5) {
		isSeason = "spring";
	} else if (month >= 6 && month <= 8) {
		isSeason = "summer";
	} else if (month >= 9 && month <= 11) {
		isSeason = "fall";
	} else { 
		isSeason = "winter";
	}

	return isSeason;
}();