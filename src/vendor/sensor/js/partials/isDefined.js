const waitForFunctionDefinition = (functionName, callback, retryCount = 0, maxRetries = 10) => {
	if (debug) debugLog(`* waitForFunctionDefinition triggered - Attempt ${retryCount + 1} of ${maxRetries} *`);

	if (typeof window[functionName] === 'function') {
		if (debug) debugLog(`${functionName} fn detected as ${typeof window[functionName] === 'function'}`);
		// the function is defined, call it
		callback();
	} else {
		if (retryCount >= maxRetries) {
			if (debug) debugLog(`Unable to determine ${functionName} as a function. setTimeout will now cancel.`);
			return; // stop retrying after max retries
		}

		if (debug) debugLog(`${functionName} fn NOT detected. Retrying in 100ms ...`);
		// retry after a short delay
		setTimeout(() => waitForFunctionDefinition(functionName, callback, retryCount + 1, maxRetries), 100);
	}
};

const defineFunction = (functionName) => {
	if (debug) debugLog('* defineFunction triggered *');

	waitForFunctionDefinition(functionName, () => {
		if (debug) debugLog('* waitForFunctionDefinition invoked *');
		window[functionName](); // call the function when it becomes available
	});
};
