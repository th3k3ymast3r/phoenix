// Determine the designated iPad device based on userAgent
const isIPad = () => {
	let isIPad;

	if (navigator.userAgent.match(/iPad/)){
		isIPad = true;
	} else {
		isIPad = false;
	}
	
	return isIPad;
};