// Determine feedback-content element has class display-content
const isFeedback = function(){
    const feedback = document.querySelector('.feedback-content');
    return feedback && feedback.classList.contains('display--content');
}();