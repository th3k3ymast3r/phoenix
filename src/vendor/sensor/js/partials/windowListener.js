// When document loads start video background conditional statement
// if user resizes browser recall video background conditional statement
const handleWindowEvent = () => {
	viewportWidth();
	orientationMode();
	snoopAgent();
	checkDisabled3Dmenu();
	setHeightMode();
	offlineMode();
	isMobile();
	isIPad();
	shortNameMode();
	suppressVidMobile();
	themeOveride();
	chkWidth();
	abandonBurger();
	departuresBoardDependencies();
	feedbackLabels();
	applyBrowserTheme();
	window.dispatchEvent(new Event('resize'));
};

const handleWindowResize = () => {
	setTimeout(() => {
		viewportWidth();
		orientationMode();
		snoopAgent();
		checkDisabled3Dmenu();
		setHeightMode();
		offlineMode();
		isMobile();
		dropMenuTracker();
		throttledResizeHandler();
		shortNameMode();
		suppressVidMobile();
		themeOveride();
		chkWidth();
		abandonBurger();
		updateShortnameState();
		cancelMenu();
		feedbackLabels();
	}, 50);
};

const handleWindowScroll = () => {
	scrollTop();
	bodyScroll();
	lazyLoad();
	departuresBoardDependencies();
}

const handleWindowOrientation = () => {
	chkWidth();
	abandonBurger();
	shortNameMode();
	orientationMode();
	snoopAgent();
	recalcDisplayHeight();
	setHeightMode();
}

window.addEventListener('load', handleWindowEvent);
window.addEventListener('resize', handleWindowResize);
window.addEventListener('scroll', handleWindowScroll);
window.addEventListener('orientationchange', handleWindowOrientation);

// Declare global variables
let uxpClck = 0;
let uxdClck = 0;
let uxiClck = 0;
let ikClck = 0;
let imClck = 0;
let icClck = 0;
let ipClck = 0;
let uxClck = 0;
let uiClck = 0;
let iaClck = 0;
let ixdClck = 0;
let fdbClck = 0;

const handleButtonClicks = (btnID, elementTarget) => {
	const btnElement = document.querySelector(elementTarget);
	let stripClassDot = elementTarget.replace('.', '');

	if (btnElement) {
		if (debug) debugLog(`${btnID} Click fn ~ click detected`);

		// set the global variable to 1 based on the button clicked

		if (btnID === 'uxp'){ // Case study 1 ~ uxp-story
			uxpClck = 1;
			if (debug) debugLog('uxpClck set to:', uxpClck);
			tierThree(btnID,false,caseStudyOutline);
		} else if (btnID === 'uxd'){ // case study 2 ~ uxd-story
			uxdClck = 1;
			if (debug) debugLog('uxdClck set to:', uxdClck);
			tierThree(btnID,false,caseStudyOutline);
		} else if (btnID === 'uxi'){ // case study 3 ~ uxi-story
			uxiClck = 1;
			if (debug) debugLog('uxiClck set to:', uxiClck);
			tierThree(btnID,false,caseStudyOutline);
		} else if (btnID === 'ik'){ // 3D menu links ~ ik-agile-story
			ikClck = 1;
			if (debug) debugLog('ikClck set to:', ikClck);
			tierFour(btnID,stripClassDot,starOutline);
		} else if (btnID === 'im'){ // 3D menu links ~ im-management-story
			imClck = 1;
			if (debug) debugLog('imClck set to:', imClck);
			tierFour(btnID,stripClassDot,starOutline);
		} else if (btnID === 'ic'){ // 3D menu links ~ ic-code-story
			icClck = 1;
			if (debug) debugLog('icClck set to:', icClck);
			tierFour(btnID,stripClassDot,starOutline);
		} else if (btnID === 'ip'){ // 3D menu links ~ ip-design-story
			ipClck = 1;
			if (debug) debugLog('ipClck set to:', ipClck);
			tierFour(btnID,stripClassDot,starOutline);
		} else if (btnID === 'ux'){ // 3D menu links ~ ux-user-experience-story
			uxClck = 1;
			if (debug) debugLog('uxClck set to:', uxClck);
			tierFour(btnID,stripClassDot,starOutline);
		} else if (btnID === 'ui'){ // 3D menu links ~ ui-user-interface-story
			uiClck = 1;
			if (debug) debugLog('uiClck set to:', uiClck);
			tierFour(btnID,stripClassDot,starOutline);
		} else if (btnID === 'ia'){ // 3D menu links ~ ia-information-architecture-story
			iaClck = 1;
			if (debug) debugLog('iaClck set to:', iaClck);
			tierFour(btnID,stripClassDot,starOutline);
		} else if (btnID === 'ixd'){ // 3D menu links ~ ixd-interactive-story
			ixdClck = 1;
			if (debug) debugLog('ixdClck set to:', ixdClck);
			tierFour(btnID,stripClassDot,starOutline);
		} else if (btnID === 'feedback'){ // footer menu links ~ feedback-form
			fdbClck = 1;
			if (debug) debugLog('fdbClck set to:', fdbClck);
			tierFour(btnID,stripClassDot,false);
		}

		// perform other actions
		toggle3Dmenu();
	}
};

// Event listener for dynamically loaded elements
document.addEventListener('click', function(event){

	//hamburger button
	if (event.target.matches('.hamburger')) {
		if (debug) debugLog('* hamburger button click detected *');
	
		if (document.querySelector('.hamburger').classList.toggle('collapsed')) { // menu is open
			if (debug) debugLog('* hamburger btn menu open *');
	
			document.body.style = '';
			document.querySelector('.navbar-nav').classList.add('show');
	
			// hide CTA-tray
			document.querySelector('.cta-tray').classList.add('hide-cta');
		} else { // menu is closed
			if (debug) debugLog('* hamburger btn menu closed *');
			
			document.body.style.overflowY = 'hidden';
			document.querySelector('.navbar-nav').classList.remove('show');
	
			// show CTA-tray
			document.querySelector('.cta-tray').classList.remove('hide-cta');
		}
	
		// reset focus state if it exists
		if (document.querySelector('.hamburger').classList.contains('hamburger-focus')) {
			document.querySelector('.hamburger').classList.remove('hamburger-focus');
		}
	}	

	// Reset hamburger menu when selecting any menu links
	if (event.target.matches('.nav-link')){

		//In mobile only
		if (window.innerWidth <= 768){
			if (debug) debugLog('nav-link clicked ~ reset burger menu');

			cancelMenu();
		}
	}

	// Case study 1
	if (event.target.matches('.uxp-case-study')){ // uxp-story.html
		handleButtonClicks('uxp', '.uxp-case-study');
	}
	if (event.target.matches('.uxp-close-btn, .uxp-close-btn .progress-bar')){ // uxp-story close btn
		closeButton('uxp',false,true);
	}
	if (event.target.matches('.uxp-next-btn')){ // uxp-story next btn
		closeButton('uxp',false,true,'#case-homeprotect');
		tierThree('uxd',false,caseStudyOutline);
	}

	// Case study 2
	if (event.target.matches('.uxd-case-study')){ // uxd-story.html
		handleButtonClicks('uxd', '.uxd-case-study');
	}
	if (event.target.matches('.uxd-close-btn, .uxd-close-btn .progress-bar')){ // uxd-story close btn
		closeButton('uxd',false,true);
	}
	if (event.target.matches('.uxd-next-btn')){ // uxd-story next btn
		closeButton('uxd',false,true,'#case-theworkshop');
		tierThree('uxi',false,caseStudyOutline);
	}

	// Case study 3
	if (event.target.matches('.uxi-case-study')){ // uxi-story.html
		handleButtonClicks('uxi', '.uxi-case-study');
	}
	if (event.target.matches('.uxi-close-btn, .uxi-close-btn .progress-bar')){ // uxi-story close btn
		closeButton('uxi',false,true);
	}

	// 3D menu ~ cube 1
	if (event.target.matches('.im-management-story')){ // im-management-story.html
		handleButtonClicks('im', '.im-management-story');
	}
	if (event.target.matches('.im-close-btn, .im-close-btn .progress-bar')){ // im-management-story close btn
		closeButton('im',false,true);
	}

	if (event.target.matches('.ic-code-story')){ // ic-code-story.html
		handleButtonClicks('ic', '.ic-code-story');
	}
	if (event.target.matches('.ic-close-btn, .ic-close-btn .progress-bar')){ // ic-code-story close btn
		closeButton('ic',false,true);
	}

	if (event.target.matches('.ik-agile-story')){ // ik-agile-story.html
		handleButtonClicks('ik', '.ik-agile-story');
	}
	if (event.target.matches('.ik-close-btn, .ik-close-btn .progress-bar')){ // ik-agile-story close btn
		closeButton('ik',false,true);
	}

	if (event.target.matches('.ip-design-story')){ // ip-design-story.html
		handleButtonClicks('ip', '.ip-design-story');
	}
	if (event.target.matches('.ip-close-btn, .ip-close-btn .progress-bar')){ // ip-design-story close btn
		closeButton('ip',false,true);
	}

	//3D menu ~ cube 2
	if (event.target.matches('.ux-user-experience-story')){ // ux-user-experience-story.html
		handleButtonClicks('ux', '.ux-user-experience-story');
	}
	if (event.target.matches('.ux-close-btn, .ux-close-btn .progress-bar')){ // ux-user-experience-story close btn
		closeButton('ux',false,true);
	}

	if (event.target.matches('.ui-user-interface-story')){ // ui-user-interface-story.html
		handleButtonClicks('ui', '.ui-user-interface-story');
	}
	if (event.target.matches('.ui-close-btn, .ui-close-btn .progress-bar')){ // ui-user-interface-story close btn
		closeButton('ui',false,true);
	}

	if (event.target.matches('.ia-information-architecture-story')){ // ia-information-architecture-story.html
		handleButtonClicks('ia', '.ia-information-architecture-story');
	}
	if (event.target.matches('.ia-close-btn, .ia-close-btn .progress-bar')){ // ia-information-architecture-story close btn
		closeButton('ia',false,true);
	}

	if (event.target.matches('.ixd-interactive-story')){ // ixd-interactive-story.html
		handleButtonClicks('ixd', '.ixd-interactive-story');
	}
	if (event.target.matches('.ixd-close-btn, .ixd-close-btn .progress-bar')){ // ixd-interactive-story close btn
		closeButton('ixd',false,true);
	}

	if (event.target.matches('.feedback, .feedback-button')){ // feedback-form.html
		handleButtonClicks('feedback', '.feedback-form');
	}
	if (event.target.matches('.feedback-close-btn, .feedback-close-btn .progress-bar')){ // feedback-form close btn
		closeButton('feedback',false,true);
	}
});