// Determine offline mode based on navigator.object
const isOffline = function(){
	let isOffline;

	if (!navigator.onLine){
		isOffline = true;
	} else {
		isOffline = false;
	}
	
	return isOffline;
}();