// Determine the designated desktop device based on viewport width +768
const isDesktop = function(){
	let isDesktop;

	if (window.innerWidth >= 768){
		isDesktop = true;
	} else {
		isDesktop = false;
	}
	
	return isDesktop;
}();