const browserTheme = () => {
	if (document.documentElement.classList.contains('dark-knight')){
		document.querySelector(`meta[media='(prefers-color-scheme: light)']`).setAttribute('content', '#222222');
		document.querySelector(`meta[name='msapplication-navbutton-color']`).setAttribute('content', '#222222');
		document.querySelector(`meta[name='apple-mobile-web-app-status-bar-style']`).setAttribute('content', '#222222');
	} else {
		document.querySelector(`meta[media='(prefers-color-scheme: light)']`).setAttribute('content','#ffab1f');
		document.querySelector(`meta[name='msapplication-navbutton-color']`).setAttribute('content','#ffab1f');
		document.querySelector(`meta[name='apple-mobile-web-app-status-bar-style']`).setAttribute('content','#ffab1f');
	};
};

// Onload add canonical link and set browser theme
window.addEventListener('load', () => {
	setCanonicalLink();
	browserTheme();
});



const setDarkMode = document.createElement('script');
setDarkMode.setAttribute('defer', '');
setDarkMode.src = '/assets/js/sunrise-sunset/sunrise-sunset-darkmode.min.js';

document.body.appendChild(setDarkMode);