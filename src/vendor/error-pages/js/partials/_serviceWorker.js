if ('serviceWorker' in navigator) {
	navigator.serviceWorker
		.register(`sw.js${versionTimestamp}`)
		.then(function() { debugLog("Service Worker Registered"); });
}