// Offline notification
window.addEventListener('load', () => {
	if (!navigator.onLine) {
		document.documentElement.classList.add('offline');
		
		// Create a new span element
		const offlineNote = document.createElement('div');
		offlineNote.setAttribute('title', `${offlineMessage}`);
		offlineNote.classList.add('offline-note');
		offlineNote.classList.add('floating');
		
		// Append the span to the h1 element
		document.querySelector('.offline h1').appendChild(offlineNote);
	}
}, false);