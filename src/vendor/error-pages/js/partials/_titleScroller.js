// Browser tab title scrolling message
(function titleScroller(text){
	document.title = text;
	setTimeout(() => {
		titleScroller(text.substr(1) + text.substr(0, 1));
	}, 500);
}(document.title + " - "));