// Output HTML error message
const detectedBrowser = navigator.userAgent;

(detectedBrowser.match(/iPad/)) ? document.documentElement.classList.add('iPad') : null;
(detectedBrowser.match(/iPhone/)) ? document.documentElement.classList.add('iPhone') : null;

let pre = document.createElement("pre");
pre.classList.add('detected-client');

document.querySelector("header").appendChild(pre);
document.querySelector(".detected-client").innerHTML = detectedBrowser;