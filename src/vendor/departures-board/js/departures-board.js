const loadDeparturesBoard = () => {
	const departuresBoard = (() => {
		// Encapsulate in lazy-load flag
		if (document.querySelector('.departures-board').classList.contains('lazy-load')){
			// Define job roles and trim spaces
			let jobRoles = [
				"UX DESIGNER        ",
				"UI DESIGNER        ",
				"DESIGN TEAM BUILDER",
				"INFO ARCHITECT     ",
				"DESIGN DIRECTOR    ",
				"USER EVANGELIST    ",
				"SASS SPECIALIST    ",
				"VISUAL DESIGNER    ",
				"CURIOUS RESEARCHER ",
				"CONTENT STRATEGIST ",
				"CREATIVE ALCHEMIST ",
				"PSEUDO WIREFRAMIST ",
				"DESIGN TECHNOLOGIST",
				"FRONTEND INTEGRATOR",
				"PRAGMATIC OPTIMIZER",
				"DESIGN SYS ATOMIST "
			];

			// Shuffle the array using D3 v7's shuffle method
			d3.shuffle(jobRoles);

			// Setup the animation cycle for characters
			let cycle = {};
			for (let i = 65; i < 90; i++) {
				cycle[String.fromCharCode(i)] = String.fromCharCode(i + 1);
			}
			cycle["Z"] = " ";
			cycle[" "] = "A";

			// Initialize departures board
			let rows = d3.select(".departures-board")
			.selectAll(".db-row")
			.data(jobRoles.splice(0, 1))
			.enter()
			.append("div")
				.attr("class", "notranslate db-row")
				.style("top", (d, i) => i * 144 + "px");

			// Initialize the flaps for each character
			let flaps = rows.selectAll("div")
			.data(jobTitle => jobTitle.split(""))
			.enter()
			.append("div")
				.attr("class", "flap")
				.style("left", "0%");

			["next", "prev", "back", "front"].forEach(d => {
			let section = flaps.append("div")
				.attr("class", "half " + d);

			if (d === "front") {
				section.append("div")
					.attr("class", "divider");
			}

			section.append("span")
				.text(letter => letter);
			});

			jobRoles.push(...rows.data());

			// Start the flipping animation
			flip();

			function flip() {
			d3.timeout(() => {
				let promises = [];
				rows.each(function() {
					let row = d3.select(this);
					row.selectAll(".flap").each(function(fromLetter, i) {
						let toLetter = jobRoles[0][i],
							flap = d3.select(this);
						if (fromLetter !== toLetter) {
							promises.push(new Promise((resolve) => {
								flipLetter(flap.datum(toLetter), fromLetter, toLetter, resolve);
							}));
						}
					});
					jobRoles.push(jobRoles.shift());
				});

				Promise.all(promises).then(() => {
					flip();
				}).catch(err => {
					console.error("Error during flip animation: ", err);
				});
			}, 3000);
			}

			function flipLetter(flap, fromLetter, toLetter, resolve) {
			let current = fromLetter,
				next = cycle[fromLetter];
			let prevFlaps = flap.selectAll(".prev span, .front span"),
				nextFlaps = flap.selectAll(".back span, .next span"),
				fast = false;

			flap.select(".front").on("animationiteration", function() {
				if (next === toLetter) {
					flap.classed("animated fast", false)
						.selectAll("span")
						.text(toLetter);
					return resolve();
				}

				if (!fast) {
					fast = true;
					flap.classed("fast", true);
				}

				prevFlaps.text(next);

				current = next;
				next = cycle[current];

				setTimeout(() => {
					nextFlaps.text(next);
				}, 500);
			});

			flap.classed("animated", true);
			nextFlaps.text(next);
			}
		}
	})();
};