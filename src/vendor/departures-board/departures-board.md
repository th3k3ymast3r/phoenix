// READme install departures-board flip flap text effect
// Requires Gulp V5 + Node v21.7.3
// Plugins: 
// plumber (gulp-plumber), 
// sourcemaps (gulp-sourcemaps), 
// sass (gulp-sass), 
// sassGlob (gulp-sass-glob), 
// autoprefixer (gulp-autoprefixer), 
// cleanCSS (gulp-clean-css), 
// rename (gulp-rename), 
// gzip (gulp-gzip),
// log (fancy-log)

// Paths code
const paths = {
    //Not required if directly integrating the container code:
	//<div class='departures-board' aria-hidden='true'></div>
    // departuresboardhtml : {
	// 	src : "src/vendor/departures-board/*.html",
	// 	dest : "dist"
	// },
	departuresboardsass : {
		src : [
			"src/vendor/departures-board/sass/*.sass",
			"src/vendor/departures-board/sass/partials/*.scss"
		],
		dest : "dist/assets/css/departures-board"
	},
	departuresboard : {
		src : "src/vendor/departures-board/js/*.js",
		dest : "dist/assets/js/departures-board"
	},
	departuresboard3p : {
		src : [
			"src/vendor/departures-board/3p/*.js"
		],
		dest : "dist/assets/js/departures-board"
	}
};


// Process function -> file.{sass, scss} = file.{css, gzip}
// For SASS/SCSS + GZIP files and minifying
export function cssminifyandgzip({ source, destination, paths, aux, task }) {
	return src(source, { since : lastRun(task), encoding : false })
		.pipe(plumber({ errorHandler: handleerror }))
		.pipe(sourcemaps.init())
		.pipe(sassGlob())
		.pipe(sassCompiler({
			outputStyle: 'compressed',
			includePaths: paths,
			errLogToConsole: true
		}).on('error', sassCompiler.logError))
		.pipe(autoprefixer("last 2 version", "safari 5", "ie 8", "ie 9", "opera 12.1", "ios 6", "android 4"))
		.pipe(cleanCSS({debug: true}, (details) => {
			// Extract the relative file name without extension
			const basename = path.basename(details.name, path.extname(details.name));
			const originalsizekb = details.stats.originalSize / 1024;
			const minifiedsizekb = details.stats.minifiedSize / 1024;
			log(`${basename}.css` + `: ${originalsizekb.toFixed(3)} kb`);
			log(`${basename}.min.css` + `: \x1b[38;2;249;42;114m${minifiedsizekb.toFixed(3)} kb\x1b[0m`);
		}))
		.pipe(sourcemaps.write({
			includeContent : false,
			sourceRoot : file => `file:///Users/th3k3ymast3r/dev/phoenix/${aux}`
		}))
		.pipe(rename({
			suffix: ".min"
		}))
		.pipe(dest(destination))
		.pipe(gzip())
		.pipe(dest(destination))
}

// For Copy + GZIP only
export function copyandgzip({ source, destination, basement, task }) {
	let copiedFiles = [];

	return src(source, { since : lastRun(task), base : basement, encoding : false })
		.on('data', (file) => {
			const basename = path.basename(file.relative, path.extname(file.relative));

			if (basename && !copiedFiles.includes(basename)) {
				log(`\x1b[1m⇧\x1b[0m '\x1b[38;2;102;217;239m${basename}${file.extname}\x1b[0m'`);
			}
			if (basename && basename !== '' && basename !== null && basename !== undefined && basename !== '-1') {
				copiedFiles.push(basename);
			}
		})
		.pipe(dest(destination))
		.pipe(gzip())
		.pipe(dest(destination));
}

// Export function
export function departuresboard() {
	return jsandgzip({
		source : paths.departuresboard.src,
		destination : paths.departuresboard.dest,
		basename : "departures-board",
		ext : ".js",
		task : "departuresboard"
	});
}

export function departuresboardsass() {
	return cssminifyandgzip({
		source : paths.departuresboardsass.src,
		destination : paths.departuresboardsass.dest,
		paths : "src/vendor/departures-board/sass/partials",
		aux : "src/vendor/departures-board/sass",
		task : "departuresboardsass"
	});
}

export function departuresboard3p() {
	return jsandgzip({
		source : paths.departuresboard3p.src,
		destination : paths.departuresboard3p.dest,
		basename : "departures-board-3p",
		ext : ".js",
		task : "departuresboard3p"
	});
}

//Not required if directly integrating the container code:
//<div class='departures-board' aria-hidden='true'></div>
// export function departuresboardhtml() {
// 	return copyandgzip({
// 		source : paths.departuresboardhtml.src,
// 		destination : paths.departuresboardhtml.dest,
// 		basement: "src/vendor/departures-board",
// 		task : "departuresboardhtml"
// 	});
// }

// Watch function
//Not required if container code is directly integrated
// watch(paths.departuresboardhtml.src, gulp.series('departuresboardhtml', reload)).on('change', path => { 
//     log({ title: 'departuresboard html update detected', message: `File ${path} was changed` });
// });
watch(paths.departuresboard.src, gulp.series('departuresboard', reload)).on('change', path => { 
	log({ title: 'departuresboard edit detected', message: `File ${path} was changed` });
});
watch(paths.departuresboardsass.src, gulp.series('departuresboardsass', reload)).on('change', path => { 
	log({ title: 'departuresboard sass edit detected', message: `File ${path} was changed` });
});
watch(paths.departuresboard3p.src, gulp.series('departuresboard3p', reload)).on('change', path => { 
	log({ title: 'departuresboard 3p js edit detected', message: `File ${path} was changed` });
});

//Build task
// Build
// export const build = series(
	departuresboard,
	departuresboardsass,
    departuresboard3p
	//Not required if container code is directly integrated
	//departuresboardhtml
// );
// export { build as default };