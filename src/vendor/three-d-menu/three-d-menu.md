// READme install three-d-menu rotating cube effect
// Requires Gulp V5 + Node v21.7.3
// Plugins: 
// plumber (gulp-plumber), 
// sourcemaps (gulp-sourcemaps), 
// sass (gulp-sass), 
// sassGlob (gulp-sass-glob), 
// autoprefixer (gulp-autoprefixer), 
// cleanCSS (gulp-clean-css), 
// rename (gulp-rename), 
// gzip (gulp-gzip),
// log (fancy-log)

// Paths code
const paths = {
	threedmenusass : {
		src : [
			"src/vendor/three-d-menu/sass/*.sass",
			"src/vendor/three-d-menu/sass/partials/*.scss"
		],
		dest : "dist/assets/css/three-d-menu"
	},
	threedmenuhtml : {
		src : "src/vendor/three-d-menu/*.html",
		dest : "dist"
	}
};

// Process function -> file.{sass, scss} = file.{css, gzip}
// For SASS/SCSS + GZIP files and minifying
export function cssminifyandgzip({ source, destination, paths, aux, task }) {
	return src(source, { since : lastRun(task), encoding : false })
		.pipe(plumber({ errorHandler: handleerror }))
		.pipe(sourcemaps.init())
		.pipe(sassGlob())
		.pipe(sassCompiler({
			outputStyle: 'compressed',
			includePaths: paths,
			errLogToConsole: true
		}).on('error', sassCompiler.logError))
		.pipe(autoprefixer("last 2 version", "safari 5", "ie 8", "ie 9", "opera 12.1", "ios 6", "android 4"))
		.pipe(cleanCSS({debug: true}, (details) => {
			// Extract the relative file name without extension
			const basename = path.basename(details.name, path.extname(details.name));
			const originalsizekb = details.stats.originalSize / 1024;
			const minifiedsizekb = details.stats.minifiedSize / 1024;
			log(`${basename}.css` + `: ${originalsizekb.toFixed(3)} kb`);
			log(`${basename}.min.css` + `: \x1b[38;2;249;42;114m${minifiedsizekb.toFixed(3)} kb\x1b[0m`);
		}))
		.pipe(sourcemaps.write({
			includeContent : false,
			sourceRoot : file => `file:///Users/th3k3ymast3r/dev/phoenix/${aux}`
		}))
		.pipe(rename({
			suffix: ".min"
		}))
		.pipe(dest(destination))
		.pipe(gzip())
		.pipe(dest(destination))
}

// For Copy + GZIP only
export function copyandgzip({ source, destination, basement, task }) {
	let copiedFiles = [];

	return src(source, { since : lastRun(task), base : basement, encoding : false })
		.on('data', (file) => {
			const basename = path.basename(file.relative, path.extname(file.relative));

			if (basename && !copiedFiles.includes(basename)) {
				log(`\x1b[1m⇧\x1b[0m '\x1b[38;2;102;217;239m${basename}${file.extname}\x1b[0m'`);
			}
			if (basename && basename !== '' && basename !== null && basename !== undefined && basename !== '-1') {
				copiedFiles.push(basename);
			}
		})
		.pipe(dest(destination))
		.pipe(gzip())
		.pipe(dest(destination));
}

// Export function
export function threedmenusass() {
	return cssminifyandgzip({
		source : paths.threedmenusass.src,
		destination : paths.threedmenusass.dest,
		paths : "src/vendor/three-d-menu/sass/partials",
		aux : "src/vendor/three-d-menu/sass",
		task : "threedmenusass"
	});
}

//Container code:
//<div class='three-d-menu' aria-hidden='true'></div>
export function threedmenuhtml() {
	return copyandgzip({
		source : paths.threedmenuhtml.src,
		destination : paths.threedmenuhtml.dest,
		basement: "src/vendor/three-d-menu",
		task : "threedmenuhtml"
	});
}

// Watch function
//Not required if container code is directly integrated
// watch(paths.threedmenuhtml.src, gulp.series('threedmenuhtml', reload)).on('change', path => { 
//     log({ title: 'threedmenu html update detected', message: `File ${path} was changed` });
// });
watch(paths.threedmenuhtml.src, gulp.series('threedmenuhtml', reload)).on('change', path => { 
	log({ title: 'threedmenu html edit detected', message: `File ${path} was changed` });
});
watch(paths.threedmenusass.src, gulp.series('threedmenusass', reload)).on('change', path => { 
	log({ title: 'threedmenu sass edit detected', message: `File ${path} was changed` });
});

//Build task
// Build
export const build = series(
	threedmenuhtml,
	threedmenusass
);
export { build as default };