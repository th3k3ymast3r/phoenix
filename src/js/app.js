const importContent = (targetElementID, fileToLoad) => {
	if (debug) {
		debugLog('targetElementID: ' + targetElementID + ' fileToLoad: ' + fileToLoad + '.html' + versionTimestamp);
	}

	fetch(fileToLoad + '.html' + versionTimestamp)
		.then(function(response){
			if (!response.ok){
				throw new Error(response.statusText);
			}
			return response.text();
		})
		.then(function(data){
			const targetElement = document.querySelector(targetElementID);
			targetElement.innerHTML = data;

			// find and execute any script tags
			const scripts = targetElement.querySelectorAll('script');
			scripts.forEach((script) => {
				const newScript = document.createElement('script');
				if (script.src) {
					// if it's an external script, set the src
					newScript.src = script.src;
				} else {
					// inline script, copy the content
					newScript.textContent = script.textContent;
				}
				document.body.appendChild(newScript); // append it to body to execute
			});

			if (debug){
				debugLog('partial load status: success');
			}
		})
		.catch(function(error){
			if (debug){
				debugLog('partial load status: error');
			}
			var msg = `Sorry we were unable to import content at this time, please try again when the network is restored or alternatively contact Leigh using the message form: ${error.message}`;
			if (debug) debugLog(msg);
		});
}

let activeDescriptor = document.querySelector('.descriptor');
activeDescriptor.scroll(bodyScroll);

const videoElement = document.getElementById('bgvid');

if (document.documentElement.classList.contains('desktop')) {
	// Listen for metadata to ensure tracks are ready
	videoElement.addEventListener('loadedmetadata', () => {
		if (videoElement.textTracks.length > 0) {
			videoElement.textTracks[0].mode = 'disabled';
			if (debug) debugLog('Track disabled on desktop');
		}
	});
}