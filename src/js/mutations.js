/*
Script handles the mutations in viewport, reload related to:
- popup window
- menu labels + dropdown toggle
- set html class: desktop, mobile, portrait, landscape, ie9, ie10, ie11, Edge, iPad, iPhone
- survey labels: mobile/desktop
- burger menu toggle
- 3D menu pause
- new window for initial + dynamic DOM
*/

let currentDisplay = document.querySelector('.display--content .descriptor');
let recalcDisplayHeight = () => {
	if (debug) debugLog('orientation recalc display height executed');
	if (currentDisplay){
		currentDisplay.style.height = '100vh';
	}
};

// Shortname update adds or remove shortname class from html element
const updateShortnameState = () => {
	if (window.innerWidth <= 768){
		if (debug) debugLog(`updateShortnameState fn ~ (invoked) isShortname: ${isShortname}, class added`);
		htmlElement.classList.add('shortname');
	} else {
		if (debug) debugLog(`updateShortnameState fn ~ (invoked) isShortname: ${isShortname}, class removed`);
		htmlElement.classList.remove('shortname');
	}
};

// Reset menu label when resizing back to desktop
const resetMenuLabels = () => {
	if (debug) debugLog('* resetMenuLabels mode triggered *');

	let targetLabels = [
		longTitle1,
		longTitle2,
		ctaPrimaryLabel,
		longTitle3,
		longTitle4,
		longTitle5,
		longTitle6,
		longTitle7
	];

	// see config.ejs for values
	document.querySelector(`.nav-link.${navLink1}`).innerHTML = targetLabels[0]; // about-us ~ About me
	document.querySelector(`.nav-link.${navLink2}`).innerHTML = targetLabels[1]; // contact-us ~ Contact me
	document.querySelector(`.nav-link.${navLink3}`).innerHTML = targetLabels[3]; // case-options ~ Case Study
	document.querySelector(`.nav-link.${navLink4}`).innerHTML = targetLabels[4]; // projects-help ~ Private Projects
	document.querySelector(`.nav-link.${navLink5}`).innerHTML = targetLabels[5]; // faqs ~ FAQs
	document.querySelector(`.nav-link.${navLink6}`).innerHTML = targetLabels[6]; // testimonials ~ Testimonials
	document.querySelector(`.nav-link.${navLink7}`).innerHTML = targetLabels[7]; // toolkit ~ Toolkit
};

// Reset menu label when resizing to mobile
const resetMobileLabels = () => {
	if (debug) debugLog('* resetMobileLabels mode triggered *');

	let targetLabels = [
		shortTitle1,
		shortTitle2,
		ctaPrimaryShort,
		shortTitle3,
		shortTitle4,
		shortTitle5,
		shortTitle6,
		shortTitle7
	];

	// See config.ejs for values
	document.querySelector(`.nav-link.${navLink1}`).innerHTML = targetLabels[0]; // about-us ~ About me
	document.querySelector(`.nav-link.${navLink2}`).innerHTML = targetLabels[1]; // contact-us ~ Contact me
	document.querySelector(`.nav-link.${navLink3}`).innerHTML = targetLabels[3]; // case-options ~ Case Study
	document.querySelector(`.nav-link.${navLink4}`).innerHTML = targetLabels[4]; // projects-help ~ Private Projects
	document.querySelector(`.nav-link.${navLink5}`).innerHTML = targetLabels[5]; // faqs ~ FAQs
	document.querySelector(`.nav-link.${navLink6}`).innerHTML = targetLabels[6]; // testimonials ~ Testimonials
	document.querySelector(`.nav-link.${navLink7}`).innerHTML = targetLabels[7]; // toolkit ~ Toolkit
};

// Check shortname mode state
const shortNameMode = () => {
	if (debug) debugLog('* shortNameMode triggered *');

	const navLink = document.querySelectorAll('.nav-link');
	const navItem = document.querySelectorAll('.nav-item');
	const navItemNth2n5 = document.querySelectorAll('.nav-item:nth-child(2), .nav-item:nth-child(5)');
	const collapseNavbarCollapse = document.querySelectorAll('.collapse .navbar-collapse');
	const navElement = document.querySelector('.nav');
	const rowNavItem = document.querySelectorAll('.row .nav-item');
	const twitterSpan = document.querySelector('.twitter span');
	const twitter = document.querySelector('.twitter');
	const aboutUs = document.querySelector('.about-us');
	const caseStudies = document.querySelector('.case-options');

	if (window.innerWidth <= 768) {
		navLink.forEach((element) => {
			element.classList.remove('dropdown-toggle');
			element.innerHTML = '';
		});

		navItem.forEach((element) => {
			element.classList.remove('dropdown');
		});

		collapseNavbarCollapse.forEach((element) => {
			element.classList.add('container');
		});

		navElement.classList.add('row');

		rowNavItem.forEach((element) => {
			element.classList.add('col-2');
		});

		twitterSpan.innerHTML = 'Tweet';
		twitter.setAttribute('aria-label', 'Tweet');

		resetMobileLabels();
	} else {
		navItemNth2n5.forEach((element) => {
			element.classList.add('dropdown');
		});

		collapseNavbarCollapse.forEach((element) => {
			element.classList.remove('container');
		});

		navElement.classList.remove('row');

		rowNavItem.forEach((element) => {
			element.classList.remove('col-2');
		});

		aboutUs.classList.add('dropdown-toggle');
		caseStudies.classList.add('dropdown-toggle');

		twitterSpan.innerHTML = 'Tweet us';
		twitter.setAttribute('aria-label', 'Tweet us');

		resetMenuLabels();
	}
};

// Update dropmenu, feedback labels and shortname states
const chkWidth = () => {
	let vpw = window.innerWidth;

	if (debug) debugLog('* chkWidth mode triggered *');
	if (debug) debugLog(`chkWidth fn ~ (invoked) viewportWidth: ${vpw}px`);

	// Handle dropdown menu on and off-ability
	const navbarNavItems = document.querySelectorAll('.navbar-nav .nav-item.submenu');
	const navbarNavSubs = document.querySelectorAll('.navbar-nav .nav-item.submenu > a');

	// Remove dropdown menu for mobile
	if (isMobile){
		// if class mobile does not exist in html element
		if (!htmlElement.classList.contains('mobile')){
			// add mobile class
			htmlElement.classList.add('mobile');
			// remove desktop
			htmlElement.classList.remove('desktop');
		} else {
			// just remove desktop
			htmlElement.classList.remove('desktop');
		}

		// disabled dropdown menu features
		navbarNavItems.forEach((navItem) => {
			navItem.classList.remove('dropdown');
		});
	
		navbarNavSubs.forEach((navSub) => {
			navSub.classList.remove('dropdown-toggle');
		});
	}

	// Reinstate dropdown menu for desktop
	if (isDesktop){
		// if class desktop does not exist in html element
		if (!htmlElement.classList.contains('desktop')){
			// add desktop class
			htmlElement.classList.add('desktop');
			// remove mobile class
			htmlElement.classList.remove('mobile');
		} else {
			// just remove mobile
			htmlElement.classList.remove('mobile');
		}
		
		// enable dropdown menu features
		navbarNavItems.forEach((navItem) => {
			if (!navItem.classList.contains('dropdown')) {
				navItem.classList.add('dropdown');
			}
		});
		
		navbarNavSubs.forEach((navSub) => {
			if (!navSub.classList.contains('dropdown-toggle')) {
				navSub.classList.add('dropdown-toggle');
			}
		});
	}

	// Update shortname class state
	updateShortnameState();
};

const feedbackLabels = () => {
	if (debug) debugLog('feedbackLabels fn ~ triggered');
	if (debug) debugLog(`isFeedback fn state: ${isFeedback}`);

	if (document.querySelector('.feedback-content').classList.contains('display--content')){
		let happyOption = document.querySelector('label[for=happy-option]');
		let angryOption = document.querySelector('label[for=angry-option]');
		let sadOption = document.querySelector('label[for=sad-option]');

		if (window.innerWidth > 900){ 
			happyOption.innerHTML = longLabel1;
			angryOption.innerHTML = longLabel2;
			sadOption.innerHTML = longLabel3;
		}

		if (window.innerWidth >= 509 && window.innerWidth < 900){
			happyOption.innerHTML = shortLabel1;
			angryOption.innerHTML = shortLabel2;
			sadOption.innerHTML = shortLabel3;
		}

		if (window.innerWidth <= 508){
			happyOption.innerHTML = iconLabel1;
			angryOption.innerHTML = iconLabel2;
			sadOption.innerHTML = iconLabel3;
		}
	}
};

// Hamburger behavior
let hamburger = document.querySelector('.hamburger');
let navCollapse = document.querySelector('.navbar-collapse');
let mask = document.querySelector('.mask');

var abandonBurger = () => {
	if (debug) debugLog('* abandonBurger mode triggered *');
	let inAside = document.querySelector('aside').classList.contains('inactive');

	let asideStatus = document.querySelector('aside').getAttribute('class');
	if (debug) debugLog('asideStatus is: '+ asideStatus);

	if (viewportWidth() >=767 && isAside()) {
		hamburger.classList.add('collapsed');
		hamburger.setAttribute('aria-expanded','false');
		navCollapse.classList.remove('in');
		navCollapse.style = '';
	} else if (viewportWidth() >=767 && inAside) {
		document.body.classList.remove('disable-scroll');
		mask.classList.remove('display-mask');
		mask.classList.remove('z-index-4');
		hamburger.classList.add('collapsed');
		hamburger.setAttribute('aria-expanded','false');
		navCollapse.classList.remove('in');
		navCollapse.style = '';
	};
};

const orientationMode = () => {
	if (debug) debugLog('* orientationMode mode triggered *');

	const setPortrait = () => { htmlElement.classList.add('portrait'); };
	const setLandscape = () => { htmlElement.classList.add('landscape'); };

	const resetOrientation = (() => {
		if (debug) debugLog('* resetOrientation mode triggered *');

		htmlElement.classList.remove('landscape', 'portrait');
	})();

	if (window.matchMedia("(orientation: portrait)").matches) setPortrait();
	if (window.matchMedia("(orientation: landscape)").matches) setLandscape();
};

// User.Agent
const snoopAgent = () => {
	if (debug) debugLog('* snoopAgent mode triggered *');

	const resetSnoop = (() => {
		if (debug) debugLog('* resetAgent mode triggered *');

		htmlElement.classList.remove('ie11', 'ie10', 'ie9', 'iPhone', 'iPad');
	})();

	// determine client browser append hook
	if(isIE11){
		htmlElement.classList.add('ie11');
	}
	if(isIE10){
		htmlElement.classList.add('ie10');
	}

	if(isIE9){
		htmlElement.classList.add('ie9');
	}

	if(isEdge){
		htmlElement.classList.add('Edge');
	}

	if(isIPhone){
		htmlElement.classList.add('iPhone');
	}

	if(isIPad()){
		htmlElement.classList.add('iPad');
	}
};

// Attach hover event listeners directly to dropdown elements
let autoDropmenu = document.querySelectorAll('.dropdown');

const dropMenuTracker = () => {
	if (debug) debugLog('* dropMenuTracker triggered *');
	if (autoDropmenu && window.innerWidth >= 768){
		if (debug) debugLog(`isDesktop is: ${isDesktop}`);

		autoDropmenu.forEach(function(element) {
			element.addEventListener('mouseenter', function() {
				this.classList.add('open');
			});

			element.addEventListener('mouseleave', function() {
				this.classList.remove('open');
			});
		});
	}
};

// Pause 3D menu CSS animations
const cubeSelect = document.querySelectorAll('.cube');
const checkDisabled3Dmenu = () => {
	cubeSelect.forEach((element) => {
		if (isMobile()) {
			element.classList.add('pause');
		} else {
			element.classList.remove('pause');
		}
	});
};

let desktopDevice = (isDesktop);
let mobileDevice = (isIPad() || (isMobile()));
let smartphoneDevice = (!isIPad() && isMobile());
let tabletDevice = (isIPad());

if (debug) debugLog(`isDesktop fn ~ (invoked) desktopDevice is: ${desktopDevice}`);
if (debug) debugLog(`isIPad or isMobile fn ~ (invoked) mobileDevice is: ${mobileDevice}`);
if (debug) debugLog(`!isIPad and isMobile fn ~ (invoked) smartphoneDevice is: ${smartphoneDevice}`);
if (debug) debugLog(`isIPad fn ~ (invoked) tabletDevice is: ${tabletDevice}`);

// Hide extra text and 3d menu for portable devices
if (smartphoneDevice){
	document.querySelectorAll('.suppress, .three-d-menu').forEach((element) => {
		element.style.display = 'none';
	});
	checkDisabled3Dmenu();
}

if (tabletDevice){ 
	document.querySelectorAll('.suppress').forEach((element) => {
		element.style.display = 'none';
	});
}

// Function to add the 'target="_blank"' attribute to all '.new-window' elements
const setTargetForNewWindowLinks = (() => {
	document.querySelectorAll('a.new-window').forEach((element) => {
		element.setAttribute('target', '_blank');
	});
})();

// Function to update dynamic message content
const updateDynamicMessage = () => {
	const dynMsg = document.querySelector('.dyn-msg');
	if (!dynMsg) return;

	if (bookingPanel.classList.contains('thankyou')) {
		dynMsg.innerHTML = panel2ConfirmMessage;
	} else if (bookingPanel.classList.contains('calendly')) {
		dynMsg.innerHTML = panel2CalendlyMessage;
	} else {
		dynMsg.innerHTML = panel2Intro;
	}
};

// Create a MutationObserver to watch for added nodes
const observer = new MutationObserver(function(mutationsList) {
	for (let mutation of mutationsList){
		if (mutation.type === 'childList'){
			mutation.addedNodes.forEach(node => {
				if (node.nodeType === 1){ // only process element nodes
					if (node.matches && node.matches('a.new-window')) {
						node.setAttribute('target', '_blank');
					}
					// check if any of the added nodes have descendants that are .new-window
					node.querySelectorAll && node.querySelectorAll('a.new-window').forEach(function(element) {
						element.setAttribute('target', '_blank');
					});
				}
			});
		}

		// Check for class attribute changes on `.booking` element
		if (mutation.type === 'attributes' && mutation.attributeName === 'class' && mutation.target.classList.contains('booking')) {
			updateDynamicMessage();
		}
	}
});

// Initialize dynamic message content on page load
const bookingPanel = document.querySelector('.booking');
const bookingIntro = document.querySelector('.booking-text');

const dynamicPanel = document.createElement('span');
dynamicPanel.classList.add('dyn-msg');
bookingIntro.prepend(dynamicPanel);
updateDynamicMessage();

// Create and add the booking text link
const bookingTextLink = document.createElement('a');
bookingTextLink.href = '#booking';
bookingTextLink.classList.add('bookme-link');
bookingTextLink.innerHTML = panel2Summary;
bookingIntro.append(bookingTextLink);

// Start observing the document body for child node additions and `.booking` class changes
observer.observe(document.body, { childList: true, subtree: true, attributes: true, attributeFilter: ['class'] });

// Calculate height of class descriptor to set height for vertical scrollbar
const setHeightMode = () => {
	if (debug) debugLog('* setHeightMode mode triggered *');

	if (isAside()){
		let scrollContainer = document.querySelector('aside.active .context-viewer .display--content .descriptor');
		let contextViewerDiv = document.querySelector('.context-viewer .display--content'); // directly target display--content div

		if (contextViewerDiv) {
			scrollContainer.style.height = '100vh'; // if display--content exists, set height to 100vh
		} else {
			scrollContainer.style.height = 'auto';  // otherwise, set it to auto
		}
	}
}

// Cancel mobile menu if resized
const cancelMenu = function(){
	if (debug) debugLog('cancelMenu fn detected');

	setTimeout(() => {
		// reset hamburger menu
		document.querySelector('.hamburger').classList.add('collapsed');

		// reset menu display
		document.querySelector('.navbar-nav').classList.remove('show');

		// reinstate scroll
		document.body.style = '';
		document.body.classList.remove('disable-scroll');

		// reinstate CTA-tray
		document.querySelector('.cta-tray').classList.remove('hide-cta');

		// hide mask
		document.querySelector('.mask').classList.remove('display-mask');
		document.querySelector('.mask').classList.remove('z-index-9');
	}, 50);
}