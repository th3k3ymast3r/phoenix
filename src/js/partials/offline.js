document.addEventListener('DOMContentLoaded', function() {
	const anchorGflag = document.querySelectorAll('a.gflag');

	if (isOffline) {
		anchorGflag.forEach(function(element) {
			let currTitle = element.getAttribute('title');
			element.setAttribute('data-online', currTitle);
			element.setAttribute('title', currTitle + ' translation offline');
		});

		document.querySelectorAll('.social-menu li.menuitem a.new-window').forEach(function(element) {
			element.setAttribute('title', 'Ext. links offline');
			element.addEventListener('click', function(e) {
				e.preventDefault();
			});
		});
	} else {
		anchorGflag.forEach(function(element) {
			if (element.getAttribute('data-online')) {
				element.setAttribute('title', element.getAttribute('data-online'));
			}
			if (element.getAttribute('data-offline')) {
				element.removeAttribute('data-offline');
			}
		});

		document.querySelectorAll('.social-menu li.menuitem a.new-window').forEach(function(element) {
			element.setAttribute('title', '');
		});
	}
});

const offlineMode = () => {
	if (isOffline){
		htmlElement.classList.add('offline');

		// append an element
		let offlineMessage = document.querySelector('.offline .offline-message');
		if (offlineMessage) {
			let span = document.createElement('span');
			offlineMessage.innerHTML = '';
			span.title = "You are viewing locally cached content";
			span.className = "offline-note";
			offlineMessage.appendChild(span);
		}

		// replace innerHTML
		let vrContainer = document.querySelector('.offline .three-sixty-container');
		if (vrContainer) {
			vrContainer.innerHTML = '<div class="vr-offline"><p>VR is unavailable offline</p></div>';
		}

		let feedbackHeader = document.querySelector('.offline #feedback h3');
		if (feedbackHeader) {
			feedbackHeader.innerHTML = 'SuperGUi is in island mode';
		}

		let feedbackContent = document.querySelector('.offline .feedback-content .descriptor form');
		if (feedbackContent) {
			feedbackContent.innerHTML = '<div class="survey-offline"><p>Survey is unavailable offline</p></div>';
		}
	} else {
		htmlElement.classList.remove('offline');
	}
}

if ('serviceWorker' in navigator){
	navigator.serviceWorker
		.register(`sw.js${versionTimestamp}`)
		.then(() => {
			debugLog("* Service Worker Registered *");
		});
}