/*
function initialD(){	
	// Fetch elements outside the animation loop
	const containers = Array.from(document.querySelectorAll('.hot-tip'));

	const elements = containers.map((container) => {
		const containerId = container.getAttribute('data-container');
		const floatingDiv = container.querySelector('.floating-div');
		const svg = container.querySelector('.line-svg');
		const line = container.querySelector('.line');
		const dotA = container.querySelector('.dot-a');
		const dotB = container.querySelector('.dot-b');

		let lineStartX, lineStartY, lineEndX, lineEndY;

		const containerClass = container.getAttribute('class');
		if (containerClass.includes('tr')) {
			lineStartX = container.offsetWidth;
			lineStartY = 0;
			lineEndX = floatingDiv.getBoundingClientRect().right - container.getBoundingClientRect().left;
			lineEndY = floatingDiv.getBoundingClientRect().top - container.getBoundingClientRect().top;
		} else if (containerClass.includes('bl')) {
			lineStartX = 0;
			lineStartY = container.offsetHeight;
			lineEndX = floatingDiv.getBoundingClientRect().height - container.getBoundingClientRect().left;
			lineEndY = floatingDiv.getBoundingClientRect().height - container.getBoundingClientRect().top;
		} else if (containerClass.includes('br')) {
			lineStartX = container.offsetWidth;
			lineStartY = container.offsetHeight;
			lineEndX = floatingDiv.getBoundingClientRect().right - container.getBoundingClientRect().left;
			lineEndY = floatingDiv.getBoundingClientRect().bottom - container.getBoundingClientRect().top;
		} else {
			// default class 'tl'
			lineStartX = 0;
			lineStartY = 0;
			lineEndX = floatingDiv.getBoundingClientRect().left - container.getBoundingClientRect().left;
			lineEndY = floatingDiv.getBoundingClientRect().top - container.getBoundingClientRect().top;
		}

		let targetId = null;
		const classList = container.classList;

		classList.forEach((className) => {
			if (className.startsWith('appoint-')) {
				targetId = className.substring(8);
			}
		});

		if (targetId) {
			const targetElement = document.getElementById(targetId);

			if (targetElement) {
				const targetRect = targetElement.getBoundingClientRect();
				lineStartX = targetRect.right - container.getBoundingClientRect().left;
				lineStartY = targetRect.bottom - container.getBoundingClientRect().top;
			}
		}

		line.setAttribute('x1', lineStartX);
		line.setAttribute('y1', lineStartY);
		line.setAttribute('x2', lineEndX);
		line.setAttribute('y2', lineEndY);

		dotA.setAttribute('cx', lineStartX);
		dotA.setAttribute('cy', lineStartY);
		dotB.setAttribute('cx', lineEndX);
		dotB.setAttribute('cy', lineEndY);

		return {
			containerId,
			container,
			floatingDiv,
			svg,
			line,
			dotA,
			dotB,
			targetId,
		};
	});

	// Update line position based on div's animation
	function updateLinePosition(element) {
	  const { container, floatingDiv, svg, line, dotA, dotB } = element;

	  const containerRect = container.getBoundingClientRect();
	  const divRect = floatingDiv.getBoundingClientRect();

	  const containerClass = container.getAttribute('class');
	  const hasAppointTrClass = containerClass.includes('appoint-') && containerClass.includes('tr');
	  const hasAppointTlClass = containerClass.includes('appoint-') && containerClass.includes('tl');
	  const hasAppointBrClass = containerClass.includes('appoint-') && containerClass.includes('br');
	  const hasAppointBlClass = containerClass.includes('appoint-') && containerClass.includes('bl');
	  const targetId = hasAppointTrClass || hasAppointTlClass || hasAppointBrClass || hasAppointBlClass ? containerClass.split(' ').find(className => className.startsWith('appoint-')).substring(8) : null;

	  let lineStartX, lineStartY, lineEndX, lineEndY;

	  if (targetId) {
	    const targetElement = document.getElementById(targetId);
	    if (targetElement) {
	      const targetRect = targetElement.getBoundingClientRect();
	      if (hasAppointTrClass) {
	        lineStartX = divRect.right - containerRect.left;
	        lineStartY = divRect.top - containerRect.top;
	        lineEndX = targetRect.left - containerRect.left;
	        lineEndY = targetRect.bottom - containerRect.top;
	      } else if (hasAppointTlClass) {
	        lineStartX = divRect.left - containerRect.left;
	        lineStartY = divRect.top - containerRect.top;
	        lineEndX = targetRect.right - containerRect.left;
	        lineEndY = targetRect.bottom - containerRect.top;
	      } else if (hasAppointBrClass) {
	        lineStartX = divRect.right - containerRect.left;
	        lineStartY = divRect.bottom - containerRect.top;
	        lineEndX = targetRect.left - containerRect.left;
	        lineEndY = targetRect.top - containerRect.top;
	      } else if (hasAppointBlClass) {
	        lineStartX = divRect.left - containerRect.left;
	        lineStartY = divRect.bottom - containerRect.top;
	        lineEndX = targetRect.right - containerRect.left;
	        lineEndY = targetRect.top - containerRect.top;
	      }
	    }
	  } else if (containerClass.includes('tl') && !containerClass.includes('appoint-')) {
	    lineStartX = 0;
	    lineStartY = 0;
	    lineEndX = divRect.left - containerRect.left;
	    lineEndY = divRect.top - containerRect.top;
	  } else if (containerClass.includes('tr') && !containerClass.includes('appoint-')) {
	    lineStartX = containerRect.width;
	    lineStartY = 0;
	    lineEndX = divRect.left - containerRect.left;
	    lineEndY = divRect.top - containerRect.top;
	  } else if (containerClass.includes('bl') && !containerClass.includes('appoint-')) {
	    lineStartX = 0;
	    lineStartY = containerRect.height;
	    lineEndX = divRect.left - containerRect.left;
	    lineEndY = divRect.bottom - containerRect.top;
	  } else if (containerClass.includes('br') && !containerClass.includes('appoint-')) {
	    lineStartX = containerRect.width;
	    lineStartY = containerRect.height;
	    lineEndX = divRect.left - containerRect.left;
	    lineEndY = divRect.top - containerRect.top;
	  } else {
	    // default class 'tl'
	    lineStartX = 0;
	    lineStartY = 0;
	    lineEndX = divRect.left - containerRect.left;
	    lineEndY = divRect.top - containerRect.top;
	  }

	  line.setAttribute('x1', lineStartX);
	  line.setAttribute('y1', lineStartY);
	  line.setAttribute('x2', lineEndX);
	  line.setAttribute('y2', lineEndY);

	  dotA.setAttribute('cx', lineStartX);
	  dotA.setAttribute('cy', lineStartY);
	  dotB.setAttribute('cx', lineEndX);
	  dotB.setAttribute('cy', lineEndY);
	}


	// Generate animation keyframes for each container with a delay
	function generateAnimationKeyframes(element, delay) {
		const { floatingDiv } = element;
		const animationDuration = 2; // animation duration in seconds
		const animationDelay = delay * 0.25; // adjust delay as desired

		const keyframes = `@keyframes floatingAnimation-${element.containerId} {
			0% {
				box-shadow: var(--elevation-1p);
				transform: translateY(0px);
			}
			50% {
				box-shadow: var(--elevation-3p);
				transform: translateY(-3px);
			}
			100% {
				box-shadow: var(--elevation-1p);
				transform: translateY(0px);
			}
		}`;

		const styleSheet = document.styleSheets[0];
		styleSheet.insertRule(keyframes, styleSheet.cssRules.length);

		floatingDiv.style.animation = `floatingAnimation-${element.containerId} ${animationDuration}s ease-in-out 3`;
		floatingDiv.style.animationDelay = `${animationDelay}s`;
	}

	// Call updateLinePosition initially and on each animation frame for each container
	let lastTimestamp = 0;

	function animate(timestamp) {
		const deltaTime = timestamp - lastTimestamp;
		const frameRate = 1000 / 60; // update 30 times per second (adjust as desired)

		if (deltaTime >= frameRate) {
			elements.forEach((element, index) => {
				try {
					updateLinePosition(element);
					generateAnimationKeyframes(element, index);
				} catch (error) {
					console.error('An error occurred:', error);
				}
			});

			lastTimestamp = timestamp;
		}

		requestAnimationFrame(animate);
	}
	animate();
}
*/