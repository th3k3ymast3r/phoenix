// Home button
document.querySelector('.menulogo').addEventListener('click', function() {
	if (debug) debugLog('* Home button triggered *');
	window.scrollTo({
		top: document.documentElement.offsetTop,
		behavior: 'smooth'
	});
});

// Text read more toggle
let abtClck = 0;
let ifrClck = 0;
let pltClck = 0;
let sprClck = 0;
let cssClck = 0;
let bknClck = 0;
let prjClck = 0;
let qstClck = 0;
let tlkClck = 0;

// Tier 1 menu button function
function tierOne(btnID,sectionID){
	if (sectionID !== '') {
		importContent('.' + btnID + ' .read', '/' + sectionID);
	}
	
	document.querySelector('.' + btnID + ' .article-footer').classList.add(btnID + '--gigante');
	document.querySelector('.cards.' + btnID).classList.add(btnID + '--resize');
	document.querySelector('.' + btnID + ' .read').classList.add('more');
	
	setTimeout(() => {
		const article = document.querySelector('article.' + btnID);
		if (article) {
			window.scrollTo({
				top: article.getBoundingClientRect().top + window.pageYOffset,
				behavior: 'smooth'
			});
		}
	}, 500);
};

// Tier 2 menu button function
function tierTwo(btnID,hashTag,sectionID,delay){
	if (sectionID !== '') {
		importContent('.' + btnID + ' .read', '/' + sectionID);
	}
	
	document.querySelector('.' + btnID + ' .article-footer').classList.add(btnID + '--gigante');
	document.querySelector('.' + btnID).classList.add(btnID + '--resize');
	document.querySelector('.' + btnID + ' .read').classList.add('more');
	
	setTimeout(() => {
		const targetElement = document.querySelector('article.' + btnID + ' #' + hashTag);
		if (targetElement) {
			window.scrollTo({
				top: targetElement.getBoundingClientRect().top + window.pageYOffset,
				behavior: 'smooth'
			});
		}
	}, delay);	
};

// Tier 3 button open medium-format or deprecated-format popup
function tierThree(btnID,deprecated,outline){
	if (debug) debugLog(`tierThree fn ~ importContent args invoked: ${btnID} + ${deprecated}`);
	
	// determine importContent based on deprecated flag
	if (deprecated){
		importContent(`.${btnID}-content .descriptor`, btnID);
	} else {
		importContent(`.${btnID}-content .descriptor`, `${btnID}-story`);
	}

	// update aside class names
	const asideElement = document.querySelector('aside');
	asideElement.classList.remove('inactive');
	
	// split class names into separate arguments
	asideElement.classList.add('active', `open-${btnID}-context`, `${btnID}--resize`);
	
	// update close button class names
	const closeButton = document.querySelector(`.open-${btnID}-context > .close-button`);
	if (closeButton) {
		closeButton.classList.add(`${btnID}-close-btn`);
	}

	// update content class names
	const contentElement = document.querySelector(`.${btnID}--resize .${btnID}-content`);
	contentElement.classList.add('display--content');

	if (deprecated){
		document.querySelector('.context-viewer .display--content .descriptor .highlight, .context-viewer .display--content .descriptor blockquote').classList.add('active');
	}

	// update mask and body classes
	const maskElement = document.querySelector('.mask');
	if (maskElement) {
		maskElement.classList.add('display-mask');
		maskElement.classList.remove('z-index-4');
	}
	
	// stop main body from scrolling while scroll is enabled on the popup
	// case-view class set for resizing cube menu
	document.body.classList.add('disable-scroll', 'case-view');

	// call additional functions
	setHeightMode(); // set popup height for scrolling
	engageScroll(`.${btnID}-content`); // sets progress marker
	engageScrollMarker(outline); // updates progress marker state
	embedFeatures(); // sets embed-medium features e.g image comparison|rollover
};

// Tier 4 button open non-story popup
function tierFour(btnID,fileToLoad,outline){
	if (debug) debugLog(`tierFour fn ~ importContent args invoked: ${btnID} + ${fileToLoad}`);

	importContent(`.${btnID}-content .descriptor`, fileToLoad);

	// update aside class names
	const asideElement = document.querySelector('aside');
	asideElement.classList.remove('inactive');

	// split class names into separate arguments
	asideElement.classList.add('active', `open-${btnID}-context`, `${btnID}--resize`);

	// update close button class names
	const closeButtonElement = document.querySelector(`.open-${btnID}-context > .close-button`);
	if (closeButtonElement) {
		closeButtonElement.classList.add(btnID + '-close-btn');
	}

	// update content class names
	const resizeContentElement = document.querySelector(`.${btnID}--resize .${btnID}-content`);
	if (resizeContentElement) {
		resizeContentElement.classList.add('display--content');
	}

	const maskElement = document.querySelector('.mask');
	if (maskElement) {
		maskElement.classList.add('display-mask');
		maskElement.classList.remove('z-index-4');
	}

	// stop main body from scrolling while scroll is enabled on the popup
	// blog-view class set for resizing cube menu
	document.body.classList.add('disable-scroll', 'blog-view');

	// call additional functions
	setHeightMode(); // set popup height for scrolling
	engageScroll(`.${btnID}-content`); //sets progress marker
	engageScrollMarker(outline); // updates progress marker state
};

// Read more toggle
function readMore(btnID,sectionID,delay){
	if (debug) debugLog('btnID: ' + btnID + ' sectionID: ' + sectionID + ' delay: ' + delay);

	const articleFooter = document.querySelector('.' + btnID + ' .article-footer');
	const btnElement = document.querySelector('.' + btnID);
	const readElement = document.querySelector('.' + btnID + ' .read');
	const sectionElement = document.getElementById(sectionID);

	if (articleFooter) {
		articleFooter.classList.toggle(btnID + '--gigante');
	}
	
	if (btnElement) {
		btnElement.classList.toggle(btnID + '--resize');
	}
	
	if (readElement) {
		readElement.classList.toggle('more');
	}

	if (sectionElement) {
		const scrollToPosition = sectionElement.offsetTop;
		window.scrollTo({
			top: scrollToPosition,
			behavior: 'smooth'
		});
	}
};

// About button
document.querySelectorAll('.about-button').forEach((element) => {
	element.addEventListener('click', function() {
		abtClck = 1;
		tierOne('about', 'about-us');
	});
});

// About profile
document.querySelector('.leigh-button').addEventListener('click', function(){
	abtClck = 1;
	tierTwo('about','leigh-laguisma','about-us','500');
});

// About skills
document.querySelectorAll('.skills-button').forEach((element) => {
	element.addEventListener('click', function(){
		abtClck = 1;
		tierTwo('about','skills','about-us','666');
	});
});

// About skills model
document.querySelector('.diorama-button').addEventListener('click', function(){
	abtClck = 1;
	tierTwo('about','skills-diorama','about-us','832');
});

// About read more toggle
document.querySelector('.about .article-footer').addEventListener('click', function(){
	if (abtClck == 0) {
		abtClck = 1;
		importContent('.about .read','about-us');
	} else {
		abtClck = 0;
		let aboutReadElement = document.querySelector('.about .read');
		if (aboutReadElement) {
			aboutReadElement.innerHTML = '';
		}
	};
	readMore('about','about-us',500);
});

// Booking button
document.querySelectorAll('.bookme-button, .bookme-link, .bookme-float-button, .bookme-cta-button').forEach(function(element) {
	element.addEventListener('click', function() {
		bknClck = 1;
		var audio = new Audio('/assets/audio/Coin.mp3');
		audio.play();
		if (document.querySelector('.calendly')) document.querySelector('.booking').classList.remove('calendly');
		if (document.querySelector('.thankyou')) document.querySelector('.booking').classList.remove('thankyou');
		tierOne('booking', 'booking-form');
	});
});

// Booking read more toggle
document.querySelector('.booking .article-footer').addEventListener('click', function(){
	if (bknClck == 0) {
		bknClck = 1;
		importContent('.booking .read', 'booking-form');
	} else {
		bknClck = 0;
		let bookingReadElement = document.querySelector('.booking .read');
		if (bookingReadElement) {
			bookingReadElement.innerHTML = '';
		}
	};
	readMore('booking', 'booking', 500);
});

// Case menu buttons
document.querySelector('.case-button').addEventListener('click', function(){
	tierOne('case','case-study');
});

// Case one
document.querySelectorAll('.case-ag-button').forEach((element) => {
	element.addEventListener('click', function(){
		cssClck = 1;
		tierTwo('case','case-avantiagroup','case-study','500');
	});
});

// Case two
document.querySelectorAll('.case-hp-button').forEach((element) => {
	element.addEventListener('click', function(){
		cssClck = 1;
		tierTwo('case','case-homeprotect','case-study','666');
	});
});

// Case three
document.querySelectorAll('.case-tw-button').forEach((element) => {
	element.addEventListener('click', function(){
		cssClck = 1;
		tierTwo('case','case-theworkshop','case-study','832');
	});
});

// Case read more toggle
document.querySelector('.case .article-footer').addEventListener('click', function(){
	if (cssClck == 0) {
		cssClck = 1;
		importContent('.case .read','case-study');
	} else {
		cssClck = 0;
		let caseReadElement = document.querySelector('.case .read');
		if (caseReadElement) {
			caseReadElement.innerHTML = '';
		}
	};
	readMore('case','case-study',500);
});

// Case-study bookmark buttons
const btnsBulletProgress = (outlineArray) => {
	// Ensure outlineArray is valid before processing
	if (outlineArray === false) return;
	
	const bookmarks = JSON.parse(outlineArray);

	bookmarks.forEach(function(bookmarkName) {
		const bookmarkElement = document.querySelector(`.${bookmarkName}`);

		if (bookmarkElement) {
			bookmarkElement.addEventListener('click', function() {
				if (typeof debugLog === 'function') {
					debugLog('clicked: .display--content section#' + bookmarkName);
				}

				// find the target element by its ID
				const targetElement = document.getElementById(bookmarkName);

				if (targetElement) {
					// smooth scroll to the target element
					targetElement.scrollIntoView({
						behavior: 'smooth',
						block: 'start' // scroll to the top of the target element
					});
				}
			});
		}
	});
}

// Global aside close-button popup
let closeButton = function(targetBtn,deactivate,caseView,relocate){
	let asideElement = document.querySelector('aside');
	let maskElement = document.querySelector('.mask');
	let bulletElement = document.querySelector('.bullet-progress');
	let bodyElement = document.body;

	// reset status
	asideElement.classList.add('inactive')
	// reset width to close aside
	asideElement.classList.remove('active', `${targetBtn}--resize`, `open-${targetBtn}-context`);
	// reset bullets
	bulletElement.innerHTML = '';

	// hide content
	document.querySelector('.'+targetBtn+'-content').classList.remove('display--content');
	document.querySelector('.'+targetBtn+'-content').innerHTML = '<div class="descriptor"></div>';

	// remove close btn identifier
	document.querySelector('.'+targetBtn+'-close-btn').classList.remove(targetBtn+'-close-btn');

	// unmask background
	maskElement.classList.remove('display-mask');

	// re-enable scroll
	bodyElement.classList.remove('disable-scroll');

	// reset 3Dmenu animation
	toggle3Dmenu();

	if (deactivate) {
		if (debug) debugLog('deactivate highlight: '+deactivate);

		let deactivateBlockquote = document.querySelectorAll('.context-viewer .display--content .descriptor .highlight, .context-viewer .display--content .descriptor blockquote');

		// deactivate highlight + blockquote
		deactivateBlockquote.forEach(element => {
			element.classList.remove('active');
		});
	}

	if (caseView) {
		if (debug) debugLog(`case-view: ${caseView}`);
		
		// reset 3D menu by removing 'case-view' class from html and body
		document.documentElement.classList.remove('case-view');
		document.body.classList.remove('case-view');
		document.documentElement.classList.remove('blog-view');
		document.body.classList.remove('blog-view');
	}

	if (relocate) {
		if (debug) debugLog('scroll target is: ' + relocate);
	
		let container = document.documentElement || document.body;
		let scrollTo = document.querySelector(relocate);
	
		if (scrollTo) {
			let scrollToPosition = scrollTo.getBoundingClientRect().top + window.scrollY;
			window.scrollTo({
				top: scrollToPosition,
				left: 0,
				behavior: 'smooth'
			});
		}
	}
	
};

/*
// Infra button open popup
$('.infra-button').on('click', function(){
	ifrClck = 1;
	tierThree('infra',true);
});

// Platform button open popup
$('.platform-button').on('click', function(){
	pltClck = 1;
	tierThree('platform',true);
});

// Esports button open popup
$('.esports-button').on('click', function(){
	sprClck = 1;
	tierThree('esports',true);
});
*/

// Projects button
document.querySelectorAll('.projects-button').forEach((element) => {
	element.addEventListener('click', function(){
		prjClck = 1;
		tierOne('projects','projects');
	});
});

// Projects read more toggle
document.querySelector('.projects .article-footer').addEventListener('click', function(){
	if (prjClck == 0) {
		prjClck = 1;
		importContent('.projects .read','projects');
	} else {
		prjClck = 0;
		projectsReadElement = document.querySelector('.projects .read');
		if (projectsReadElement) {
			projectsReadElement.innerHTML = '';
		}
	};
	readMore('projects','projects',500);
});

// Questions button
document.querySelectorAll('.questions-button').forEach((element) => {
	element.addEventListener('click', function(){
		qstClck = 1;
		tierOne('questions','faqs');
	});
});

// Questions read more toggle
document.querySelector('.questions .article-footer').addEventListener('click', function(){
	if (qstClck == 0) {
		qstClck = 1;
		importContent('.questions .read','faqs');
	} else {
		qstClck = 0;
		let questionsReadElement = document.querySelector('.questions .read');
		if (questionsReadElement) {
			questionsReadElement.innerHTML = '';
		}
	};
	readMore('questions','questions',500);
});

// Testimonials button
document.querySelectorAll('.testimonials-button').forEach((element) => {
	element.addEventListener('click', function(){
		window.scrollTo({
			top: document.querySelector('#testimonials').offsetTop, // get the top offset of the target element
			behavior: 'smooth' // enable smooth scrolling
		});
	});
});

// Toolkit button
document.querySelectorAll('.toolkit-button').forEach((element) => {
	element.addEventListener('click', function(){
		tlkClck = 1;
		tierOne('toolkit','toolkit');
	});
});

// Toolkit read more toggle
document.querySelector('.toolkit .article-footer').addEventListener('click', function(){
	if (tlkClck == 0) {
		tlkClck = 1;
		importContent('.toolkit .read','toolkit');
	} else {
		tlkClck = 0;
		let toolkitReadElement = document.querySelector('.toolkit .read');
		if (toolkitReadElement) {
			toolkitReadElement.innerHTML = '';
		}
	};
	readMore('toolkit','toolkit',500);
});

// Contact button
document.querySelector('.contact-button').addEventListener('click', function(){
	window.scrollTo({
		top: document.querySelector('#contact-us').offsetTop, // get the top offset of the target element
		behavior: 'smooth' // enable smooth scrolling
	});
});

// Generic keystroke esc for close button
$('body').keydown(function(e){
	if (e.which == 27){
		e.preventDefault();

		const asideElement = document.querySelector('aside');
		const hideContentViewer = document.querySelector('.context-viewer div');
		const htmlElement = document.documentElement;
		const maskElement = document.querySelector('.mask');
		const bodyElement = document.body;
		const navbarNavElement = document.querySelector('.navbar-nav');

		// escape popup modal
		if (isAside){
			// reset status
			asideElement.classList.remove();
			asideElement.classList.add('inactive');

			// hide content
			hideContentViewer.classList.remove('display--content');

			// reset 3D menubar
			htmlElement.classList.remove('case-view');

			// reset mask
			maskElement.classList.remove('display-mask');

			// reset scroll
			bodyElement.classList.remove('disable-scroll');
		}

		// escape hamburger menu
		if (navbarNavElement.classList.contains('show')){
			cancelMenu();
		}
	}
});

// Send tab focus to hamburger menu
let lastMenuItemLink = document.querySelector('.mobile .social-menu > .menuitem:last-child a');

if (lastMenuItemLink){
	let navbarToggler = document.querySelector('.navbar-toggler.collapsed');

	// on tabbing bring focus to hamburger btn after social channel links
	lastMenuItemLink.addEventListener('keydown', function(e) {
		if (e.key === 'Tab' || e.which === 9) { // modern browsers use 'e.key' while older use 'e.which'
			e.preventDefault();

			if (navbarToggler) {
				navbarToggler.focus();
				navbarToggler.classList.add('hamburger-focus');
			}
		}
	});

	// reset hamburger menu focus
	if (navbarToggler){
		navbarToggler.addEventListener('blur', function(event){
			if (navbarToggler.classList.contains('hamburger-focus')){
				navbarToggler.classList.remove('hamburger-focus');
			}
		})
	}
}