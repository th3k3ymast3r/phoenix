const structuredData = {
	"@context": "https://schema.org",
	"@type": "LocalBusiness",
	"url": companyURL,
	"businessType": businessType,
	"businessName": companyName,
	"businessOwner": {
		"@type": "Person",
		"name": businessOwner
	},
	"brand": {
		"logo": `${companyURL}/${companyName}-logo.png`,
		"image": `${companyURL}/${companyName}-picture.png`
	},
	"address": {
		"@type": "Business",
		"streetAddress": `${businessAddress0} ${businessAddress1}`,
		"addressLocality": businessAddress2,
		"addressRegion": businessAddress3,
		"postalCode":businessAddress4,
		"addressCountry": businessCountry,
		"telephone": businessTelephone,
		"fax": businessFax
	},
	"sameAs" :	[	`https://www.facebook.com/${companyName}`,
					`https://www.twitter.com/${companyName}`
				],
	"name": companyName,
	"description": `${companyName} - ${companyDescription}`,
	"telephone": businessTelephone,
	"openingHours": [	"Mo,Tu,We,Th,Fr 15:00-20:00",
						"Sa 08:00-20:00",
						"Su By Appointment Only"
					],
	"map": `https://www.google.com/maps/dir/?api=1&destination=${businessLocation}`,
	"geo": {
		"@type": "GeoCoordinates",
		"latitude": businessLatitude,
		"longitude": businessLongitude
	},
	"publisher": "SuperGUi.Co LLC",
	"author": {
		"@type": "Person",
		"name": businessOwner
	}
};

// Create a script element for structured data
const script = document.createElement('script');
script.type = 'application/ld+json';
script.textContent = JSON.stringify(structuredData);

const noscript = document.createElement('noscript');
noscript.textContent = "Local business schema is a type of structured data markup code you can add to your business's website to make it easier for search engines to identify what type of organization you are and what you do. This is a helpful way to help optimize your website for local SEO, as it's a major ranking factor";

document.body.appendChild(script);
document.body.appendChild(noscript);