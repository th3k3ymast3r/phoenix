let engageScroll = function(targetStory){
	if (debug) debugLog('engageScroll fn ~ args:' + targetStory);

	// close progress button
	const scrollAside = document.querySelector(targetStory+' .descriptor');
	const pct = document.querySelector('.pct');

	// exception from progress percentage update
	const exceptionPage = (() => {
		if (debug) debugLog(`exceptionPage fn ~ exclude pct for: ${targetStory}`);

		// if feedback-form.html
		return targetStory === '.feedback-content';
	})();

	// initialize pct state
	if (exceptionPage){
		pct.classList.add('active');
	}

	scrollAside.addEventListener('scroll', event => {
		asideScroll();
	}, { passive: true });

	function setTopValue(svg){
		svg.style.top = document.documentElement.clientHeight * 0.5 - (svg.getBoundingClientRect().height * 0.5) + 'px';
	}

	function setProgress(container, pct, progressBar, totalLength) {
		const clientHeight = scrollAside.clientHeight;
		const scrollHeight = scrollAside.scrollHeight;
		const scrollTop = scrollAside.scrollTop;
		const percentage = scrollTop / (scrollHeight - clientHeight);

		if(percentage === 1){
			container.classList.add('completed');
			pct.classList.add('active');
		} else {
			container.classList.remove('completed');
			pct.classList.remove('active');
		}
		pct.innerHTML = Math.round(percentage * 100) + '%';
		progressBar.style.strokeDashoffset = totalLength - totalLength * percentage;
	}

	let asideScroll = function(){
		if (debug) debugLog('engageScroll fn ~ asideScroll');

		// animate figure images ~ isInViewport()
		let figurePanel = document.querySelectorAll('figure.animated');
		if (figurePanel){
			if (debug) debugLog('* figurePanel invoked *');

			figurePanel.forEach((element) => {
				if (element.isInViewport()) {
					if (debug) debugLog('figurePanel fn ~ forEach detects isInViewport()');

					element.classList.add('zoomInDown');
				} else {
					if (debug) debugLog('figurePanel fn ~ forEach isInViewport() not detected');
					element.classList.remove('zoomInDown');
				}
			});
		}

		// case study close button progress indicator
		const container = document.querySelector('.progress-container');
		const svg = document.querySelector('svg.completed');
		const progressBar = document.querySelector('.progress-bar');
		const totalLength = progressBar.getTotalLength();

		setTopValue(svg);

		progressBar.style.strokeDasharray = totalLength;
		progressBar.style.strokeDashoffset = totalLength;

		if (exceptionPage){
			if (debug) debugLog('exceptionPage fn ~ pct progress disabled');
			if (!pct.classList.contains('active')){
				pct.classList.add('active');
			}
		} else {
			scrollAside.addEventListener('scroll', () => {
				setProgress(container, pct, progressBar, totalLength);
			});
	
			window.addEventListener('resize', () => {
				setTopValue(svg);
			});
		}
	}
}

// Hide pseudo background when out of viewport
let hidePseudoImg = (caseIDname,contentClassID) => {
	if (debug) debugLog(`* hidePseudoImg triggered * ~ args is: ${caseIDname} and ${contentClassID}`);

	// invoked at uxp-story.html
	// args passed: hidePseudoImg('#view-uxp-story','.uxp-content .descriptor');
	let scrollAside = document.querySelector(contentClassID); 

	let setPseudoImgVisibility = (caseIDname) => {
		if (debug) debugLog(`* setPseudoImgVisibility triggered * ~ args is: ${caseIDname}`);
		
		if (document.querySelector(caseIDname).isInViewport()){
			if (debug) debugLog(`${caseIDname} is viewable`);

			document.querySelector(caseIDname).classList.remove('hideBackgroundPseudoImg');
		} else {
			if (debug) debugLog(`${caseIDname} is not viewable ~ hide background img`);

			document.querySelector(caseIDname).classList.add('hideBackgroundPseudoImg');
		}
	}

	scrollAside.addEventListener('scroll', event => {
		if (debug) debugLog(`hidePseudoImg fn ~ scrollAside is: ${caseIDname}`);

		setPseudoImgVisibility(caseIDname); // '#view-uxp-story'
	}, { passive: true });
}