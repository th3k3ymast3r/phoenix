'use strict';

$(document).on('ready');

// Prevent body from scrolling
if (isScrollDisabled){
	document.ontouchmove = function(e){e.preventDefault()};
} else {
	document.ontouchmove = function(e){return true};
}